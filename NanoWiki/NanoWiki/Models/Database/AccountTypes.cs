﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("AccountTypes")]
    public class AccountTypes
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Description", ResourceType = typeof(Translations)), Column("Description")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "AccessLvl", ResourceType = typeof(Translations)), Column("AccessLvl")]
        public int AccessLvl { get; set; }
    }
}