﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("JournalsComments")]
    public class JournalsComments
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkJournals", ResourceType = typeof(Translations)), Column("FkJournals")]
        public int FkJournals { get; set; }

        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        [Display(Name = "FkAdmins", ResourceType = typeof(Translations)), Column("FkAdmins")]
        public int FkAdmins { get; set; }

        [Display(Name = "FkJournalsComments", ResourceType = typeof(Translations)), Column("FkJournalsComments")]
        public int FkJournalsComments { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "DateComment", ResourceType = typeof(Translations)), Column("DateComment")]
        public string DateComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "TimeComment", ResourceType = typeof(Translations)), Column("TimeComment")]
        public string TimeComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        // References
        [ForeignKey("FkJournals")]
        public virtual Journals Journals { get; set; }
    }
}