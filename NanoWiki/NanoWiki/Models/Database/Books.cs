﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("Books")]
    public class Books
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAuthors", ResourceType = typeof(Translations)), Column("FkAuthors")]
        public int FkAuthors { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkTopics", ResourceType = typeof(Translations)), Column("FkTopics")]
        public int FkTopics { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "PostDate", ResourceType = typeof(Translations)), Column("PostDate")]
        public string PostDate { get; set; }

        [Display(Name = "Photo", ResourceType = typeof(Translations)), Column("Photo")]
        public string Photo { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(40)]
        [Display(Name = "BookLanguage", ResourceType = typeof(Translations)), Column("BookLanguage")]
        public string BookLanguage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "PublishingHouse", ResourceType = typeof(Translations)), Column("PublishingHouse")]
        public string PublishingHouse { get; set; }

        [StringLength(255)]
        [Display(Name = "ISBN", ResourceType = typeof(Translations)), Column("ISBN")]
        public string ISBN { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Year", ResourceType = typeof(Translations)), Column("Year")]
        public int Year { get; set; }

        [Display(Name = "PageCount", ResourceType = typeof(Translations)), Column("PageCount")]
        public int PageCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(120)]
        [Display(Name = "Description", ResourceType = typeof(Translations)), Column("Description")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }
        
        [Display(Name = "Shops", ResourceType = typeof(Translations)), Column("Shops")]
        public string Shops { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "PostStatus", ResourceType = typeof(Translations)), Column("PostStatus")]
        public bool PostStatus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "LikeCount", ResourceType = typeof(Translations)), Column("LikeCount")]
        public int LikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "DislikeCount", ResourceType = typeof(Translations)), Column("DislikeCount")]
        public int DislikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "VisitCount", ResourceType = typeof(Translations)), Column("VisitCount")]
        public int VisitCount { get; set; }

        // References
        [ForeignKey("FkAuthors")]
        public virtual Authors Authors { get; set; }

        [ForeignKey("FkTopics")]
        public virtual Topics Topics { get; set; }

    }
}