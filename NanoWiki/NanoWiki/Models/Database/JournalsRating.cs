﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("JournalsRating")]
    public class JournalsRating
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkJournals", ResourceType = typeof(Translations)), Column("FkJournals")]
        public int FkJournals { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        [Display(Name = "LikeRating", ResourceType = typeof(Translations)), Column("LikeRating")]
        public bool LikeRating { get; set; }

        [Display(Name = "DislikeRating", ResourceType = typeof(Translations)), Column("DislikeRating")]
        public bool DislikeRating { get; set; }

        // References
        [ForeignKey("FkJournals")]
        public virtual Journals Journals { get; set; }

        [ForeignKey("FkUsers")]
        public virtual Users Users { get; set; }

    }
}