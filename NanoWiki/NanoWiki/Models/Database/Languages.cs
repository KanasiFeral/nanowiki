﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("Languages")]
    public class Languages
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "FullName", ResourceType = typeof(Translations)), Column("FullName")]
        public string FullName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(5)]
        [Display(Name = "Abbreviation", ResourceType = typeof(Translations)), Column("Abbreviation")]
        public string Abbreviation { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Description", ResourceType = typeof(Translations)), Column("Description")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(20)]
        [Display(Name = "DateFormat", ResourceType = typeof(Translations)), Column("DateFormat")]
        public string DateFormat { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(20)]
        [Display(Name = "TimeFormat", ResourceType = typeof(Translations)), Column("TimeFormat")]
        public string TimeFormat { get; set; }
    }
}