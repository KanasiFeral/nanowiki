﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("BooksRating")]
    public class BooksRating
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkBooks", ResourceType = typeof(Translations)), Column("FkBooks")]
        public int FkBooks { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        [Display(Name = "LikeRating", ResourceType = typeof(Translations)), Column("LikeRating")]
        public bool LikeRating { get; set; }

        [Display(Name = "DislikeRating", ResourceType = typeof(Translations)), Column("DislikeRating")]
        public bool DislikeRating { get; set; }

        // References
        [ForeignKey("FkBooks")]
        public virtual Books Books { get; set; }

        [ForeignKey("FkUsers")]
        public virtual Users Users { get; set; }

    }
}