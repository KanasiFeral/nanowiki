﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("Authors")]
    public class Authors
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Initials", ResourceType = typeof(Translations)), Column("Initials")]
        public string Initials { get; set; }

        [StringLength(50)]
        [Display(Name = "BirthDate", ResourceType = typeof(Translations)), Column("BirthDate")]
        public string BirthDate { get; set; }

        [StringLength(50)]
        [Display(Name = "DeathDate", ResourceType = typeof(Translations)), Column("DeathDate")]
        public string DeathDate { get; set; }

        [StringLength(255)]
        [Display(Name = "BirthLocation", ResourceType = typeof(Translations)), Column("BirthLocation")]
        public string BirthLocation { get; set; }

        [StringLength(50)]
        [Display(Name = "Nationality", ResourceType = typeof(Translations)), Column("Nationality")]
        public string Nationality { get; set; }
    }
}