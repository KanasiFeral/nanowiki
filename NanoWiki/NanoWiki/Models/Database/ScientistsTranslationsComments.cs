﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("ScientistsTranslationsComments")]
    public class ScientistsTranslationsComments
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkScientistsTranslations", ResourceType = typeof(Translations)), Column("FkScientistsTranslations")]
        public int FkScientistsTranslations { get; set; }

        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        [Display(Name = "FkAdmins", ResourceType = typeof(Translations)), Column("FkAdmins")]
        public int FkAdmins { get; set; }

        [Display(Name = "FkScientistsTranslationsComments", ResourceType = typeof(Translations)), Column("FkScientistsTranslationsComments")]
        public int FkScientistsTranslationsComments { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "DateComment", ResourceType = typeof(Translations)), Column("DateComment")]
        public string DateComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "TimeComment", ResourceType = typeof(Translations)), Column("TimeComment")]
        public string TimeComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        // References
        [ForeignKey("FkScientistsTranslations")]
        public virtual ScientistsTranslations ScientistsTranslations { get; set; }
    }
}