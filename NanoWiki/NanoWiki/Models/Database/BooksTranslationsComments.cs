﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("BooksTranslationsComments")]
    public class BooksTranslationsComments
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkBooksTranslations", ResourceType = typeof(Translations)), Column("FkBooksTranslations")]
        public int FkBooksTranslations { get; set; }
        
        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }
        
        [Display(Name = "FkAdmins", ResourceType = typeof(Translations)), Column("FkAdmins")]
        public int FkAdmins { get; set; }
        
        [Display(Name = "FkBooksTranslationsComments", ResourceType = typeof(Translations)), Column("FkBooksTranslationsComments")]
        public int FkBooksTranslationsComments { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "DateComment", ResourceType = typeof(Translations)), Column("DateComment")]
        public string DateComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "TimeComment", ResourceType = typeof(Translations)), Column("TimeComment")]
        public string TimeComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        // References
        [ForeignKey("FkBooksTranslations")]
        public virtual BooksTranslations BooksTranslations { get; set; }
    }
}