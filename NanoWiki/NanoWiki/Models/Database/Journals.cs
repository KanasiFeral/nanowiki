﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("Journals")]
    public class Journals
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkCities", ResourceType = typeof(Translations)), Column("FkCities")]
        public int FkCities { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkCountries", ResourceType = typeof(Translations)), Column("FkCountries")]
        public int FkCountries { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAuthors", ResourceType = typeof(Translations)), Column("FkAuthors")]
        public int FkAuthors { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkTopics", ResourceType = typeof(Translations)), Column("FkTopics")]
        public int FkTopics { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "PostDate", ResourceType = typeof(Translations)), Column("PostDate")]
        public string PostDate { get; set; }

        [Display(Name = "Photo", ResourceType = typeof(Translations)), Column("Photo")]
        public string Photo { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(120)]
        [Display(Name = "Description", ResourceType = typeof(Translations)), Column("Description")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(40)]
        [Display(Name = "JournalLanguage", ResourceType = typeof(Translations)), Column("JournalLanguage")]
        public string JournalLanguage { get; set; }

        [StringLength(255)]
        [Display(Name = "Publisher", ResourceType = typeof(Translations)), Column("Publisher")]
        public string Publisher { get; set; }

        [Display(Name = "Year", ResourceType = typeof(Translations)), Column("Year")]
        public int Year { get; set; }

        [StringLength(255)]
        [Display(Name = "Refereed", ResourceType = typeof(Translations)), Column("Refereed")]
        public string Refereed { get; set; }

        [Display(Name = "IssuesPerYear", ResourceType = typeof(Translations)), Column("IssuesPerYear")]
        public int IssuesPerYear { get; set; }

        [StringLength(50)]
        [Display(Name = "JCRImpactFactor", ResourceType = typeof(Translations)), Column("JCRImpactFactor")]
        public string JCRImpactFactor { get; set; }

        [Display(Name = "IssueArticles", ResourceType = typeof(Translations)), Column("IssueArticles")]
        public int IssueArticles { get; set; }

        [StringLength(50)]
        [Display(Name = "ImpactFactorRSCI", ResourceType = typeof(Translations)), Column("ImpactFactorRSCI")]
        public string ImpactFactorRSCI { get; set; }

        [StringLength(255)]
        [Display(Name = "RenameTransferInformation", ResourceType = typeof(Translations)), Column("RenameTransferInformation")]
        public string RenameTransferInformation { get; set; }

        [StringLength(255)]
        [Display(Name = "Reduction", ResourceType = typeof(Translations)), Column("Reduction")]
        public string Reduction { get; set; }

        [StringLength(50)]
        [Display(Name = "ISSNPrintVersion", ResourceType = typeof(Translations)), Column("ISSNPrintVersion")]
        public string ISSNPrintVersion { get; set; }

        [Display(Name = "SubscriptionIndex", ResourceType = typeof(Translations)), Column("SubscriptionIndex")]
        public int SubscriptionIndex { get; set; }

        [StringLength(50)]
        [Display(Name = "ISSNIOnlineVersion", ResourceType = typeof(Translations)), Column("ISSNIOnlineVersion")]
        public string ISSNIOnlineVersion { get; set; }

        [StringLength(255)]
        [Display(Name = "PresentationOption", ResourceType = typeof(Translations)), Column("PresentationOption")]
        public string PresentationOption { get; set; }

        [StringLength(50)]
        [Display(Name = "ISI", ResourceType = typeof(Translations)), Column("ISI")]
        public string ISI { get; set; }

        [Display(Name = "TotalArticles", ResourceType = typeof(Translations)), Column("TotalArticles")]
        public int TotalArticles { get; set; }

        [StringLength(255)]
        [Display(Name = "CurrentlyStatus", ResourceType = typeof(Translations)), Column("CurrentlyStatus")]
        public string CurrentlyStatus { get; set; }

        [StringLength(50)]
        [Display(Name = "SCOPUS", ResourceType = typeof(Translations)), Column("SCOPUS")]
        public string SCOPUS { get; set; }

        [Display(Name = "TotalIssues", ResourceType = typeof(Translations)), Column("TotalIssues")]
        public int TotalIssues { get; set; }

        [StringLength(50)]
        [Display(Name = "RISC", ResourceType = typeof(Translations)), Column("RISC")]
        public string RISC { get; set; }

        [Display(Name = "FullTexts", ResourceType = typeof(Translations)), Column("FullTexts")]
        public int FullTexts { get; set; }

        [StringLength(50)]
        [Display(Name = "Abstract", ResourceType = typeof(Translations)), Column("Abstract")]
        public string Abstract { get; set; }

        [StringLength(255)]
        [Display(Name = "HACList", ResourceType = typeof(Translations)), Column("HACList")]
        public string HACList { get; set; }

        [Display(Name = "Quotes", ResourceType = typeof(Translations)), Column("Quotes")]
        public int Quotes { get; set; }

        [StringLength(50)]
        [Display(Name = "Multidisciplinary", ResourceType = typeof(Translations)), Column("Multidisciplinary")]
        public string Multidisciplinary { get; set; }

        [Display(Name = "SubjectHeadings", ResourceType = typeof(Translations)), Column("SubjectHeadings")]
        public string SubjectHeadings { get; set; }

        [Display(Name = "EditorialCollege", ResourceType = typeof(Translations)), Column("EditorialCollege")]
        public string EditorialCollege { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "PostStatus", ResourceType = typeof(Translations)), Column("PostStatus")]
        public bool PostStatus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "LikeCount", ResourceType = typeof(Translations)), Column("LikeCount")]
        public int LikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "DislikeCount", ResourceType = typeof(Translations)), Column("DislikeCount")]
        public int DislikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "VisitCount", ResourceType = typeof(Translations)), Column("VisitCount")]
        public int VisitCount { get; set; }

        // References
        [ForeignKey("FkCities")]
        public virtual Cities Cities { get; set; }

        [ForeignKey("FkCountries")]
        public virtual Countries Countries { get; set; }

        [ForeignKey("FkAuthors")]
        public virtual Authors Authors { get; set; }

        [ForeignKey("FkTopics")]
        public virtual Topics Topics { get; set; }
    }
}