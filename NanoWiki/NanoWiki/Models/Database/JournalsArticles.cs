﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("JournalsArticles")]
    public class JournalsArticles
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkJournals", ResourceType = typeof(Translations)), Column("FkJournals")]
        public int FkJournals { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkTopics", ResourceType = typeof(Translations)), Column("FkTopics")]
        public int FkTopics { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkOrganizations", ResourceType = typeof(Translations)), Column("FkOrganizations")]
        public int FkOrganizations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAuthors", ResourceType = typeof(Translations)), Column("FkAuthors")]
        public int FkAuthors { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Description", ResourceType = typeof(Translations)), Column("Description")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(40)]
        [Display(Name = "ArticleLanguage", ResourceType = typeof(Translations)), Column("ArticleLanguage")]
        public string ArticleLanguage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "ArticleYear", ResourceType = typeof(Translations)), Column("ArticleYear")]
        public int ArticleYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(40)]
        [Display(Name = "Pages", ResourceType = typeof(Translations)), Column("Pages")]
        public string Pages { get; set; }

        // References
        [ForeignKey("FkJournals")]
        public virtual Journals Journals { get; set; }

        [ForeignKey("FkTopics")]
        public virtual Topics Topics { get; set; }

        [ForeignKey("FkOrganizations")]
        public virtual Organizations Organizations { get; set; }

        [ForeignKey("FkAuthors")]
        public virtual Authors Authors { get; set; }
    }
}