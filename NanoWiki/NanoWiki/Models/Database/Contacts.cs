﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("Contacts")]
    public class Contacts
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkLanguages", ResourceType = typeof(Translations)), Column("FkLanguages")]
        public int FkLanguages { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Address", ResourceType = typeof(Translations)), Column("Address")]
        public string Address { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Phone", ResourceType = typeof(Translations)), Column("Phone")]
        public string Phone { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Email", ResourceType = typeof(Translations)), Column("Email")]
        public string Email { get; set; }

        // References
        [ForeignKey("FkLanguages")]
        public virtual Languages Languages { get; set; }
    }
}