﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("JournalsTranslationsContents")]
    public class JournalsTranslationsContents
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkJournalsTranslations", ResourceType = typeof(Translations)), Column("FkJournalsTranslations")]
        public int FkJournalsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAuthorsTranslations", ResourceType = typeof(Translations)), Column("FkAuthorsTranslations")]
        public int FkAuthorsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkLanguages", ResourceType = typeof(Translations)), Column("FkLanguages")]
        public int FkLanguages { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(25)]
        [Display(Name = "Pages", ResourceType = typeof(Translations)), Column("Pages")]
        public string Pages { get; set; }

        // References
        [ForeignKey("FkJournalsTranslations")]
        public virtual JournalsTranslations JournalsTranslations { get; set; }

        [ForeignKey("FkAuthorsTranslations")]
        public virtual AuthorsTranslations AuthorsTranslations { get; set; }

        [ForeignKey("FkLanguages")]
        public virtual Languages Languages { get; set; }

    }
}