﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("Organizations")]
    public class Organizations
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkCountries", ResourceType = typeof(Translations)), Column("FkCountries")]
        public int FkCountries { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(100)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        // References
        [ForeignKey("FkCountries")]
        public virtual Countries Countries { get; set; }
    }
}