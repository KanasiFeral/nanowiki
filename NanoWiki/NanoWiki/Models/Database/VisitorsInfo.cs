﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("VisitorsInfo")]
    public class VisitorsInfo
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("VisitorId")]
        public string VisitorId { get; set; }

        [Column("VisitDate")]
        public string VisitDate { get; set; }

        [Column("TableName")]
        public string TableName { get; set; }

        [Column("TableId")]
        public int TableId { get; set; }
    }
}