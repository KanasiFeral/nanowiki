﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("CountriesTranslations")]
    public class CountriesTranslations
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkCountries", ResourceType = typeof(Translations)), Column("FkCountries")]
        public int FkCountries { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkLanguages", ResourceType = typeof(Translations)), Column("FkLanguages")]
        public int FkLanguages { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(100)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        // References
        [ForeignKey("FkCountries")]
        public virtual Countries Countries { get; set; }

        [ForeignKey("FkLanguages")]
        public virtual Languages Languages { get; set; }
    }
}