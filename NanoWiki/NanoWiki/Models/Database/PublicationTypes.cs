﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("PublicationTypes")]
    public class PublicationTypes
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }
    }
}