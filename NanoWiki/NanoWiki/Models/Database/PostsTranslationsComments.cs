﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("PostsTranslationsComments")]
    public class PostsTranslationsComments
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkPostsTranslations", ResourceType = typeof(Translations)), Column("FkPostsTranslations")]
        public int FkPostsTranslations { get; set; }

        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        [Display(Name = "FkAdmins", ResourceType = typeof(Translations)), Column("FkAdmins")]
        public int FkAdmins { get; set; }

        [Display(Name = "FkPostsTranslationsComments", ResourceType = typeof(Translations)), Column("FkPostsTranslationsComments")]
        public int FkPostsTranslationsComments { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "DateComment", ResourceType = typeof(Translations)), Column("DateComment")]
        public string DateComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "TimeComment", ResourceType = typeof(Translations)), Column("TimeComment")]
        public string TimeComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        // References
        [ForeignKey("FkPostsTranslations")]
        public virtual PostsTranslations PostsTranslations { get; set; }
    }
}