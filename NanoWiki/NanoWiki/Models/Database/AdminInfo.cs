﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("AdminInfo")]
    public class AdminInfo
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAdmins", ResourceType = typeof(Translations)), Column("FkAdmins")]
        public int FkAdmins { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAccountTypes", ResourceType = typeof(Translations)), Column("FkAccountTypes")]
        public int FkAccountTypes { get; set; }

        //Don't need "required" property because this field generated automatically by add/edit
        [StringLength(50)]
        [Display(Name = "CreateDate", ResourceType = typeof(Translations)), Column("CreateDate")]
        public string CreateDate { get; set; }

        //Don't need "required" property because this field generated automatically by add/edit
        [StringLength(50)]
        [Display(Name = "CreateTime", ResourceType = typeof(Translations)), Column("CreateTime")]
        public string CreateTime { get; set; }

        [StringLength(255)]
        [Display(Name = "Avatar", ResourceType = typeof(Translations)), Column("Avatar")]
        public string Avatar { get; set; }

        // References
        [ForeignKey("FkAdmins")]
        public virtual Admins Admins { get; set; }

        [ForeignKey("FkAccountTypes")]
        public virtual AccountTypes AccountTypes { get; set; }
    }
}