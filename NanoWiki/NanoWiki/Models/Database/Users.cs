﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("Users")]
    public class Users
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(30)]
        [Display(Name = "Login", ResourceType = typeof(Translations)), Column("Login")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(30)]
        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "InvalidEmail")]
        [Display(Name = "Email", ResourceType = typeof(Translations)), Column("Email")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Password", ResourceType = typeof(Translations)), Column("Password")]
        public string Password { get; set; }

        [StringLength(50)]
        [Display(Name = "DateLink", ResourceType = typeof(Translations)), Column("DateLink")]
        public string DateLink { get; set; }

        [StringLength(50)]
        [Display(Name = "TimeLink", ResourceType = typeof(Translations)), Column("TimeLink")]
        public string TimeLink { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "ConfirmEmail", ResourceType = typeof(Translations)), Column("ConfirmEmail")]
        public int ConfirmEmail { get; set; }

        //Don't need "required" property because this field generated automatically by add/edit
        [Display(Name = "Hash", ResourceType = typeof(Translations)), Column("Hash")]       
        public string Hash { get; set; }
    }
}