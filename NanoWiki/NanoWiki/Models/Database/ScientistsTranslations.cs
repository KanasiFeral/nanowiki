﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("ScientistsTranslations")]
    public class ScientistsTranslations
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkScientists", ResourceType = typeof(Translations)), Column("FkScientists")]
        public int FkScientists { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkLanguages", ResourceType = typeof(Translations)), Column("FkLanguages")]
        public int FkLanguages { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "PostDate", ResourceType = typeof(Translations)), Column("PostDate")]
        public string PostDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Initials", ResourceType = typeof(Translations)), Column("Initials")]
        public string Initials { get; set; }

        [StringLength(15)]
        [Display(Name = "BirthDate", ResourceType = typeof(Translations)), Column("BirthDate")]       
        public string BirthDate { get; set; }

        [StringLength(15)]
        [Display(Name = "DeathDate", ResourceType = typeof(Translations)), Column("DeathDate")]
        public string DeathDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "BirthLocation", ResourceType = typeof(Translations)), Column("BirthLocation")]       
        public string BirthLocation { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]        
        [StringLength(50)]
        [Display(Name = "Nationality", ResourceType = typeof(Translations)), Column("Nationality")]
        public string Nationality { get; set; }

        [Display(Name = "Autobiography", ResourceType = typeof(Translations)), Column("Autobiography")]
        public string Autobiography { get; set; }

        [Display(Name = "Photo", ResourceType = typeof(Translations)), Column("Photo")]
        public string Photo { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "PostStatus", ResourceType = typeof(Translations)), Column("PostStatus")]
        public bool PostStatus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "LikeCount", ResourceType = typeof(Translations)), Column("LikeCount")]
        public int LikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "DislikeCount", ResourceType = typeof(Translations)), Column("DislikeCount")]
        public int DislikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "VisitCount", ResourceType = typeof(Translations)), Column("VisitCount")]
        public int VisitCount { get; set; }

        // References
        [ForeignKey("FkScientists")]
        public virtual Scientists Scientists { get; set; }

        [ForeignKey("FkLanguages")]
        public virtual Languages Languages { get; set; }
    }
}