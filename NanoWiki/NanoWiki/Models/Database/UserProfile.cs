﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("UserProfile")]
    public class UserProfile
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        //Don't need "required" property because this field generated automatically by add/edit
        [StringLength(50)]
        [Display(Name = "CreateDate", ResourceType = typeof(Translations)), Column("CreateDate")]
        public string CreateDate { get; set; }

        //Don't need "required" property because this field generated automatically by add/edit
        [StringLength(50)]
        [Display(Name = "CreateTime", ResourceType = typeof(Translations)), Column("CreateTime")]
        public string CreateTime { get; set; }

        [StringLength(50)]
        [Display(Name = "FirstName", ResourceType = typeof(Translations)), Column("FirstName")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [Display(Name = "LastName", ResourceType = typeof(Translations)), Column("LastName")]
        public string LastName { get; set; }

        [StringLength(50)]
        [Display(Name = "MiddleName", ResourceType = typeof(Translations)), Column("MiddleName")]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [Display(Name = "Address", ResourceType = typeof(Translations)), Column("Address")]
        public string Address { get; set; }

        [StringLength(50)]
        [Display(Name = "City", ResourceType = typeof(Translations)), Column("City")]
        public string City { get; set; }

        [Display(Name = "ZipPostCode", ResourceType = typeof(Translations)), Column("ZipPostCode")]
        public int ZipPostCode { get; set; }

        [StringLength(50)]
        [Display(Name = "StateProvince", ResourceType = typeof(Translations)), Column("StateProvince")]
        public string StateProvince { get; set; }

        [StringLength(50)]
        [Display(Name = "Country", ResourceType = typeof(Translations)), Column("Country")]
        public string Country { get; set; }

        [StringLength(20)]
        [Display(Name = "Phone", ResourceType = typeof(Translations)), Column("Phone")]
        public string Phone { get; set; }

        [Display(Name = "Avatar", ResourceType = typeof(Translations)), Column("Avatar")]
        public string Avatar { get; set; }

        // References
        [ForeignKey("FkUsers")]
        public virtual Users Users { get; set; }
    }
}