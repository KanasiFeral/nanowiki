﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("PostsTranslations")]
    public class PostsTranslations
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkPosts", ResourceType = typeof(Translations)), Column("FkPosts")]
        public int FkPosts { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkLanguages", ResourceType = typeof(Translations)), Column("FkLanguages")]
        public int FkLanguages { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAdmins", ResourceType = typeof(Translations)), Column("FkAdmins")]
        public int FkAdmins { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkTopicsTranslations", ResourceType = typeof(Translations)), Column("FkTopicsTranslations")]
        public int FkTopicsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "PostDate", ResourceType = typeof(Translations)), Column("PostDate")]
        public string PostDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(100)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(600)]
        [Display(Name = "Description", ResourceType = typeof(Translations)), Column("Description")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        [StringLength(255)]
        [Display(Name = "Literature", ResourceType = typeof(Translations)), Column("Literature")]
        public string Literature { get; set; }

        [StringLength(255)]
        [Display(Name = "SourceReference", ResourceType = typeof(Translations)), Column("SourceReference")]
        public string SourceReference { get; set; }

        [Display(Name = "Photo", ResourceType = typeof(Translations)), Column("Photo")]
        public string Photo { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "PostStatus", ResourceType = typeof(Translations)), Column("PostStatus")]
        public bool PostStatus { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "LikeCount", ResourceType = typeof(Translations)), Column("LikeCount")]
        public int LikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "DislikeCount", ResourceType = typeof(Translations)), Column("DislikeCount")]
        public int DislikeCount { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "VisitCount", ResourceType = typeof(Translations)), Column("VisitCount")]
        public int VisitCount { get; set; }

        // References
        [ForeignKey("FkPosts")]
        public virtual Posts Posts { get; set; }

        [ForeignKey("FkLanguages")]
        public virtual Languages Languages { get; set; }

        [ForeignKey("FkAdmins")]
        public virtual Admins Admins { get; set; }
        
        [ForeignKey("FkTopicsTranslations")]
        public virtual TopicsTranslations TopicsTranslations { get; set; }
    }
}