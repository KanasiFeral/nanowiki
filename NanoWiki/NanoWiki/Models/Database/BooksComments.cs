﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("BooksComments")]
    public class BooksComments
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkBooks", ResourceType = typeof(Translations)), Column("FkBooks")]
        public int FkBooks { get; set; }
        
        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }
        
        [Display(Name = "FkAdmins", ResourceType = typeof(Translations)), Column("FkAdmins")]
        public int FkAdmins { get; set; }
        
        [Display(Name = "FkBooksComments", ResourceType = typeof(Translations)), Column("FkBooksComments")]
        public int FkBooksComments { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "DateComment", ResourceType = typeof(Translations)), Column("DateComment")]
        public string DateComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(50)]
        [Display(Name = "TimeComment", ResourceType = typeof(Translations)), Column("TimeComment")]
        public string TimeComment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "Text", ResourceType = typeof(Translations)), Column("Text")]
        public string Text { get; set; }

        // References
        [ForeignKey("FkBooks")]
        public virtual Books Books { get; set; }
    }
}