﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("UserSettings")]
    public class UserSettings
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkLanguages", ResourceType = typeof(Translations)), Column("FkLanguages")]
        public int FkLanguages { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAccountTypes", ResourceType = typeof(Translations)), Column("FkAccountTypes")]
        public int FkAccountTypes { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "EventsNotifications", ResourceType = typeof(Translations)), Column("EventsNotifications")]
        public int EventsNotifications { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "BooksNotifications", ResourceType = typeof(Translations)), Column("BooksNotifications")]
        public int BooksNotifications { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "JournalsNotifications", ResourceType = typeof(Translations)), Column("JournalsNotifications")]
        public int JournalsNotifications { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "ScientistsNotifications", ResourceType = typeof(Translations)), Column("ScientistsNotifications")]
        public int ScientistsNotifications { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "PostsNotifications", ResourceType = typeof(Translations)), Column("PostsNotifications")]
        public int PostsNotifications { get; set; }

        // References
        [ForeignKey("FkUsers")]
        public virtual Users Users { get; set; }

        [ForeignKey("FkLanguages")]
        public virtual Languages Languages { get; set; }

        [ForeignKey("FkAccountTypes")]
        public virtual AccountTypes AccountTypes { get; set; }
    }
}