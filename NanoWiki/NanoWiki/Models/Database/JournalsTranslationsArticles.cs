﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("JournalsTranslationsArticles")]
    public class JournalsTranslationsArticles
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkJournalsTranslations", ResourceType = typeof(Translations)), Column("FkJournalsTranslations")]
        public int FkJournalsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkTopicsTranslations", ResourceType = typeof(Translations)), Column("FkTopicsTranslations")]
        public int FkTopicsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkOrganizationsTranslations", ResourceType = typeof(Translations)), Column("FkOrganizationsTranslations")]
        public int FkOrganizationsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAuthorsTranslations", ResourceType = typeof(Translations)), Column("FkAuthorsTranslations")]
        public int FkAuthorsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Description", ResourceType = typeof(Translations)), Column("Description")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(40)]
        [Display(Name = "ArticleLanguage", ResourceType = typeof(Translations)), Column("ArticleLanguage")]
        public string ArticleLanguage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "ArticleYear", ResourceType = typeof(Translations)), Column("ArticleYear")]
        public int ArticleYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(40)]
        [Display(Name = "Pages", ResourceType = typeof(Translations)), Column("Pages")]
        public string Pages { get; set; }

        // References
        [ForeignKey("FkJournalsTranslations")]
        public virtual JournalsTranslations JournalsTranslations { get; set; }

        [ForeignKey("FkTopicsTranslations")]
        public virtual TopicsTranslations TopicsTranslations { get; set; }

        [ForeignKey("FkOrganizationsTranslations")]
        public virtual OrganizationsTranslations OrganizationsTranslations { get; set; }

        [ForeignKey("FkAuthorsTranslations")]
        public virtual AuthorsTranslations AuthorsTranslations { get; set; }
    }
}