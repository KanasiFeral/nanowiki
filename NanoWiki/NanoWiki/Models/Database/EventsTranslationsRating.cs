﻿using NanoWiki.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Database
{
    [Table("EventsTranslationsRating")]
    public class EventsTranslationsRating
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkEventsTranslations", ResourceType = typeof(Translations)), Column("FkEventsTranslations")]
        public int FkEventsTranslations { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkUsers", ResourceType = typeof(Translations)), Column("FkUsers")]
        public int FkUsers { get; set; }

        [Display(Name = "LikeRating", ResourceType = typeof(Translations)), Column("LikeRating")]
        public bool LikeRating { get; set; }

        [Display(Name = "DislikeRating", ResourceType = typeof(Translations)), Column("DislikeRating")]
        public bool DislikeRating { get; set; }

        // References
        [ForeignKey("FkEventsTranslations")]
        public virtual EventsTranslations EventsTranslations { get; set; }

        [ForeignKey("FkUsers")]
        public virtual Users Users { get; set; }

    }
}