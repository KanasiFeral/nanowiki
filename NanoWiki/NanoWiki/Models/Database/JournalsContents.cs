﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Models.Database
{
    [Table("JournalsContents")]
    public class JournalsContents
    {
        [Display(Name = "Id", ResourceType = typeof(Translations)), Column("Id")]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkJournals", ResourceType = typeof(Translations)), Column("FkJournals")]
        public int FkJournals { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [Display(Name = "FkAuthors", ResourceType = typeof(Translations)), Column("FkAuthors")]
        public int FkAuthors { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Translations)), Column("Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ThisFieldIsRequired")]
        [StringLength(25)]
        [Display(Name = "Pages", ResourceType = typeof(Translations)), Column("Pages")]
        public string Pages { get; set; }

        // References
        [ForeignKey("FkJournals")]
        public virtual Journals Journals { get; set; }

        [ForeignKey("FkAuthors")]
        public virtual Authors Authors { get; set; }

    }
}