﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NanoWiki.Models.Common
{
    [Table("tablesinfo")]
    public class TablesInfo
    {
        [Key]
        [Column("schemaname")]
        public string Schemaname { get; set; }

        [Column("tablename")]
        public string Tablename { get; set; }

        [Column("tableowner")]
        public string Tableowner { get; set; }

        [Column("tablespace")]
        public string Tablespace { get; set; }

        [Column("hasindexes")]
        public bool Hasindexes { get; set; }

        [Column("hasrules")]
        public bool Hasrules { get; set; }

        [Column("hastriggers")]
        public bool Hastriggers { get; set; }
    }
}