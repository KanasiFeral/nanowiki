﻿using NanoWiki.Models.Database;

namespace NanoWiki.Models.Common
{
    public class PersonalPageModel
    {
        public Users Users { get; set; }

        public UserSettings UserSettings { get; set; }

        public UserProfile UserProfile { get; set; }

        public int NumberOfComments { get; set; }

        public FileModel FileModel { get; set; }
    }
}