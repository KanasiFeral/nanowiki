﻿namespace NanoWiki.Models.Common
{
    public class SinglePostModel
    {
        public string Table { get; set; }

        public int Id { get; set; }

        public string Lang { get; set; }
    }
}