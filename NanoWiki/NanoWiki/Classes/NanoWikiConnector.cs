﻿using System;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace NanoWiki.Classes
{
    /// <summary>
    /// DbContext add-in class for working with the database
    /// </summary>
    public static class NanoWikiConnector
    {
        static readonly MethodInfo _setMethod = typeof(DbContext).GetMethod(nameof(DbContext.Set));

        #region Public

        /// <summary>
        /// Executing sql queries
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="query">SQL text query to the database</param>
        /// <returns>Specific table type T data list</returns>
        public static List<T> ExecSQL<T>(string query)
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();

            using var command = context.Database.GetDbConnection().CreateCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            context.Database.OpenConnection();

            List<T> list = new List<T>();

            using (var result = command.ExecuteReader())
            {
                T obj = default;
                while (result.Read())
                {
                    obj = Activator.CreateInstance<T>();
                    foreach (PropertyInfo prop in obj.GetType().GetProperties())
                    {
                        var propName = result[prop.Name].ToString();

                        if (string.IsNullOrEmpty(propName) || propName.Equals("") || Equals(propName, DBNull.Value))
                        {
                            continue;
                        }

                        prop.SetValue(obj, result[prop.Name], null);
                    }
                    list.Add(obj);
                }
            }
            context.Database.CloseConnection();

            return list;
        }

        /// <summary>
        /// Method that displays all records from a table
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <returns>Specific table type T data list</returns>
        public static List<T> Get<T>() where T : class
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();

            return context.Set<T>().AsQueryable().ToList();
        }

        /// <summary>
        /// Method that displays records by one condition from a table
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Specific table type T data list</returns>
        public static List<T> Get<T>(string field, object value) where T : class
        {
            if (string.IsNullOrEmpty(field) || value == null)
                return Get<T>();

            string condition = GenerateSingleCondition(field, value);

            return Get<T>(condition);
        }

        /// <summary>
        /// Method that displays all records from a table with condition
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Specific table type T data list</returns>
        public static List<T> Get<T>(List<Condition> data) where T : class
        {
            if (data == null || data.Count <= 0)
                return Get<T>();

            string condition = GenerateListCondition(data);

            return Get<T>(condition);
        }

        /// <summary>
        /// Method that displays all records from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <returns>Returns records from a table by name</returns>
        public static List<object> Get(string table)
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table)?.ToDynamicList();
        }

        /// <summary>
        /// Method that displays first record by one condition from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Returns records from a table by name</returns>
        public static List<object> Get(string table, string field, object value)
        {
            if (string.IsNullOrEmpty(table))
                return null;

            if (string.IsNullOrEmpty(field) || value == null)
                return Get(table);

            string condition = GenerateSingleCondition(field, value);

            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table).Where(condition)?.ToDynamicList();
        }

        /// <summary>
        /// Method that displays all records from a table by its name with condition
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Returns records from a table by name</returns>
        public static List<object> Get(string table, List<Condition> data)
        {
            if (string.IsNullOrEmpty(table))
                return null;

            if (data == null || data.Count <= 0)
                return Get(table);

            string condition = GenerateListCondition(data);

            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table).Where(condition)?.ToDynamicList();
        }

        /// <summary>
        /// Method that displays all records from a table by its name with full string linq condition
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="condition">Pre-generated fetch condition</param>
        /// <returns>Returns records from a table by name</returns>
        public static List<object> Get(string table, string condition)
        {
            if (string.IsNullOrEmpty(table))
                return null;

            if (string.IsNullOrEmpty(condition))
                return Get(table);

            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table).Where(condition)?.ToDynamicList();
        }

        /// <summary>
        /// Method that return count of records by one condition from a table
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Returning count of records by one condition from a table</returns>
        public static int Count(string table, string field, object value)
        {
            var result = Get(table, field, value);

            return result == null ? 0 : result.Count;
        }

        /// <summary>
        /// Method that return count of records from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Returning count of records from a table by its name</returns>
        public static int Count(string table, List<Condition> data)
        {
            var result = Get(table, data);

            return result == null ? 0 : result.Count;
        }

        /// <summary>
        /// Method that return count of records from a table by its name with full string linq condition
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="condition">Pre-generated fetch condition</param>
        /// <returns>Returning count of records from a table by its name with full string linq condition</returns>
        public static int Count(string table, string condition)
        {
            var result = Get(table, condition);

            return result == null ? 0 : result.Count;
        }

        /// <summary>
        /// Method that displays record by Id from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Record ID in the table</param>
        /// <returns>Returning record by Id from a table by its name</returns>
        public static object GetById(string table, int id)
        {
            if (string.IsNullOrEmpty(table) || id <= 0)
                return null;

            string condition = GenerateSingleCondition("Id", id);

            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table).Where(condition)?.FirstOrDefault();
        }

        /// <summary>
        /// Method that displays record by Id from a table
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="id">Record ID in the table</param>
        /// <returns>Rerurning record by Id from a table</returns>
        public static T GetById<T>(int id) where T : class
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();
            return context.Find<T>(id);
        }

        /// <summary>
        /// Method that displays first record by one condition from a table
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Returning first record by one condition from a table</returns>
        public static T GetSingle<T>(string field, object value) where T : class
        {
            if (string.IsNullOrEmpty(field) || value == null)
                return null;

            string condition = GenerateSingleCondition(field, value);

            return Get<T>(condition)?.FirstOrDefault();
        }

        /// <summary>
        /// Method that displays first record by condition from a table
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Returning first record by condition from a table</returns>
        public static T GetSingle<T>(List<Condition> data) where T : class
        {
            if (data == null || data.Count <= 0)
                return null;

            string condition = GenerateListCondition(data);

            return Get<T>(condition)?.FirstOrDefault();
        }

        /// <summary>
        /// Method that displays first record by one condition from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Returning first record by one condition from a table by its name</returns>
        public static object GetSingle(string table, string field, object value)
        {
            if (string.IsNullOrEmpty(field) || value == null)
                return null;

            string condition = GenerateSingleCondition(field, value);

            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table).Where(condition)?.FirstOrDefault();
        }

        /// <summary>
        /// Method that displays first record by condition from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Returning first record by condition from a table by its name</returns>
        public static object GetSingle(string table, List<Condition> data)
        {
            if (string.IsNullOrEmpty(table))
                return null;

            if (data == null || data.Count <= 0)
                return Get(table);

            string condition = GenerateListCondition(data);

            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table).Where(condition)?.FirstOrDefault();
        }

        /// <summary>
        /// Method that displays first record by condition from a table by its name with full string linq condition
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="condition">Pre-generated fetch condition</param>
        /// <returns>Returning first record by condition from a table by its name with full string linq condition</returns>
        public static object GetSingle(string table, string condition)
        {
            if (string.IsNullOrEmpty(table) || string.IsNullOrEmpty(condition))
                return null;

            using NanoWikiDBContext context = new NanoWikiDBContext();

            return Query(context, table).Where(condition)?.FirstOrDefault();
        }

        /// <summary>
        /// Method returning true or false depending on whether there is data by condition
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Returning true or false depending on whether there is data by condition</returns>
        public static bool Any<T>(List<Condition> data) where T : class
        {
            if (data == null || data.Count <= 0)
                return false;

            var result = Get<T>(data);

            if (result == null || result.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Method returning true or false depending on whether there is data by single condition
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Returning true or false depending on whether there is data by single condition</returns>
        public static bool AnySingle<T>(string field, object value) where T : class
        {
            if (string.IsNullOrEmpty(field) || value == null)
                return false;

            return Get<T>(field, value) != null;
        }

        /// <summary>
        /// Method adding a new row to the table
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="model">Database model with data</param>
        /// <returns>Returning result of adding a record to the database</returns>
        public static bool Insert<T>(T model)
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();

            try
            {
                context.Add(model);
                context.SaveChanges();

                return true;
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Method changing a row in a table
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="model">Database model with data</param>
        /// <returns>Returning result of updating a record to the database</returns>
        public static bool Update<T>(T model)
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();

            try
            {
                context.Update(model);
                context.SaveChanges();

                return true;
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Method that removes a row from a table by Id
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Record ID in the table</param>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Returning result of updating a record to the database</returns>
        public static bool Update(string table, int id, string field, object value)
        {
            if (string.IsNullOrEmpty(table) || id <= 0 || string.IsNullOrEmpty(field) || value == null)
                return false;

            using NanoWikiDBContext context = new NanoWikiDBContext();

            try
            {
                object model = GetById(table, id);
                model.GetType().GetProperty(field).SetValue(model, value);

                context.Update(model);
                context.SaveChanges();

                return true;
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Method that removes a row from a table by its name
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="model">Database model with data</param>
        /// <returns>Returning result of deleting a record to the database</returns>
        public static bool Delete<T>(T model)
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();

            try
            {
                context.Remove(model);
                context.SaveChanges();

                return true;
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Method that returns field value by one condition from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <returns>Returning result of deleting a record to the database</returns>
        public static bool Delete(string table)
        {
            if (string.IsNullOrEmpty(table))
                return false;

            using NanoWikiDBContext context = new NanoWikiDBContext();

            try
            {
                var records = Get(table);

                context.RemoveRange(records);
                context.SaveChanges();

                return true;
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Method that returns field value by id from a table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="field">Table field</param>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Returning field value by id from a table by its name</returns>
        public static string GetValue(string table, string field, List<Condition> data)
        {
            if (string.IsNullOrEmpty(table) || string.IsNullOrEmpty(field) || data == null || data.Count <= 0)
                return null;

            var model = GetSingle(table, data);

            return GetValue(model, field);
        }

        /// <summary>
        /// Method that return max id from table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="field">Table field</param>
        /// <param name="id">Record ID in the table</param>
        /// <returns>Returning max id from table by its name</returns>
        public static string GetValueById(string table, string field, int id)
        {
            if (string.IsNullOrEmpty(table) || string.IsNullOrEmpty(field) || id <= 0)
                return null;

            var model = GetById(table, id);

            return GetValue(model, field);
        }

        /// <summary>
        /// Method that return max id value from table by its name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <returns>Returning max id value from table by its name</returns>
        public static string MaxId(string table)
        {
            string result = "";

            if (string.IsNullOrEmpty(table))
                return result;

            try
            {
                var list = Get(table);

                if (list != null && list.Count() > 0)
                    result = list.Max(x => x.GetType().GetProperty("Id").GetValue(x, null)).ToString();
            }
            catch (NullReferenceException)
            {
                result = "0";
            }
            catch (Exception)
            {
                result = "";
            }

            return result.ToString();
        }

        /// <summary>
        /// Method that returns the maximum table field by table name
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="field">Table field</param>
        /// <returns>Returning max field value from table by its name</returns>
        public static string Max(string table, string field)
        {
            string result = "";

            if (string.IsNullOrEmpty(table))
                return null;

            try
            {
                var list = Get(table);

                if (list != null && list.Count() > 0)
                    result = list.Max(x => x.GetType().GetProperty(field).GetValue(x, null)).ToString();

                if (result == null)
                    return null;

                return result.ToString();
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return null;
            }
        }

        /// <summary>
        /// Method that returns the maximum id of a table by condition
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="field">Table field</param>
        /// <param name="value">Field value</param>
        /// <returns></returns>
        public static string Max(string table, string field, string value)
        {
            string result = "";

            if (string.IsNullOrEmpty(table))
                return null;

            try
            {
                var list = Get(table, field, value);

                if (list != null && list.Count() > 0)
                    result = list.Max(x => x.GetType().GetProperty("Id").GetValue(x, null)).ToString();

                if (result == null)
                    return null;

                return result.ToString();
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return null;
            }
        }

        #endregion

        #region Private

        /// <summary>
        /// Method that generate condition by data
        /// </summary>
        /// <param name="field">Table field</param>
        /// <param name="value">Table value</param>
        /// <returns>Generated linq condition as a string by single condition</returns>
        private static string GenerateSingleCondition(string field, object value)
        {
            string condition = "";

            if (value.GetType() == typeof(int) || value.GetType() == typeof(double))
                condition += $"{field} = {value} && ";
            else if (string.IsNullOrEmpty(field))
                condition += $"{field} && ";
            else
                condition += $"{field} = \"{value}\" && ";

            condition = condition.Substring(0, condition.LastIndexOf(" &&"));

            return condition;
        }

        /// <summary>
        /// Method that returns a list of values by condition
        /// </summary>
        /// <param name="data">Key-value condition list</param>
        /// <returns>Generated linq condition as a string by condition list</returns>
        private static string GenerateListCondition(List<Condition> data)
        {
            string condition = "";

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].Value != null)
                {
                    if (data[i].Value.GetType() == typeof(int) || data[i].Value.GetType() == typeof(double))
                    {
                        condition += $"{data[i].Field} = {data[i].Value} && ";
                    }
                    else if (string.IsNullOrEmpty(data[i].Field))
                    {
                        condition += $"{data[i].Value} && ";
                    }
                    else
                    {
                        condition += $"{data[i].Field} = \"{data[i].Value}\" && ";
                    }
                }
            }

            condition = condition.Substring(0, condition.LastIndexOf(" &&"));

            return condition;
        }

        /// <summary>
        /// Invoking the generic Set<T> method via reflection using string param
        /// </summary>
        /// <typeparam name="T">Type of database table (model)</typeparam>
        /// <param name="condition">Pre-generated fetch condition</param>
        /// <returns>Specific table type T data list</returns>
        private static List<T> Get<T>(string condition) where T : class
        {
            using NanoWikiDBContext context = new NanoWikiDBContext();

            try
            {
                var result = context.Set<T>().AsQueryable().Where(condition).ToList();

                return result;
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return null;
            }
        }

        /// <summary>
        /// Extension method. Invoking the generic Set<T> method via reflection using type param
        /// </summary>
        /// <param name="context">Class object for working with the database</param>
        /// <param name="entityName">Table name</param>
        /// <returns>Result of a database query with a LINQ condition as a string</returns>
        private static IQueryable Query(this DbContext context, string entityName)
        {
            if (context == null || string.IsNullOrEmpty(entityName))
                return null;

            try
            {
                var entityType = context.Model.FindEntityType($"{GlobalValues.GetModelsAllias()}{entityName}");

                if (entityType != null)
                {
                    return context.Query(entityType.ClrType);
                }
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return null;
            }

            return null;
        }

        /// <summary>
        /// Extension method. Invoking the generic Set<T> method via reflection using type param
        /// </summary>
        /// <param name="context">Class object for working with the database</param>
        /// <param name="entityType">Table name</param>
        /// <returns>Result of a database query with a LINQ condition as a string</returns>
        private static IQueryable Query(this DbContext context, Type entityType)
        {
            if (context == null || entityType == null)
                return null;

            try
            {
                return (IQueryable)_setMethod.MakeGenericMethod(entityType).Invoke(context, null);
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return null;
            }
        }

        /// <summary>
        /// Method that returns a value from an object
        /// </summary>
        /// <param name="model">Database model with data</param>
        /// <param name="field">Table field</param>
        /// <returns>Data from record by field name</returns>
        private static string GetValue(object model, string field)
        {
            object result;

            if (model == null || string.IsNullOrEmpty(field))
                return null;

            try
            {
                result = model.GetType().GetProperty(field)?.GetValue(model);
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return null;
            }

            return result == null ? "" : result.ToString();
        }

        #endregion
    }
}