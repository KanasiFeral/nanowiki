﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace NanoWiki.Classes
{
    public class Encryption
    {
        /// <summary>
        /// Encrypts any data transmitted as a string. In the future, it may be possible to replace a static key with a dynamic
        /// </summary>
        /// <param name="text">Plain text for encryption</param>
        /// <returns>Cipher text</returns>
        public static string Encrypt(string text/*, string keyString*/)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            string key = "U3s9Jjst4V0SRnTUPjR3SDF";

            try
            {
                var buffer = Encoding.UTF8.GetBytes(text);
                var hash = new SHA512CryptoServiceProvider();
                var aesKey = new byte[24];
                Buffer.BlockCopy(hash.ComputeHash(Encoding.UTF8.GetBytes(key)), 0, aesKey, 0, 24);

                using var aes = Aes.Create();
                if (aes == null)
                    return null;

                aes.Key = aesKey;

                using var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using var resultStream = new MemoryStream();
                using (var aesStream = new CryptoStream(resultStream, encryptor, CryptoStreamMode.Write))
                using (var plainStream = new MemoryStream(buffer))
                {
                    plainStream.CopyTo(aesStream);
                }

                var result = resultStream.ToArray();
                var combined = new byte[aes.IV.Length + result.Length];
                Array.ConstrainedCopy(aes.IV, 0, combined, 0, aes.IV.Length);
                Array.ConstrainedCopy(result, 0, combined, aes.IV.Length, result.Length);

                return Convert.ToBase64String(combined);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Decrypts data encrypted with this class as a string. In the future, it may be possible to replace a static key with a dynamic
        /// </summary>
        /// <param name="encryptedText">Cipher text</param>
        /// <returns>Ciphertext to decrypt and return plain text</returns>
        public static string Decrypt(string encryptedText/*, string keyString*/)
        {
            if (string.IsNullOrEmpty(encryptedText))
                return null;

            string key = "U3s9Jjst4V0SRnTUPjR3SDF";

            try
            {
                var combined = Convert.FromBase64String(encryptedText);
                var buffer = new byte[combined.Length];
                var hash = new SHA512CryptoServiceProvider();
                var aesKey = new byte[24];
                Buffer.BlockCopy(hash.ComputeHash(Encoding.UTF8.GetBytes(key)), 0, aesKey, 0, 24);

                using var aes = Aes.Create();
                if (aes == null)
                    return null;

                aes.Key = aesKey;

                var iv = new byte[aes.IV.Length];
                var ciphertext = new byte[buffer.Length - iv.Length];

                Array.ConstrainedCopy(combined, 0, iv, 0, iv.Length);
                Array.ConstrainedCopy(combined, iv.Length, ciphertext, 0, ciphertext.Length);

                aes.IV = iv;

                using var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using var resultStream = new MemoryStream();
                using (var aesStream = new CryptoStream(resultStream, decryptor, CryptoStreamMode.Write))
                using (var plainStream = new MemoryStream(ciphertext))
                {
                    plainStream.CopyTo(aesStream);
                }

                return Encoding.UTF8.GetString(resultStream.ToArray());
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}