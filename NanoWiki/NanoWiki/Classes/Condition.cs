﻿namespace NanoWiki.Classes
{
    public class Condition
    {
        public string Field { get; set; }

        public object Value { get; set; }
    }
}