﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NanoWiki.Models.Database;

namespace NanoWiki.Classes
{
    /// <summary>
    /// Class providing counting the number of visits to the site
    /// </summary>
    public class VisitorCounterMiddleware
    {
        private readonly RequestDelegate _requestDelegate;

        /// <summary>
        /// Constructor accepting a request delegate object
        /// </summary>
        /// <param name="requestDelegate">Site request</param>
        public VisitorCounterMiddleware(RequestDelegate requestDelegate) => _requestDelegate = requestDelegate;

        /// <summary>
        /// Counting the number of visits to the site, writing to the database and saving the current session in cookies
        /// </summary>
        /// <param name="context">Cookie context</param>
        /// <returns>Delegate with an httpContext object</returns>
        public async Task Invoke(HttpContext context)
        {
            // Get user Id
            string visitorId = context.Request.Cookies["VisitorId"];

            // If the site opens for the first time
            if (visitorId == null)
            {
                // Get server date
                var currentDate = DateTime.Now.Date.ToShortDateString();

                // Get all list of visits
                var totalVisit = NanoWikiConnector.GetSingle<TotalVisitCount>("VisitDate", currentDate);

                // If the site opens today for the first time
                if (totalVisit == null)
                {
                    int.TryParse(NanoWikiConnector.MaxId("TotalVisitCount"), out int maxId);

                    maxId++;

                    NanoWikiConnector.Insert(new TotalVisitCount()
                    {
                        Id = maxId,
                        VisitCount = 1,
                        VisitDate = currentDate
                    });
                }
                else
                {
                    totalVisit.VisitCount++;
                    NanoWikiConnector.Update(totalVisit);
                }

                // Set cookie
                context.Response.Cookies.Append("VisitorId", Guid.NewGuid().ToString(), new CookieOptions()
                {
                    Path = "/",
                    HttpOnly = true,
                    Secure = false,
                });
            }

            await _requestDelegate(context);
        }
    }
}