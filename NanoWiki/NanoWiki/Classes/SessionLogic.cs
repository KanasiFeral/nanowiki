﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace NanoWiki.Classes
{
    public static class SessionLogic
    {
        /// <summary>
        /// Method "Set" accepts an ISession object, a string key and a value of type T. Then the data obtained using Json convector enter the data
        /// </summary>
        /// <typeparam name="T">Type of saving object</typeparam>
        /// <param name="session">Session object</param>
        /// <param name="key">Session name</param>
        /// <param name="value">Session value</param>
        public static void Set<T>(this ISession session, string key, T value) => session.SetString(key, JsonConvert.SerializeObject(value));

        /// <summary>
        /// Method "Get" accepts an ISession object and a string key. It then gets the value from the session by key. If the value obtained is empty, then we return the standard T, otherwise, using the Json convector, we return the value obtained as a string
        /// </summary>
        /// <typeparam name="T">Type of saving object</typeparam>
        /// <param name="session">Session object</param>
        /// <param name="key">Session name</param>
        /// <returns>Deserialized json data from the session by key</returns>
        public static T Get<T>(this ISession session, string key)
        {
            // Getting the value
            var value = session.GetString(key);
            // Return value
            return value == null ? default : JsonConvert.DeserializeObject<T>(value);
        }
    }
}