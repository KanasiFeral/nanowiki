﻿using Microsoft.EntityFrameworkCore;
using NanoWiki.Models.Common;
using NanoWiki.Models.Database;

namespace NanoWiki.Classes
{
    /// <summary>
    /// Entity Framework class for working with a database, according to the database first principle
    /// </summary>
    public class NanoWikiDBContext : DbContext
    {
        public DbSet<AboutProject> aboutProjects { get; set; }

        public DbSet<AccountTypes> accountTypes { get; set; }

        public DbSet<AccountTypesTranslations> accountTypesTranslations { get; set; }

        public DbSet<AdminInfo> adminInfos { get; set; }

        public DbSet<Admins> admins { get; set; }

        public DbSet<Authors> authors { get; set; }

        public DbSet<AuthorsTranslations> authorsTranslations { get; set; }

        public DbSet<Books> books { get; set; }

        public DbSet<BooksComments> booksComments { get; set; }

        public DbSet<BooksRating> booksRatings { get; set; }

        public DbSet<BooksTranslations> booksTranslations { get; set; }

        public DbSet<BooksTranslationsComments> booksTranslationsComments { get; set; }

        public DbSet<BooksTranslationsRating> booksTranslationsRatings { get; set; }

        public DbSet<Cities> cities { get; set; }

        public DbSet<CitiesTranslations> citiesTranslations { get; set; }

        public DbSet<Contacts> contacts { get; set; }

        public DbSet<Countries> countries { get; set; }

        public DbSet<CountriesTranslations> countriesTranslations { get; set; }

        public DbSet<EmailList> emailLists { get; set; }

        public DbSet<Events> events { get; set; }

        public DbSet<EventsComments> eventsComments { get; set; }

        public DbSet<EventsRating> eventsRatings { get; set; }

        public DbSet<EventsTranslations> eventsTranslations { get; set; }

        public DbSet<EventsTranslationsComments> eventsTranslationsComments { get; set; }

        public DbSet<EventsTranslationsRating> eventsTranslationsRatings { get; set; }

        public DbSet<Journals> journals { get; set; }

        public DbSet<JournalsArticles> journalsArticles { get; set; }

        public DbSet<JournalsComments> journalsComments { get; set; }

        public DbSet<JournalsContents> journalsContents { get; set; }

        public DbSet<JournalsPublications> journalsPublications { get; set; }

        public DbSet<JournalsRating> journalsRatings { get; set; }

        public DbSet<JournalsTranslations> journalsTranslations { get; set; }

        public DbSet<JournalsTranslationsArticles> journalsTranslationsArticles { get; set; }

        public DbSet<JournalsTranslationsComments> journalsTranslationsComments { get; set; }

        public DbSet<JournalsTranslationsContents> journalsTranslationsContents { get; set; }

        public DbSet<JournalsTranslationsPublications> journalsTranslationsPublications { get; set; }

        public DbSet<JournalsTranslationsRating> journalsTranslationsRatings { get; set; }

        public DbSet<Languages> languages { get; set; }

        public DbSet<Materials> materials { get; set; }

        public DbSet<MaterialsTranslations> materialsTranslations { get; set; }

        public DbSet<MaterialsTypes> materialsTypes { get; set; }

        public DbSet<MaterialsTypesTranslations> materialsTypesTranslations { get; set; }

        public DbSet<Organizations> organizations { get; set; }

        public DbSet<OrganizationsTranslations> organizationsTranslations { get; set; }

        public DbSet<Posts> posts { get; set; }

        public DbSet<PostsComments> postsComments { get; set; }

        public DbSet<PostsRating> postsRatings { get; set; }

        public DbSet<PostsTranslations> postsTranslations { get; set; }

        public DbSet<PostsTranslationsComments> postsTranslationsComments { get; set; }

        public DbSet<PostsTranslationsRating> postsTranslationsRatings { get; set; }

        public DbSet<PublicationTypes> publicationTypes { get; set; }

        public DbSet<PublicationTypesTranslations> publicationTypesTranslations { get; set; }

        public DbSet<Scientists> scientists { get; set; }

        public DbSet<ScientistsComments> scientistsComments { get; set; }

        public DbSet<ScientistsRating> scientistsRatings { get; set; }

        public DbSet<ScientistsTranslations> scientistsTranslations { get; set; }

        public DbSet<ScientistsTranslationsComments> scientistsTranslationsComments { get; set; }

        public DbSet<ScientistsTranslationsRating> scientistsTranslationsRatings { get; set; }

        public DbSet<Tags> tags { get; set; }

        public DbSet<TestTable> testTables { get; set; }

        public DbSet<Topics> topics { get; set; }

        public DbSet<TopicsTranslations> topicsTranslations { get; set; }

        public DbSet<UserProfile> userProfiles { get; set; }

        public DbSet<Users> users { get; set; }

        public DbSet<UserSettings> userSettings { get; set; }

        public DbSet<TotalVisitCount> totalVisitCounts { get; set; }

        public DbSet<VisitorsInfo> visitorsInfos { get; set; }

        public DbSet<TablesInfo> TablesInfos { get; set; }

        /// <summary>
        /// Works with the database connection string and initializes the connection
        /// </summary>
        /// <param name="optionsBuilder">Object for initializing the class in the system</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Localhost or Server
            var computerType = GlobalValues.GetAppSettingsString("ConnectionStrings:ComputerType");

            var connectionPath = GlobalValues.GetAppSettingsString(computerType.Equals("Localhost") ? "ConnectionStrings:LocalhostConnection" : "ConnectionStrings:ServerhostConnection");

            string password;

            if (computerType.Equals("Localhost"))
            {
                password = GlobalValues.GetAppSettingsString("ConnectionStrings:LocalhostPassword");
            }
            else
            {
                password = Encryption.Decrypt(GlobalValues.GetAppSettingsString("ConnectionStrings:ServerhostPassword"));
            }

            connectionPath = connectionPath.Replace("*", password);

            optionsBuilder.UseNpgsql(connectionPath);
        }
    }
}