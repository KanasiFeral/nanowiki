﻿using Microsoft.Extensions.Configuration;
using NanoWiki.Managers;
using NanoWiki.Resources;

namespace NanoWiki.Classes
{
    public static class GlobalValues
    {
        /// <summary>
        /// Getting the status of sending a message to the mail
        /// </summary>
        public static bool SendMessage { get; set; }

        /// <summary>
        /// Getting a multilingual description of the current status of the post based on its digital value
        /// </summary>
        /// <param name="status">Status value</param>
        /// <returns>Translation text</returns>
        public static string GetNotificationStatus(int status) => status == 1 ? Translations.Notify : Translations.DontNotify;

        /// <summary>
        /// Methos that return database project allias path
        /// </summary>
        /// <returns>Get allias to the model</returns>
        public static string GetModelsAllias() => $"{StringsManager.GetProjectName()}.Models.Database.";

        /// <summary>
        /// Sql query to get the current list of tables from the database
        /// </summary>
        /// <returns>SQL query for getting list of tables</returns>
        public static string SqlQueryTables() => "SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema' AND tablename != '__EFMigrationsHistory' AND tableName != 'tablesinfo' AND tableName != 'VisitorsInfo'";

        /// <summary>
        /// Get appsettings string by name from XML config file
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Value from appsettings XML config file</returns>
        public static string GetAppSettingsString(string name)
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var appString = config[name];

            return appString;
        }
    }
}