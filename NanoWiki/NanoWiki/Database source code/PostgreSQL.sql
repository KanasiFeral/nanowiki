/*database source| HeiLong*/
/*Created by Asinouski Kanstantsin © NanoWiki.net*/

create database NanoWiki;
use NanoWiki;

/* last update 10/19/2020 8:00 PM */
create table if not exists "Languages"
(
	"Id" serial primary key not null,
	"FullName" varchar(50) not null,
	"Abbreviation" varchar(5) not null,
	"Description" text not null,
	"DateFormat" varchar(20) not null,
	"TimeFormat" varchar(20) not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Contacts"
(
	"Id" serial primary key not null,
	"FkLanguages" int not null,
	"Address" varchar(255) not null,
	"Phone" varchar(255) not null,
	"Email" varchar(255) not null,
	foreign key("FkLanguages") references "Languages"("Id") on update cascade on delete cascade	
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "AboutProject"
(
	"Id" serial primary key not null,
	"FkLanguages" int not null,
	"Header" varchar(100) not null,
	"Text" text not null,	
	foreign key("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Admins"
(
	"Id" serial primary key not null,
	"Login" varchar(30) not null,
	"Email" varchar(50) not null,
	"Password" varchar(255) not null,	
	"DateLink" varchar(50) null,
	"TimeLink" varchar(50) null,
	"Hash" text not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Users"
(
	"Id" serial primary key not null,
	"Login" varchar(30) not null,
	"Email" varchar(30) not null,
	"Password" varchar(255) not null,
	"DateLink" varchar(50) null,
	"TimeLink" varchar(50) null,	
	"ConfirmEmail" int not null,
	"Hash" text not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "AccountTypes"
(
	"Id" serial primary key not null,
	"Name" varchar(50) not null,
	"Description" text not null,
	"AccessLvl" int not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "AccountTypesTranslations"
(
	"Id" serial primary key not null,
	"FkAccountTypes" int not null,
	"FkLanguages" int not null,
	"Name" varchar(50) not null,
	"Description" text not null,
	foreign key("FkAccountTypes") references public."AccountTypes"("Id") on update cascade on delete cascade,
	foreign key("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade	
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "AdminInfo"
(
	"Id" serial primary key not null,
	"FkAdmins" int not null,
	"FkAccountTypes" int not null,
	"CreateDate" varchar(50) not null,
	"CreateTime" varchar(50) not null,
	"Avatar" varchar(255) null,
	foreign key("FkAdmins") references public."Admins"("Id") on update cascade on delete cascade,
	foreign key("FkAccountTypes") references public."AccountTypes"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "UserProfile"
(
	"Id" serial primary key not null,
	"FkUsers" int not null,
	"CreateDate" varchar(50) not null,
	"CreateTime" varchar(50) not null,
	"FirstName" varchar(50) null,
	"LastName" varchar(50) null,
	"MiddleName" varchar(50) null,
	"Address" varchar(255) null,
	"City" varchar(50) null,
	"ZipPostCode" int null,
	"StateProvince" varchar(50) null,
	"Country" varchar(50) null,
	"Phone" varchar(20) null,
	"Avatar" varchar(255) null,
	foreign key("FkUsers") references public."Users"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "UserSettings"
(
	"Id" serial primary key not null,
	"FkUsers" int not null,
	"FkLanguages" int not null,
	"FkAccountTypes" int not null,
	"EventsNotifications" int not null,	
	"BooksNotifications" int not null,
	"JournalsNotifications" int not null,
	"ScientistsNotifications" int not null,	
	"PostsNotifications" int not null,	
	foreign key("FkUsers") references public."Users"("Id") on update cascade on delete cascade,
	foreign key("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade,
	foreign key("FkAccountTypes") references public."AccountTypes"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Tags"
(
	"Id" serial primary key not null,
	"Name" varchar(20) not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Topics"
(
	"Id" serial primary key not null,
	"Name" varchar(255) not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "TopicsTranslations"
(
	"Id" serial primary key not null,
	"FkTopics" int not null,
	"FkLanguages" int not null,
	"Name" varchar(255) not null,
	foreign key ("FkTopics") references public."Topics"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "Posts"
(
	"Id" serial primary key not null,
	"FkAdmins" int not null,
	"FkTopics" int not null,
	"PostDate" varchar(50) not null,
	"Name" varchar(100) not null,
	"Description" text not null,
	"Text" text not null,
	"Literature" varchar(255) null,
	"SourceReference" text null,
	"Tags" text null,
	"Photo" text null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,
	"VisitCount" int not null,
	foreign key ("FkAdmins") references public."Admins"("Id") on update cascade on delete cascade,
	foreign key ("FkTopics") references public."Topics"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "PostsComments"
(
	"Id" serial primary key not null,
	"FkPosts" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkPostsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	Text text not null,
	foreign key ("FkPosts") references public."Posts"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "PostsRating"
(
	"Id" serial primary key not null,
	"FkPosts" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkPosts") references public."Posts"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "PostsTranslations"
(
	"Id" serial primary key not null, 
	"FkPosts" int not null,
	"FkLanguages" int not null,
	"FkAdmins" int not null,
	"FkTopicsTranslations" int not null,
	"PostDate" varchar(50) not null,
	"Name" varchar(100) not null,
	"Description" text not null,
	"Text" text not null,
	"Literature" varchar(255) null,
	"SourceReference" text null,
	"Photo" text null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,	
	"VisitCount" int not null,
	foreign key ("FkPosts") references public."Posts"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade,
	foreign key ("FkAdmins") references public."Admins"("Id") on update cascade on delete cascade,
	foreign key ("FkTopicsTranslations") references public."TopicsTranslations"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "PostsTranslationsComments"
(
	"Id" serial primary key not null,
	"FkPostsTranslations" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkPostsTranslationsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkPostsTranslations") references public."PostsTranslations"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "PostsTranslationsRating"
(
	"Id" serial primary key not null,
	"FkPostsTranslations" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkPostsTranslations") references public."PostsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "Scientists"
(
	"Id" serial primary key not null,
	"PostDate" varchar(50) not null,
	"Initials" varchar(255) not null,
	"BirthDate" varchar(50) null,
	"DeathDate" varchar(50) null,
	"BirthLocation" varchar(255) not null,
	"Nationality" varchar(50) not null,	
	"Autobiography" text null,
	"Photo" text null,
	"Phone" varchar(255) null,
	"Email" varchar(255) null,
	"SocialNetwork" text null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,
	"VisitCount" int not null,
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "ScientistsComments"
(
	"Id" serial primary key not null,
	"FkScientists" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkScientistsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkScientists") references public."Scientists"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "ScientistsRating"
(
	"Id" serial primary key not null,
	"FkScientists" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkScientists") references public."Scientists"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "ScientistsTranslations"
(
	"Id" serial primary key not null, 
	"FkScientists" int not null,
	"FkLanguages" int not null,
	"PostDate" varchar(50) not null,
	"Initials" varchar(255) not null,
	"BirthDate" varchar(50) null,
	"DeathDate" varchar(50) null,
	"BirthLocation" varchar(255) not null,
	"Nationality" varchar(50) not null,	
	"Autobiography" text null,
	"Photo" text null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,	
	"VisitCount" int not null,
	foreign key ("FkScientists") references public."Scientists"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "ScientistsTranslationsComments"
(
	"Id" serial primary key not null,
	"FkScientistsTranslations" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkScientistsTranslationsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkScientistsTranslations") references "ScientistsTranslations"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "ScientistsTranslationsRating"
(
	"Id" serial primary key not null,
	"FkScientistsTranslations" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkScientistsTranslations") references public."ScientistsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "PublicationTypes"
(
	"Id" serial primary key not null,
	"Name" varchar(255) not null
);

/* last update 1/25/2021 9:42 PM */
create table if not exists "PublicationTypesTranslations"
(
	"Id" serial primary key not null,
	"FkPublicationTypes" int not null,
	"FkLanguages" int not null,
	"Name" varchar(255) not null,
	foreign key ("FkPublicationTypes") references public."PublicationTypes"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Authors"
(
	"Id" serial primary key not null,
	"Initials" varchar(255) not null,
	"BirthDate" varchar(50) null,
	"DeathDate" varchar(50) null,
	"BirthLocation" varchar(255) null,
	"Nationality" varchar(50) null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "AuthorsTranslations"
(
	"Id" serial primary key not null,
	"FkAuthors" int not null,
	"FkLanguages" int not null,
	"Initials" varchar(255) not null,
	"BirthDate" varchar(50) null,
	"DeathDate" varchar(50) null,
	"BirthLocation" varchar(255) null,
	"Nationality" varchar(50) null,
	foreign key ("FkAuthors") references public."Authors"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "Books"
(
	"Id" serial primary key not null,
	"FkAuthors" int not null,
	"FkTopics" int not null,
	"PostDate" varchar(50) not null,
	"Photo" text null,
	"BookLanguage" varchar(40) not null,
	"Name" varchar(255) not null,	
	"PublishingHouse" varchar(255) not null,
	"ISBN" varchar(255) null,
	"Year" int null,
	"PageCount" int not null,
	"Description" varchar(120) not null,
	"Text" text not null,
	"Shops" text null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,
	"VisitCount" int not null,
	foreign key ("FkAuthors") references public."Authors"("Id") on update cascade on delete cascade,
	foreign key ("FkTopics") references public."Topics"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "BooksComments"
(
	"Id" serial primary key not null,
	"FkBooks" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkBooksComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkBooks") references public."Books"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "BooksRating"
(
	"Id" serial primary key not null,
	"FkBooks" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkBooks") references public."Books"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "BooksTranslations"
(
	"Id" serial primary key not null, 
	"FkBooks" int not null,
	"FkAuthorsTranslations" int not null,
	"FkTopicsTranslations" int not null,
	"FkLanguages" int not null,
	"PostDate" varchar(50) not null,
	"Photo" text null,
	"BookLanguage" varchar(40) not null,
	"Name" varchar(255) not null,	
	"PublishingHouse" varchar(255) not null,
	"ISBN" varchar(255) null,
	"Description" varchar(120) not null,
	"Text" text not null,
	"Shops" text null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,	
	"VisitCount" int not null,
	foreign key ("FkBooks") references public."Books"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthorsTranslations") references public."AuthorsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkTopicsTranslations") references public."TopicsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "BooksTranslationsComments"
(
	"Id" serial primary key not null,
	"FkBooksTranslations" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkBooksTranslationsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkBooksTranslations") references public."BooksTranslations"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "BooksTranslationsRating"
(
	"Id" serial primary key not null,
	"FkBooksTranslations" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkBooksTranslations") references public."BooksTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "MaterialsTypes"
(
	"Id" serial primary key not null,
	"Name" varchar(100) not null,
	"Description" text null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "MaterialsTypesTranslations"
(
	"Id" serial primary key not null,
	"FkMaterialsTypes" int not null,
	"FkLanguages" int not null,
	"Name" varchar(100) not null,
	"Description" text null,
	foreign key ("FkMaterialsTypes") references public."MaterialsTypes"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Materials"
(
	"Id" serial primary key not null,
	"FkMaterialsTypes" int not null,
	"Name" varchar(100) not null,
	"Description" text null,
	"PathFile" varchar(255) not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "MaterialsTranslations"
(
	"Id" serial primary key not null,
	"FkMaterialsTypesTranslations" int not null,
	"FkLanguages" int not null,
	"Name" varchar(100) not null,
	"Description" text null,
	"PathFile" varchar(255) not null,
	foreign key ("FkMaterialsTypesTranslations") references public."MaterialsTypesTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "EmailList"
(
	"Id" serial primary key not null,
	"FkLanguages" int not null,
	"Email" varchar(100) not null,
	"CreateDate" varchar(50) not null,
	"CreateTime" varchar(50) not null,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "TestTable"
(
	"Id" serial primary key not null,
	"Key" varchar(100) not null,
	"Value" varchar(100) not null
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Countries"
(
	"Id" serial primary key not null,
	"Name" varchar(100) not null
);
 
/* last update 10/19/2020 8:00 PM */
create table if not exists "CountriesTranslations"
(
	"Id" serial primary key not null,
	"FkCountries" int not null,
	"FkLanguages" int not null,
	"Name" varchar(100) not null,
	foreign key ("FkCountries") references public."Countries"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Cities"
(
	"Id" serial primary key not null,
	"FkCountries" int not null,
	"Name" varchar(100) not null,
	foreign key ("FkCountries") references public."Countries"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "CitiesTranslations"
(
	"Id" serial primary key not null,
	"FkCities" int not null,
	"FkCountriesTranslations" int not null,
	"FkLanguages" int not null,
	"Name" varchar(100) not null,
	foreign key ("FkCities") references public."Cities"("Id") on update cascade on delete cascade,
	foreign key ("FkCountriesTranslations") references public."CountriesTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "Organizations"
(
	"Id" serial primary key not null,
	"FkCountries" int not null,
	"Name" varchar(100) not null,
	"Text" text not null,
	foreign key ("FkCountries") references public."Countries"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "OrganizationsTranslations"
(
	"Id" serial primary key not null,
	"FkCountriesTranslations" int not null,
	"FkLanguages" int not null,
	"Name" varchar(100) not null,
	"Text" text not null,
	foreign key ("FkCountriesTranslations") references public."CountriesTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "Events"
(
	"Id" serial primary key not null,
	"FkCountries" int not null,
	"PostDate" varchar(50) not null,
	"Photo" text null,
	"Name" varchar(255) not null,
	"Description" varchar(255) not null,
	"EventDate" varchar(50) not null,
	"EventTime" varchar(50) not null,
	"Place" varchar(100) not null,
	"EventEntrance" varchar(50) not null,
	"Text" text not null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,	
	"VisitCount" int not null,
	foreign key ("FkCountries") references public."Countries"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "EventsComments"
(
	"Id" serial primary key not null,
	"FkEvents" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkEventsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkEvents") references public."Events"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "EventsRating"
(
	"Id" serial primary key not null,
	"FkEvents" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkEvents") references public."Events"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "EventsTranslations"
(
	"Id" serial primary key not null,
	"FkEvents" int not null,
	"FkCountriesTranslations" int not null,
	"FkLanguages" int not null,	
	"PostDate" varchar(50) not null,
	"Photo" text null,
	"Name" varchar(255) not null,
	"Description" varchar(255) not null,
	"EventDate" varchar(50) not null,
	"EventTime" varchar(50) not null,
	"Place" varchar(100) not null,
	"EventEntrance" varchar(50) not null,
	"Text" text not null,
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,	
	"VisitCount" int not null,
	foreign key ("FkEvents") references public."Events"("Id") on update cascade on delete cascade,
	foreign key ("FkCountriesTranslations") references public."CountriesTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "EventsTranslationsComments"
(
	"Id" serial primary key not null,
	"FkEventsTranslations" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkEventsTranslationsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkEventsTranslations") references public."EventsTranslations"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "EventsTranslationsRating"
(
	"Id" serial primary key not null,
	"FkEventsTranslations" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkEventsTranslations") references public."EventsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "Journals"
(
	"Id" serial primary key not null,
	"FkCities" int not null,
	"FkCountries" int not null,	
	"FkAuthors" int not null,
	"FkTopics" int not null,
	"PostDate" varchar(50) not null,
	"Photo" text null,
	"Name" varchar(255) not null,
	"Description" varchar(120) not null,
	"Text" text not null,
	"JournalLanguage" varchar(40) not null,
	"Publisher" varchar(255) null,
	"Year" int null,
	"Refereed" varchar(50) null,
	"IssuesPerYear" int null,
	"JCRImpactFactor" varchar(50) null,
	"IssueArticles" int null,
	"ImpactFactorRSCI" varchar(50) null,
	"RenameTransferInformation" varchar(255) null,
	"Reduction" varchar(255) null,
	"ISSNPrintVersion" varchar(50) null,
	"SubscriptionIndex" int null,
	"ISSNIOnlineVersion" varchar(50) null,
	"PresentationOption" varchar(255) null,
	"ISI" varchar(50) null,
	"TotalArticles" int null,
	"CurrentlyStatus" varchar(255) null,
	"SCOPUS" varchar(50) null,
	"TotalIssues" int null,
	"RISC" varchar(50) null,
	"FullTexts" int null,
	"Abstract" varchar(50) null,
	"HACList" varchar(255) null,
	"Quotes" int null,
	"Multidisciplinary" varchar(50) null,
	"SubjectHeadings" text null,
	"EditorialCollege" text null,	
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,
	"VisitCount" int not null,
	foreign key ("FkCities") references public."Cities"("Id") on update cascade on delete cascade,	
	foreign key ("FkCountries") references public."Countries"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthors") references public."Authors"("Id") on update cascade on delete cascade,
	foreign key ("FkTopics") references public."Topics"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "JournalsContents"
(
	"Id" serial primary key not null,
	"FkJournals" int not null,
	"FkAuthors" int not null,
	"Name" varchar(255) not null,
	"Pages" varchar(25) not null,
	foreign key ("FkJournals") references public."Journals"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthors") references public."Authors"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "JournalsArticles"
(
	"Id" serial primary key not null,
	"FkJournals" int not null,
	"FkTopics" int not null,
	"FkOrganizations" int not null,
	"FkAuthors" int not null,
	"Name" varchar(255) not null,
	"Description" varchar(255) not null,
	"ArticleLanguage" varchar(40) not null,
	"ArticleYear" int not null,
	"Pages" varchar(25) not null,
	foreign key ("FkJournals") references public."Journals"("Id") on update cascade on delete cascade,
	foreign key ("FkTopics") references public."Topics"("Id") on update cascade on delete cascade,
	foreign key ("FkOrganizations") references public."Organizations"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthors") references public."Authors"("Id") on update cascade on delete cascade
);

/* last update 1/25/2021 10:00 PM */
create table if not exists "JournalsPublications"
(
	"Id" serial primary key not null,
	"FkJournalsArticles" int not null,
	"FkPublicationTypes" int not null,
	"FkOrganizations" int not null,
	"FkAuthors" int not null,
	"Name" varchar(255) not null,
	"Description" varchar(255) not null,
	"ArticleLanguage" varchar(40) not null,
	"ArticleYear" int not null,
	"Pages" varchar(25) not null,
	foreign key ("FkJournalsArticles") references public."JournalsArticles"("Id") on update cascade on delete cascade,
	foreign key ("FkPublicationTypes") references public."PublicationTypes"("Id") on update cascade on delete cascade,
	foreign key ("FkOrganizations") references public."Organizations"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthors") references public."Authors"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "JournalsComments"
(
	"Id" serial primary key not null,
	"FkJournals" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkJournalsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkJournals") references public."Journals"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "JournalsRating"
(
	"Id" serial primary key not null,
	"FkJournals" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkJournals") references public."Journals"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "JournalsTranslations"
(
	"Id" serial primary key not null,
	"FkJournals" int not null,
	"FkLanguages" int not null,
	"FkCitiesTranslations" int not null,
	"FkCountriesTranslations" int not null,	
	"FkAuthorsTranslations" int not null,
	"FkTopicsTranslations" int not null,
	"PostDate" varchar(50) not null,
	"Photo" text null,
	"Name" varchar(255) not null,
	"Description" varchar(120) not null,
	"Text" text not null,
	"JournalLanguage" varchar(40) not null,
	"Publisher" varchar(255) null,
	"Refereed" varchar(50) null,
	"JCRImpactFactor" varchar(50) null,
	"ImpactFactorRSCI" varchar(50) null,
	"RenameTransferInformation" varchar(255) null,
	"Reduction" varchar(255) null,
	"ISSNPrintVersion" varchar(50) null,
	"ISSNIOnlineVersion" varchar(50) null,
	"PresentationOption" varchar(255) null,
	"ISI" varchar(50) null,
	"CurrentlyStatus" varchar(255) null,
	"SCOPUS" varchar(50) null,
	"RISC" varchar(50) null,
	"Abstract" varchar(50) null,
	"HACList" varchar(255) null,
	"Multidisciplinary" varchar(50) null,
	"SubjectHeadings" text null,
	"EditorialCollege" text null,	
	"PostStatus" boolean not null,
	"LikeCount" int not null,
	"DislikeCount" int not null,
	"VisitCount" int not null,
	foreign key ("FkJournals") references public."Journals"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade,
	foreign key ("FkCitiesTranslations") references public."CitiesTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkCountriesTranslations") references public."CountriesTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthorsTranslations") references public."AuthorsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkTopicsTranslations") references public."TopicsTranslations"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "JournalsTranslationsContents"
(
	"Id" serial primary key not null,
	"FkJournalsTranslations" int not null,
	"FkAuthorsTranslations" int not null,
	"FkLanguages" int not null,
	"Name" varchar(255) not null,
	"Pages" varchar(25) not null,
	foreign key ("FkJournalsTranslations") references public."JournalsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthorsTranslations") references public."AuthorsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkLanguages") references public."Languages"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "JournalsTranslationsArticles"
(
	"Id" serial primary key not null,
	"FkJournalsTranslations" int not null,
	"FkTopicsTranslations" int not null,
	"FkOrganizationsTranslations" int not null,
	"FkAuthorsTranslations" int not null,
	"Name" varchar(255) not null,
	"Description" varchar(255) not null,
	"ArticleLanguage" varchar(40) not null,
	"ArticleYear" int not null,
	"Pages" varchar(25) not null,
	foreign key ("FkJournalsTranslations") references public."JournalsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkTopicsTranslations") references public."TopicsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkOrganizationsTranslations") references public."OrganizationsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthorsTranslations") references public."AuthorsTranslations"("Id") on update cascade on delete cascade
);

/* last update 1/25/2021 10:00 PM */
create table if not exists "JournalsTranslationsPublications"
(
	"Id" serial primary key not null,
	"FkJournalsTranslationsArticles" int not null,
	"FkPublicationTypesTranslations" int not null,
	"FkOrganizationsTranslations" int not null,
	"FkAuthorsTranslations" int not null,
	"Name" varchar(255) not null,
	"Description" varchar(255) not null,
	"ArticleLanguage" varchar(40) not null,
	"ArticleYear" int not null,
	"Pages" varchar(25) not null,
	foreign key ("FkJournalsTranslationsArticles") references public."JournalsTranslationsArticles"("Id") on update cascade on delete cascade,
	foreign key ("FkPublicationTypesTranslations") references public."PublicationTypesTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkOrganizationsTranslations") references public."OrganizationsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkAuthorsTranslations") references public."AuthorsTranslations"("Id") on update cascade on delete cascade
);

/* last update 12/2/2020 10:52 PM */
create table if not exists "JournalsTranslationsComments"
(
	"Id" serial primary key not null,
	"FkJournalsTranslations" int not null,
	"FkUsers" int null,
	"FkAdmins" int null,
	"FkJournalsTranslationsComments" int null,
	"DateComment" varchar(50) not null,
	"TimeComment" varchar(50) not null,	
	"Text" text not null,
	foreign key ("FkJournalsTranslations") references public."JournalsTranslations"("Id") on update cascade on delete cascade
);

/* last update 10/19/2020 8:00 PM */
create table if not exists "JournalsTranslationsRating"
(
	"Id" serial primary key not null,
	"FkJournalsTranslations" int not null,
	"FkUsers" int not null,
	"LikeRating" boolean null,
	"DislikeRating" boolean  null,
	foreign key ("FkJournalsTranslations") references public."JournalsTranslations"("Id") on update cascade on delete cascade,
	foreign key ("FkUsers") references public."Users"("Id") on update cascade on delete cascade	
);

/* last update 2/3/2021 7:24 PM */
create table if not exists "TotalVisitCount"
(
	"Id" serial primary key not null,
	"VisitDate" varchar (50) not null,
	"VisitCount" int not null
);

/* last update 2/3/2021 8:41 PM */
create table if not exists "VisitorsInfo"
(
	"Id" serial primary key not null,
	"VisitorId" text not null,
	"VisitDate" varchar (50) not null,
	"TableName" varchar(100) not null,
	"TableId" int not null
);