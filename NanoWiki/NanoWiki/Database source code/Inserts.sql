INSERT INTO public."Languages"("Id","FullName","Abbreviation","Description","DateFormat","TimeFormat") 
VALUES 
(1,'English','en','USA language','M/d/yyyy','hh:mm:ss tt'),
(2,'Russian','ru','Russian language','dd.MM.yyyy','hh:mm:ss'),
(3,'Chinese (simplified)','zh','Chinese Language','yyyy-M-d','hh:mm:ss'),
(4,'Japanese','ja','Japanese language','"yyyy.mm.dd"','hh:mm:ss');

INSERT INTO public."Admins"("Id","Login","Email","Password","DateLink","TimeLink","Hash")
VALUES
(1,'NanoWikiAdmin','NanoWikiHelp@gmail.com','ncANotQ9hBhIjaL89DW0M3creXiXYu00Q+Dk9vsyqIk=',null,null,'DE9F339D8034FDF2B5EB3DB1FF390D3F‡DEC7C50B1AAA468F975B0BC5CA7AE604');

INSERT INTO public."AboutProject"("Id","FkLanguages","Header","Text") 
VALUES 
(1,1,'The Big Library of NANO','Free library on nanotechnology. And also events, people, news and much more.'),
(2,2,'Большая библиотека НАНО','Свободная библиотека по нанотехнологиям. А так же события, люди, новости и многое другое.'),
(3,3,'NANO大圖書館','納米技術免費圖書館。 還有活動，人物，新聞等等。'),
(4,4,'ナノの大図書館','ナノテクノロジーに関する無料の図書館。 そしてまた、イベント、人々、ニュースなど。');

INSERT INTO public."Authors"("Id","Initials","BirthDate","DeathDate","BirthLocation","Nationality")
VALUES 
(1,'Ivanov Ivan Ivanovich','10.05.1970','','Belarus','Belarus'),
(2,'Tim Smit','23.03.1990','','Belarus','Belarus');

INSERT INTO public."Topics"("Id","Name")
VALUES 
(1, 'Automation. Computer Engineering'),
(2, 'Astronomy'),
(3, 'Biology'),
(4, 'Biotechnology'),
(5, 'Water industry'),
(6, 'Warfare'),
(7, 'Geodesy'),
(8, 'Geology'),
(9, 'Geophysics'),
(10, 'Mining engineering');

INSERT INTO public."TopicsTranslations"("Id","FkTopics","FkLanguages","Name")
VALUES
(1, 1, 2, 'Автоматика. Компьютерная инженерия'),
(2, 1, 3, '自动化。 计算机工程'),
(3, 1, 4, 'オートメーション。 コンピューターエンジニア');

INSERT INTO public."Posts"("Id","FkAdmins","FkTopics","PostDate","Name","Description","Text","Literature","SourceReference","Tags","Photo","PostStatus","LikeCount","DislikeCount","VisitCount") 
VALUES
(1,1,1,'23.06.2019','Name 1','Description 1','Text 1','Literature 1','Sources 1','#tag1','~/images/user/posts/default.png',true,0,0,0),
(2,1,2,'23.06.2019','Name 2','Description 2','Text 2','Literature 2','Sources 2','#tag1','~/images/user/posts/default.png',true,0,0,0),
(3,1,3,'23.06.2019','Name 3','Description 3','Text 3','Literature 3','Sources 3','#tag1','~/images/user/posts/default.png',true,0,0,0),
(4,1,4,'23.06.2019','Name 4','Description 4','Text 4','Literature 4','Sources 4','#tag1','~/images/user/posts/default.png',true,0,0,0),
(5,1,5,'23.06.2019','Name 5','Description 5','Text 5','Literature 5','Sources 5','#tag1','~/images/user/posts/default.png',true,0,0,0),
(6,1,6,'23.06.2019','Name 6','Description 6','Text 6','Literature 6','Sources 6','#tag1','~/images/user/posts/default.png',true,0,0,0),
(7,1,7,'23.06.2019','Name 7','Description 7','Text 7','Literature 7','Sources 7','#tag1','~/images/user/posts/default.png',true,0,0,0),
(8,1,8,'23.06.2019','Name 8','Description 8','Text 8','Literature 8','Sources 8','#tag1','~/images/user/posts/default.png',true,0,0,0),
(9,1,9,'23.06.2019','Name 9','Description 9','Text 9','Literature 9','Sources 9','#tag1','~/images/user/posts/default.png',true,0,0,0),
(10,1,10,'23.06.2019','Name 10','Description 10','Text 10','Literature 10','Sources 10','#tag1','~/images/user/posts/default.png',true,0,0,0);

INSERT INTO public."PostsTranslations"("Id","FkPosts","FkLanguages","FkAdmins","FkTopicsTranslations","PostDate","Name","Description","Text","Literature","SourceReference","Photo","PostStatus","LikeCount","DislikeCount","VisitCount") 
VALUES
/*en*/
(1,1,1,1,1,'23.06.2019','Название 1','Description 1','Text 1','Литература 1','Источники 1','~/images/user/posts/default.png',true,0,0,0),
(2,2,1,1,1,'23.06.2019','Название 2','Description 2','Text 2','Литература 2','Источники 2','~/images/user/posts/default.png',true,0,0,0),
(3,3,1,1,1,'23.06.2019','Название 3','Description 3','Text 3','Литература 3','Источники 3','~/images/user/posts/default.png',true,0,0,0),
(4,4,1,1,1,'23.06.2019','Название 4','Description 4','Text 4','Литература 4','Источники 4','~/images/user/posts/default.png',true,0,0,0),
(5,5,1,1,1,'23.06.2019','Название 5','Description 5','Text 5','Литература 5','Источники 5','~/images/user/posts/default.png',true,0,0,0),
(6,6,1,1,1,'23.06.2019','Название 6','Description 6','Text 6','Литература 6','Источники 6','~/images/user/posts/default.png',true,0,0,0),
(7,7,1,1,1,'23.06.2019','Название 7','Description 7','Text 7','Литература 7','Источники 7','~/images/user/posts/default.png',true,0,0,0),
(8,8,1,1,1,'23.06.2019','Название 8','Description 8','Text 8','Литература 8','Источники 8','~/images/user/posts/default.png',true,0,0,0),
(9,9,1,1,1,'23.06.2019','Название 9','Description 9','Text 9','Литература 9','Источники 9','~/images/user/posts/default.png',true,0,0,0),
(10,10,1,1,1,'23.06.2019','Название 10','Description 10','Text 10','Литература 10','Источники 10','~/images/user/posts/default.png',true,0,0,0),
/*zh*/
(11,1,3,1,2,'2019-6-23','名称 1','描述 1','文本 1','文学 1','来源 1','~/images/user/posts/default.png',true,0,0,0),
(12,2,3,1,2,'2019-6-23','名称 2','描述 2','文本 2','文学 2','来源 2','~/images/user/posts/default.png',true,0,0,0),
(13,3,3,1,2,'2019-6-23','名称 3','描述 3','文本 3','文学 3','来源 3','~/images/user/posts/default.png',true,0,0,0),
(14,4,3,1,2,'2019-6-23','名称 4','描述 4','文本 4','文学 4','来源 4','~/images/user/posts/default.png',true,0,0,0),
(15,5,3,1,2,'2019-6-23','名称 5','描述 5','文本 5','文学 5','来源 5','~/images/user/posts/default.png',true,0,0,0),
(16,6,3,1,2,'2019-6-23','名称 6','描述 6','文本 6','文学 6','来源 6','~/images/user/posts/default.png',true,0,0,0),
(17,7,3,1,2,'2019-6-23','名称 7','描述 7','文本 7','文学 7','来源 7','~/images/user/posts/default.png',true,0,0,0),
(18,8,3,1,2,'2019-6-23','名称 8','描述 8','文本 8','文学 8','来源 8','~/images/user/posts/default.png',true,0,0,0),
(19,9,3,1,2,'2019-6-23','名称 9','描述 9','文本 9','文学 9','来源 9','~/images/user/posts/default.png',true,0,0,0),
(20,10,3,1,2,'2019-6-23','名称 10','描述 10','文本 10','文学 10','来源 10','~/images/user/posts/default.png',true,0,0,0),
/*ja*/
(31,1,4,1,3,'2019.06.23','お名前 1','説明 1','テキスト 1','文学 1','出典 1','~/images/user/posts/default.png',true,0,0,0),
(32,2,4,1,3,'2019.06.23','お名前 2','説明 2','テキスト 2','文学 2','出典 2','~/images/user/posts/default.png',true,0,0,0),
(33,3,4,1,3,'2019.06.23','お名前 3','説明 3','テキスト 3','文学 3','出典 3','~/images/user/posts/default.png',true,0,0,0),
(34,4,4,1,3,'2019.06.23','お名前 4','説明 4','テキスト 4','文学 4','出典 4','~/images/user/posts/default.png',true,0,0,0),
(35,5,4,1,3,'2019.06.23','お名前 5','説明 5','テキスト 5','文学 5','出典 5','~/images/user/posts/default.png',true,0,0,0),
(36,6,4,1,3,'2019.06.23','お名前 6','説明 6','テキスト 6','文学 6','出典 6','~/images/user/posts/default.png',true,0,0,0),
(37,7,4,1,3,'2019.06.23','お名前 7','説明 7','テキスト 7','文学 7','出典 7','~/images/user/posts/default.png',true,0,0,0),
(38,8,4,1,3,'2019.06.23','お名前 8','説明 8','テキスト 8','文学 8','出典 8','~/images/user/posts/default.png',true,0,0,0),
(39,9,4,1,3,'2019.06.23','お名前 9','説明 9','テキスト 9','文学 9','出典 9','~/images/user/posts/default.png',true,0,0,0),
(40,10,4,1,3,'2019.06.23','お名前 10','説明 10','テキスト 10','文学 10','出典 10','~/images/user/posts/default.png',true,0,0,0);

INSERT INTO public."AccountTypes"("Id","Name","Description","AccessLvl")
VALUES 
(1,'Admin','Full access',0),
(2,'User','User access',1);

INSERT INTO public."AccountTypesTranslations"("Id","FkAccountTypes","FkLanguages","Name","Description")
VALUES 
/*ru*/
(1,1,2,'Администратор','Полный доступ'),
(2,2,2,'Пользователь','Пользовательский доступ'),
/*zh*/
(3,1,3,'管理员','完全访问'),
(4,2,3,'用户','用户访问'),
/*ja*/
(5,1,4,'管理者','全権アクセス'),
(6,2,4,'ユーザー','ユーザーアクセス');

INSERT INTO public."Books"("Id","FkAuthors","FkTopics","PostDate","Photo","BookLanguage","Name","PublishingHouse","ISBN","Year","PageCount","Description","Text","Shops","PostStatus","LikeCount","DislikeCount","VisitCount")
VALUES
(1,1,1,'23.06.2019','~/images/user/books/default.png','Russian','Book 1','Publisher 1','ISBN-9999',2019,512,'Description 1','Text 1','[[https://www.google.by] - Download here] - Download here',true,0,0,0),
(2,2,2,'23.06.2019','~/images/user/books/default.png','Russian','Book 2','Publisher 2','ISBN-9999',2019,512,'Description 2','Text 2','[https://www.google.by] - Download here',true,0,0,0),
(3,1,3,'23.06.2019','~/images/user/books/default.png','Russian','Book 3','Publisher 3','ISBN-9999',2019,512,'Description 3','Text 3','[https://www.google.by] - Download here',true,0,0,0),
(4,2,4,'23.06.2019','~/images/user/books/default.png','Russian','Book 4','Publisher 4','ISBN-9999',2019,512,'Description 4','Text 4','[https://www.google.by] - Download here',true,0,0,0),
(5,1,5,'23.06.2019','~/images/user/books/default.png','Russian','Book 5','Publisher 5','ISBN-9999',2019,512,'Description 5','Text 5','[https://www.google.by] - Download here',true,0,0,0),
(6,2,6,'23.06.2019','~/images/user/books/default.png','Russian','Book 6','Publisher 6','ISBN-9999',2019,512,'Description 6','Text 6','[https://www.google.by] - Download here',true,0,0,0),
(7,1,7,'23.06.2019','~/images/user/books/default.png','Russian','Book 7','Publisher 7','ISBN-9999',2019,512,'Description 7','Text 7','[https://www.google.by] - Download here',true,0,0,0),
(8,2,8,'23.06.2019','~/images/user/books/default.png','Russian','Book 8','Publisher 8','ISBN-9999',2019,512,'Description 8','Text 8','[https://www.google.by] - Download here',true,0,0,0),
(9,1,9,'23.06.2019','~/images/user/books/default.png','Russian','Book 9','Publisher 9','ISBN-9999',2019,512,'Description 9','Text 9','[https://www.google.by] - Download here',true,0,0,0),
(10,2,10,'23.06.2019','~/images/user/books/default.png','Russian','Book 10','Publisher 10','ISBN-9999',2019,512,'Description 10','Text 10','[https://www.google.by] - Download here',true,0,0,0);

INSERT INTO public."Scientists"("Id","PostDate","Initials","BirthDate","DeathDate","BirthLocation","Nationality","Autobiography","Photo","Phone","Email","LinkedIn","PostStatus","LikeCount","DislikeCount","VisitCount")
VALUES 
(1,'23.06.2019','Surname 1 Name 1 Middle name 1','10.10.1960','','USA','American','Autobiography 1','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(2,'23.06.2019','Surname 2 Name 2 Middle name 2','10.10.1960','','USA','American','Autobiography 2','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(3,'23.06.2019','Surname 3 Name 3 Middle name 3','10.10.1960','','USA','American','Autobiography 3','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(4,'23.06.2019','Surname 4 Name 4 Middle name 4','10.10.1960','','USA','American','Autobiography 4','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(5,'23.06.2019','Surname 5 Name 5 Middle name 5','10.10.1960','','USA','American','Autobiography 5','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(6,'23.06.2019','Surname 6 Name 6 Middle name 6','10.10.1960','','USA','American','Autobiography 6','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(7,'23.06.2019','Surname 7 Name 7 Middle name 7','10.10.1960','','USA','American','Autobiography 7','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(8,'23.06.2019','Surname 8 Name 8 Middle name 8','10.10.1960','','USA','American','Autobiography 8','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(9,'23.06.2019','Surname 9 Name 9 Middle name 9','10.10.1960','','USA','American','Autobiography 9','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0),
(10,'23.06.2019','Surname 10 Name 10 Middle name 10','10.10.1960','USA','','American','Autobiography 10','~/images/user/scientists/default.png','+375-29-333-33-33','test@gmail.com','',true,0,0,0);

INSERT INTO public."Countries"("Id","Name")
VALUES 
(1,'Belarus'),
(2,'USA'),
(3,'Russia');

INSERT INTO public."Cities"("Id","FkCountries","Name")
VALUES 
(1,1,'Minsk'),
(2,1,'Brest'),
(3,2,'Moscow'),
(4,2,'Piter');

INSERT INTO public."Events"("Id","FkCountries","PostDate","Photo","Name","Description","EventDate","EventTime","Place","EventEntrance","Text","PostStatus","LikeCount","DislikeCount","VisitCount")
VALUES
(1,1,'23.06.2019','~/images/user/events/default.png','Event 1','Description 1','25.10.2019','15:00','Minsk Arena','Free','Text 1',true,0,0,0),
(2,1,'23.06.2019','~/images/user/events/default.png','Event 2','Description 2','25.10.2019','15:00','Minsk Arena','Free','Text 2',true,0,0,0),
(3,1,'23.06.2019','~/images/user/events/default.png','Event 3','Description 3','25.10.2019','15:00','Minsk Arena','Free','Text 3',true,0,0,0),
(4,1,'23.06.2019','~/images/user/events/default.png','Event 4','Description 4','25.10.2019','15:00','Minsk Arena','Free','Text 4',true,0,0,0),
(5,1,'23.06.2019','~/images/user/events/default.png','Event 5','Description 5','25.10.2019','15:00','Minsk Arena','Free','Text 5',true,0,0,0),
(6,1,'23.06.2019','~/images/user/events/default.png','Event 6','Description 6','25.10.2019','15:00','Minsk Arena','Free','Text 6',true,0,0,0),
(7,1,'23.06.2019','~/images/user/events/default.png','Event 7','Description 7','25.10.2019','15:00','Minsk Arena','Free','Text 7',true,0,0,0),
(8,1,'23.06.2019','~/images/user/events/default.png','Event 8','Description 8','25.10.2019','15:00','Minsk Arena','Free','Text 8',true,0,0,0),
(9,1,'23.06.2019','~/images/user/events/default.png','Event 9','Description 9','25.10.2019','15:00','Minsk Arena','Free','Text 9',true,0,0,0),
(10,1,'23.06.2019','~/images/user/events/default.png','Event 10','Description 10','25.10.2019','15:00','Minsk Arena','Free','Text 10',true,0,0,0);

INSERT INTO public."PostsComments"("Id","FkPosts","FkUsers","FkAdmins","FkPostsComments","DateComment","TimeComment","Text")
VALUES 
(1,20,1,null,null,'23.06.2019','15:38','user comment 1'),
(2,20,1,null,1,'23.06.2019','15:38','user comment 2'),
(3,20,1,null,2,'23.06.2019','15:38','user comment 3'),
(4,20,null,1,null,'23.06.2019','15:38','admin comment 1'),
(5,20,1,null,null,'23.06.2019','15:38','user comment 4'),
(6,20,1,null,null,'23.06.2019','15:38','user comment 5'),
(7,20,null,1,4,'23.06.2019','15:38','admin comment 2'),
(8,20,1,null,null,'23.06.2019','15:38','user comment 6'),
(9,20,1,null,null,'23.06.2019','15:38','user comment 7'),
(10,20,null,1,null,'23.06.2019','15:38','admin comment 3'),
(11,20,1,null,3,'23.06.2019','15:38','user comment 4'),
(12,20,1,null,11,'23.06.2019','15:38','user comment 5');

INSERT INTO public."Journals"("Id","FkCities","FkCountries","FkAuthors","FkTopics","PostDate","Photo","Name","Description","Text","JournalLanguage",
"Publisher","Year","Refereed","IssuesPerYear","JCRImpactFactor","IssueArticles","ImpactFactorRSCI","RenameTransferInformation","Reduction",
"ISSNPrintVersion","SubscriptionIndex","ISSNIOnlineVersion","PresentationOption","ISI","TotalArticles","CurrentlyStatus","SCOPUS",
"TotalIssues","RISC","FullTexts","Abstract","HACList","Quotes","Multidisciplinary","SubjectHeadings","EditorialCollege","PostStatus","LikeCount","DislikeCount","VisitCount") 
values 
('1','1','1','1','1','24.06.2019','~/images/user/journals/default.png','Name 1','Description 1',
'Text','ru','Publisher 1','2019','','1990','12','10','Indexed in the RSCI',
'Individual articles are translated','','','220140','','','','120','Coming out now',
'Partially indexed','12','','0','','','0','','','',true,0,0,0),
('2','2','1','2','1','24.06.2019','~/images/user/journals/default.png','Name 2','Description 2',
'Text','ru','Publisher 2','2019','','1990','12','10','Не Indexed in the RSCI',
'Fully translated','','','220140','','','','120','Coming out now',
'Indexed in SCOPUS','12','','0','','','0','','','',true,0,0,0),
('3','1','1','1','3','24.06.2019','~/images/user/journals/default.png','Name 3','Description 3',
'Text','ru','Publisher 3','2019','','1990','12','10','Indexed in the RSCI',
'Fully translated','','','220140','','','','120','Coming out now',
'Indexed in SCOPUS','12','','0','','','0','','','',true,0,0,0),
('4','2','1','2','1','24.06.2019','~/images/user/journals/default.png','Name 4','Description 4',
'Text','ru','Publisher 4','2019','','1990','12','10','Indexed in the RSCI',
'Fully translated','','','220140','','','','120','Coming out now',
'Partially indexed','12','','0','','','0','','','',true,0,0,0),
('5','1','1','1','3','24.06.2019','~/images/user/journals/default.png','Name 5','Description 5',
'Text','ru','Publisher 5','2019','','1990','12','10','Не Indexed in the RSCI',
'Fully translated','','','220140','','','','120','Coming out now',
'Indexed in SCOPUS','12','','0','','','0','','','',true,0,0,0),
('6','2','1','1','5','24.06.2019','~/images/user/journals/default.png','Name 6','Description 6',
'Text','ru','Publisher 6','2019','','1990','12','10','Indexed in the RSCI',
'Fully translated','','','220140','','','','120','Coming out now',
'Indexed in SCOPUS','12','','0','','','0','','','',true,0,0,0),
('7','1','1','1','1','24.06.2019','~/images/user/journals/default.png','Name 7','Description 7',
'Text','ru','Publisher 7','2019','','1990','12','10','Не Indexed in the RSCI',
'Не переводится','','','220140','','','','120','Coming out now',
'Partially indexed','12','','0','','','0','','','',true,0,0,0),
('8','1','1','2','7','24.06.2019','~/images/user/journals/default.png','Name 8','Description 8',
'Text','ru','Publisher 8','2019','','1990','12','10','Indexed in the RSCI',
'Individual articles are translated','','','220140','','','','120','Coming out now',
'Indexed in SCOPUS','12','','0','','','0','','','',true,0,0,0),
('9','2','1','1','10','24.06.2019','~/images/user/journals/default.png','Name 9','Description 9',
'Text','ru','Publisher 9','2019','','1990','12','10','Indexed in the RSCI',
'Fully translated','','','220140','','','','120','Coming out now',
'Indexed in SCOPUS','12','','0','','','0','','','',true,0,0,0),
('10','1','1','2','2','24.06.2019','~/images/user/journals/default.png','Name 10','Description 10',
'Text','ru','Publisher 10','2019','','1990','12','10','Не Indexed in the RSCI',
'Individual articles are translated','','','220140','','','','120','Coming out now',
'Indexed in SCOPUS','12','','0','','','0','','','',true,0,0,0);

INSERT INTO public."JournalsContents"("Id","FkJournals","FkAuthors","Name","Pages")
VALUES 
(1,10,1,'Chapter 1','1-5'),
(2,10,1,'Chapter 2','5-10'),
(3,10,1,'Chapter 3','10-15'),
(4,10,1,'Chapter 4','15-25'),
(5,10,1,'Chapter 5','25-45'),
(6,10,1,'Chapter 6','45-50'),
(7,10,1,'Chapter 7','55-60'),
(8,10,1,'Chapter 8','60-65'),
(9,10,1,'Chapter 9','65-75'),
(10,10,1,'Chapter 10','75-100');

INSERT INTO public."Organizations"("Id","FkCountries","Name","Text")
VALUES 
(1,1,'Organization 1','Description 1'),
(2,2,'Organization 2','Description 2'),
(3,3,'Organization 3','Description 3'),
(4,1,'Organization 4','Description 4'),
(5,2,'Organization 5','Description 5'),
(6,3,'Organization 6','Description 6');

INSERT INTO public."JournalsArticles"("Id","FkJournals","FkTopics",
"FkOrganizations","FkAuthors","Name","Description","ArticleLanguage","ArticleYear","Pages")
VALUES 
(1,1,1,1,1,'Article 1','Description 1','Russian',2019,'1-5'),
(2,2,2,2,2,'Article 2','Description 2','Russian',2019,'1-5'),
(3,3,5,3,1,'Article 3','Description 3','Russian',2019,'1-5'),
(4,5,4,4,1,'Article 4','Description 4','Russian',2019,'1-5'),
(5,5,6,5,2,'Article 5','Description 5','Russian',2019,'1-5'),
(6,3,9,6,1,'Article 6','Description 6','Russian',2019,'1-5'),
(7,3,10,1,1,'Article 7','Description 7','Russian',2019,'1-5'),
(8,3,3,1,2,'Article 8','Description 8','Russian',2019,'1-5'),
(9,3,7,1,2,'Article 9','Description 9','Russian',2019,'1-5'),
(10,10,8,1,2,'Article 10','Description 10','Russian',2019,'1-5');

INSERT INTO public."PublicationTypes"("Id", "Name")
VALUES 
(1, 'Automation. Computer Engineering'),
(2, 'Astronomy'),
(3, 'Biology'),
(4, 'Biotechnology'),
(5, 'Water industry'),
(6, 'Warfare'),
(7, 'Geodesy'),
(8, 'Geology'),
(9, 'Geophysics'),
(10, 'Mining engineering');

INSERT INTO public."JournalsPublications"("Id","FkJournalsArticles","FkPublicationTypes",
"FkOrganizations","FkAuthors","Name","Description","ArticleLanguage","ArticleYear","Pages")
VALUES 
(1,1,1,1,1,'Publication 1','Description of the publication 1','Russian',2019,''),
(2,1,2,2,1,'Publication 2','Description of the publication 2','Russian',2019,''),
(3,1,3,1,1,'Publication 3','Description of the publication 3','Russian',2019,''),
(4,2,4,2,1,'Publication 4','Description of the publication 4','Russian',2019,''),
(5,2,5,2,1,'Publication 5','Description of the publication 5','Russian',2019,''),
(6,2,6,1,1,'Publication 6','Description of the publication 6','Russian',2019,''),
(7,3,7,2,1,'Publication 7','Description of the publication 7','Russian',2019,''),
(8,4,8,1,1,'Publication 8','Description of the publication 8','Russian',2019,''),
(9,5,9,2,1,'Publication 9','Description of the publication 9','Russian',2019,''),
(10,6,10,1,1,'Publication 10','Description of the publication 10','Russian',2019,''),
(11,6,1,2,1,'Publication 11','Description of the publication 11','Russian',2019,''),
(12,7,2,1,1,'Publication 12','Description of the publication 12','Russian',2019,''),
(13,7,3,2,1,'Publication 13','Description of the publication 13','Russian',2019,''),
(14,8,4,2,1,'Publication 14','Description of the publication 14','Russian',2019,''),
(15,8,5,2,1,'Publication 15','Description of the publication 15','Russian',2019,''),
(16,9,6,1,1,'Publication 16','Description of the publication 16','Russian',2019,''),
(17,10,7,1,1,'Publication 17','Description of the publication 17','Russian',2019,''),
(18,10,8,1,1,'Publication 18','Description of the publication 18','Russian',2019,''),
(19,10,9,1,1,'Publication 19','Description of the publication 19','Russian',2019,''),
(20,10,10,1,1,'Publication 20','Description of the publication 20','Russian',2019,'');


ALTER TABLE public."Books" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."BooksTranslations" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."Events" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."EventsTranslations" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."Journals" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."JournalsTranslations" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."Posts" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."PostsTranslations" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."Scientists" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."Scientists" ALTER COLUMN "SocialNetwork" TYPE text;
ALTER TABLE public."ScientistsTranslations" ALTER COLUMN "Photo" TYPE text;
ALTER TABLE public."UserProfile" ALTER COLUMN "Avatar" TYPE text;
ALTER TABLE public."AdminInfo" ALTER COLUMN "Avatar" TYPE text;

ALTER TABLE public."Books" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."BooksTranslations" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."Events" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."EventsTranslations" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."Journals" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."JournalsTranslations" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."Posts" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."PostsTranslations" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."Scientists" ADD COLUMN "VisitCount" INT NULL;
ALTER TABLE public."ScientistsTranslations" ADD COLUMN "VisitCount" INT NULL;

ALTER TABLE public."Books" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."BooksTranslations" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."Events" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."EventsTranslations" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."Journals" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."JournalsTranslations" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."Posts" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."PostsTranslations" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."Scientists" ALTER COLUMN "VisitCount" SET NOT NULL;
ALTER TABLE public."ScientistsTranslations" ALTER COLUMN "VisitCount" SET NOT NULL;

ALTER TABLE public."Posts" ALTER COLUMN "SourceReference" TYPE text;
ALTER TABLE public."PostsTranslations" ALTER COLUMN "SourceReference" TYPE text;