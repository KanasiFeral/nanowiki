﻿using System.Globalization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using NanoWiki.Managers;
using Newtonsoft.Json.Serialization;
using NanoWiki.Classes;

namespace NanoWiki
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure IIS server
            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });

            // Add possibility to read json data
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

            // Using the resource files from a folder "Resources"
            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });

            var supportedCultures = new[]                
            {
                new CultureInfo("ru"),
                new CultureInfo("en"),
                new CultureInfo("zh"),                    
                new CultureInfo("ja")                
            };

            // Get default culture
            var abbreviation = GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation");

            // Allowed cultures
            services.Configure<RequestLocalizationOptions>(options =>
            {
                // Default locale
                options.DefaultRequestCulture = new RequestCulture(abbreviation);
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                // If cookies are empty, setting the default locale
                options.RequestCultureProviders = new List<IRequestCultureProvider>
                {
                    new QueryStringRequestCultureProvider(),
                    new CookieRequestCultureProvider()
                };
            });

            // Add session support
            services.AddDistributedMemoryCache();
            services.AddSession();

            // Adding translation for html pages using resource files
            services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix, opts =>
            {
                opts.ResourcesPath = "Resources";
            }).AddDataAnnotationsLocalization();

            // Adding possibility to authentication with cookies
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = "/Account/Authorization/";
            });

            // Add possibility to read cookies from _Layout.cshtml
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Registred classes
            services.AddTransient<IResourcesManager, ResourcesManager>();
            services.AddTransient<IModelAttributesManager, ModelAttributesManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, 
                // see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            // Enable route functions
            app.UseRouting();

            // Enable localization
            app.UseRequestLocalization();

            // HTTP error handling
            app.UseStatusCodePagesWithReExecute("/Error/ErrorStatus", "?statusCode={0}");

            // Enable static files
            app.UseStaticFiles();

            // Enable sessions
            app.UseSession();

            // Enable cookie auth
            app.UseAuthentication();

            // Enable localization
            app.UseRequestLocalization();

            // Enable roles authorization
            app.UseAuthorization();

            // Enable total hit/visitor counter
            app.UseMiddleware(typeof(VisitorCounterMiddleware));

            // Set route map
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}