﻿$(window).on('load', function () {

    // Deleting account, deletion request
    $("#deleteAvatar").on("click", function (evt) {

        evt.preventDefault();

        $.ajax({
            url: "/Account/ChangeAvatar",
            type: "POST",
            data: {
                FileImg: null,
                Status: 'Delete'
            },
            dataType: "html",
            success: function (url) {
                location.reload();
            },
            error: function () {
                console.log("Error avatar delete!");
            }
        });
    });

    var hide = false;
    // Hide and show filter blocl form
    $("#filterButton").on("click", function () {
        if (hide === false) {
            $("#filterDiv").hide(1000);
            hide = true;
        }
        else {
            $("#filterDiv").show(1000);
            hide = false;
        }
    });

    // The method of sending data from the filtering form to the data processing method and generating the final condition
    $("#btnFilter").on("click", function (evt) {

        evt.preventDefault();

        var jsonArr = "[{'Key':'Table','Value':'" + $("#tableId").val() + "'},";
        jsonArr += "{'Key':'Id','Value':'" + $("#idValue").val() + "'},";
        jsonArr += "{'Key':'FkId','Value':'" + $("#fkIdValue").val() + "'},";

        var other_data = $("#filterForm").serializeArray();
        $.each(other_data, function (_key, input) {
            jsonArr += "{'Key':'" + input.name + "','Value':'" + input.value + "'},";
        });

        jsonArr = jsonArr.slice(0, -1);
        jsonArr += "]";

        $.ajax({
            url: "/Home/GenerateFilterCondition",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(jsonArr),
            async: false,
            success: function (data) {
                PostsRecordsLoad(data);
            },
            error: function () {
                $.ajax({
                    url: "Home/NoPosts",
                    type: "GET",
                    data: {
                        ajaxQuery: true
                    },
                    dataType: "html",
                    success: function (data) {
                        $("#PartialViewId").html(data);
                    }
                });
            }
        });

    });

    SetNavigation();
});

// Loading comments on the page by partial submission
function Comments(data) {
    if (data.ignoreMessage === false) {
        $("#" + data.divId).css("display", "block");
        $("#" + data.divId).html("<p>" + data.message + "</p>");

        $("#" + data.btnId).attr("disabled", true);

        setTimeout(function () {
            $("#" + data.divId).css("display", "none");
            $("#" + data.btnId).removeAttr("disabled");
        }, 3000);
    }
    else {
        if (data.secondLvl === true) {

            var divIdInsert = data.divIdInsert;

            $.ajax({
                url: "/Home/SingleCommentLvl2",
                type: "POST",
                data: {
                    table: data.table,
                    postId: data.postId,
                    replyCommentId: data.replyCommentId,
                    userId: data.userId,
                    adminId: data.adminId,
                    date: data.date,
                    time: data.time,
                    commentText: data.commentText
                },
                dataType: "html",
                success: function (data) {
                    // Insert block with new comment before form comment (second branch)
                    $("." + divIdInsert).css("display", "block");
                    $("." + divIdInsert + " ol").last().before(data);

                    // Insert event-click on the new blocks
                    var replyButtons = $(".replySecondLvl");

                    // Add "on click" event for all buttons in second lvl of comments
                    Array.from(replyButtons).forEach(function (element) {
                        element.addEventListener('click', function () {
                            $(".formSecondlvlComments textarea").val($(this).attr("name") + ", ");
                            $(".formSecondlvlComments textarea").focus();
                        });
                    });

                    // Show "reply" and "all comments" buttons (first branch)
                    $("a[name=" + divIdInsert + "]").eq(0).show();
                    $("a[name=" + divIdInsert + "]").eq(1).show();

                    // Close all forms of adding a comment and clear all input fields (first branch) and clear all input fields (second branch)
                    $(".formComments").css("display", "none");
                    $(".formComments textarea").val("");
                    $(".formSecondlvlComments textarea").val("");

                },
                error: function () {
                    $("." + divIdInsert + " ol").last().before("<ol class='children'><li><img src=\"../images/admin/adminError.png\" style=\"width: 200px; height: 200px; margin-top: 10%;\"/></li></ol><br>");
                }
            });
        }
        else {
            $.ajax({
                url: "/Home/Comments",
                type: "POST",
                data: {
                    table: data.table,
                    id: data.id,
                    page: data.page
                },
                dataType: "html",
                beforeSend: function () {
                    $('#PartialViewId').html("<img src=\"../../gif/admin/Loading.gif\"/>");
                },
                success: function (data) {
                    $("#PartialViewId").html(data);
                },
                error: function () {
                    $("#PartialViewId").html("<li><img src=\"../images/admin/adminError.png\" style=\"width: 200px; height: 200px; margin-top: 10%;\"/></li><br>");
                }
            });
            $("#commentTextId").val("");
        }
    }
}

// Rating change, request to change the rating in one direction or another
function SetRatingValue(tagId, ratingValue, ratingIcon) {
    if ($("." + tagId).length > 0) {
        for (var i = 0; i < $("." + tagId).length; i++) {
            $("." + tagId).eq(i).html("<i class='fa fa-thumbs-o-" + ratingIcon + "' aria-hidden='true'>" + ratingValue);
        }
    }
    else {
        $("." + tagId).html("<i class='fa fa-thumbs-o-" + ratingIcon + "' aria-hidden='true'>" + ratingValue);
    }
}

// Change the color of a pressed key rating
function SetRatingColor(tagId, color) {
    if ($("." + tagId).length > 0) {
        for (var q = 0; q < $("." + tagId).length; q++) {
            $("." + tagId).eq(q).css("color", color);
        }
    }
    else {
        $("." + tagId).css("color", color);
    }
}

// Request for visual display of a modified rating.Change the status and color of one button, depending on the next button
function RatingUpdate(data) {
    if (data.auth === false) {
        $("." + data.blockId).html("<a class='authLink' href='/Account/Authorization'>" + data.message + "</a>");
    }
    else if ((data.tagId === undefined || data.tagId === null) && data.ratingType !== "both") {
        console.log(data);
    }
    else {
        //set rating value
        if (data.ratingType === "LikeCount") {
            SetRatingValue(data.tagId, data.ratingCount, "up");
        }
        else if (data.ratingType === "DislikeCount") {
            SetRatingValue(data.tagId, data.ratingCount, "down");
        }
        else {
            SetRatingValue(data.likeId, data.likeCount, "up");
            SetRatingValue(data.dislikeId, data.dislikeCount, "down");
        }

        // set rating color
        if (data.likePressed === true) {
            if (data.ratingType === "both") {
                SetRatingColor(data.likeId, "rgb(62,166,255)");
                SetRatingColor(data.dislikeId, "");
            }
            else {
                SetRatingColor(data.tagId, "rgb(62, 166, 255)");
            }
        }
        else if (data.dislikePressed === true) {
            if (data.ratingType === "both") {
                SetRatingColor(data.likeId, "");
                SetRatingColor(data.dislikeId, "rgb(62,166,255)");
            }
            else {
                SetRatingColor(data.tagId, "rgb(62, 166, 255)");
            }
        }
        else {
            if (data.ratingType === "both") {
                SetRatingColor(data.likeId, "");
                SetRatingColor(data.dislikeId, "rgb(62,166,255)");
            }
            else {
                SetRatingColor(data.tagId, "");
            }
        }
    }
}

// Redirect to the page on the incoming address from the object or reload the page
function AjaxLoad(data) {
    //var urlReturn = JSON.parse(data).url;

    var urlReturn = "";

    if (data.message !== undefined) {
        urlReturn = data.url + "?Message=" + data.message;
        location.href = urlReturn;
    }
    else {
        document.location.reload(true);
    }
}

// Display a message with a text prompt and lock a button(protection against spam keystrokes) for a certain time
function AlertMessage(data) {

    var divId = "#" + data.divId;
    var btnId = "#" + data.btnId;

    $(divId).css("display", "block");
    $(divId).html("<p>" + data.message + "</p>");

    if (btnId !== "#undefined") { $(btnId).attr("disabled", true); }

    setTimeout(function () {
        $(divId).css("display", "none");
        if (btnId !== "#undefined") { $(btnId).removeAttr("disabled"); }
        if (data.home === true) { location.href = "/"; }
    }, data.time);
}

// Preloader loading
function IndexBeforeSend(data) {
    var divId = "#" + data;
    $(divId).html("<img src=\"../gif/admin/Loading.gif\"/>");
}

// Uploading the received partial view to the page
function IndexLoadData(data) {
    $("#PartialViewId").html(data);
}

// Message output and button lock(for a specific time) in the footer area of the site, newsletter subscription area
function AlertFooterMessage(data) {
    $("#idFooterLabel").append(JSON.parse(data).message);
    $("#btnSubscribe").attr("disabled", true);

    setTimeout(function () {
        $("#btnSubscribe").removeAttr("disabled");
        $("#idFooterLabel").append("");
    }, 3000);
}

// Select the currently open page(if possible)
function SetNavigation() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);

    $(".header__nav a").each(function () {
        var href = $(this).attr('href');
        try {
            if (path.substring(0, href.length) === href) {
                if (path === "/Home/Posts" || path === "/Home/SinglePost") {
                    if (window.location.search === "?Table=Events") {
                        $("#liHome").removeClass("active");
                        $("#liEvents").addClass("active");
                    }
                    else {
                        $("#liHome").removeClass("active");
                        $("#liPosts").addClass("active");
                    }
                }
                else if (path === "/Account/Authorization") {
                    $("#liHome").removeClass("active");
                }
                else {
                    if (path !== "" || path !== "/") {
                        $("#liHome").removeClass("active");
                    }

                    $(this).closest("li").addClass("active");
                }
            }
        } catch (e) { /console.log(e.message);/ }
    });
}

// Display modal window
function ShowPrettyWindow(url) {
    var title = $("#idPrettyWindowTittle").val();
    var content = $("#idPrettyWindowContent").val();
    var confirmButtonText = $("#idPrettyWindowConfirmBtn").val();
    var cancelButtonText = $("#idPrettyWindowCancelBtn").val();
    var errorMessage = $("#idPrettyWindowErrorMsg").val();

    $.sweetModal({
        theme: $.sweetModal.THEME_DARK,
        title: title,
        content: content,
        type: $.sweetModal.TYPE_MODAL,
        buttons: {
            cancelAction: {
                label: cancelButtonText,
                classes: "bordered"
            },
            deletionAction: {
                label: confirmButtonText,
                classes: "bordered",
                action: function () {
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "html",
                        success: function (data) {
                            var urlReturn = JSON.parse(data).url;
                            location.href = urlReturn;
                        },
                        error: function () {
                            var someObject = { url: "/Home/Error?Message=" + errorMessage };
                            OpenReturnUrl(someObject);
                        }
                    });
                }
            }
        }
    });
}

// Reload page
function ReloadPage() { document.location.reload(true); }

// Redirect to the page whose address is specified in the incoming object
function OpenReturnUrl(data) { location.href = data.url; }

// The method that loads the lines on the "Posts" page in the special area "PartialViewId"
function PostsRecordsLoad(data) {
    var searchValue = $("#searchValueId").val();
    var tableValue = $("#tableId").val();
    var idValue = $("#idValue").val();
    var fkIdValue = $("#fkIdValue").val();
    var filterValue = false;
    var urlRecords = "/Home/Records";
    var sortingValue = $("#sortingValueId").val();
    var orderValue = $("#orderValueId").val();

    try {
        searchValue = data.filterCondition;
        tableValue = data.table;
        idValue = data.id;
        fkIdValue = data.fkId;
        filterValue = data.filter;
        urlRecords = data.urlRecords;
        sortingValue = data.sortingValue;
        orderValue = data.orderValue;
    } catch (e) {
        console.log(e.message);
        searchValue = $("#searchValueId").val();
        tableValue = $("#tableId").val();
        idValue = $("#idValue").val();
        fkIdValue = $("#fkIdValue").val();
        filterValue = false;
        urlRecords = "/Home/Records";
        sortingValue = $("#sortingValueId").val();;
        orderValue = $("#orderValueId").val();
    }

    $.ajax({
        url: urlRecords,
        type: "POST",
        data: {
            table: tableValue,
            id: idValue,
            fkId: fkIdValue,
            searchValue: searchValue,
            page: 1,
            typePost: $("#typePostId").val(),
            filter: filterValue,
            sortingValue: sortingValue,
            orderValue: orderValue
        },
        dataType: "html",
        beforeSend: function () {
            $('#PartialViewId').html("<img src=\"../../gif/admin/Loading.gif\"/>");
        },
        success: function (data) {
            $("#PartialViewId").html(data);
        }
    });
}

// Method that loading all journal data on the page
function JournalDataLoad() {
    $.ajax({
        url: "/Home/JournalArticlesAndPublucations",
        type: "POST",
        data: {
            table: $("#tableId").val(),
            searchValue: $("#searchValueId").val(),
            id: $("#idValue").val(),
            fkId: $("#fkIdValue").val(),
            page: 1,
            filter: false,
            sortingValue: $("#sortingValueId").val(),
            orderValue: $("#orderValueId").val()
        },
        dataType: "html",
        beforeSend: function () {
            $('#PartialViewId').html("<img src=\"../../gif/admin/Loading.gif\"/>");
        },
        success: function (data) {
            $("#PartialViewId").html(data);
        }
    });
}

// The method controls the visibility of the cookie policy container.
function CookiePolicyScript() {

    var cookieAgree = getCookie("AgreeWithCookiePolicy");

    if (cookieAgree == "") {
 
        cookiePolicyInvisible();
   
        $(document).ready(function () {
            $(".close").click(function () {
                $("#cookiePolicy").alert("close");
            });
            $("#cookiePolicy").on('close.bs.alert', function () {
                setAgreeWithCookiePolicy();
            });       
        });
    };
}

function cookiePolicyInvisible() {

    var timeout = 0;
    var addfunc = getCookie("CookiePolicyInvisibleTimeOut");

    if (addfunc == "") {
        timeout = 2000; 
        setCookiePolicyInvisibleTimeOut();
    };

    cookieContainer = document.getElementById("cookiePolicy");
    cookieContainer.style.display = "none";
    setTimeout(function () { cookieContainer.style.display = "block"; }, timeout);
}

function setAgreeWithCookiePolicy() {
    var d = new Date();
    d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = "AgreeWithCookiePolicy=" + true + ";" + expires;   
}

function setCookiePolicyInvisibleTimeOut() {
    document.cookie = "CookiePolicyInvisibleTimeOut=" + true;   
}

function getCookie(cname) {

    var name = cname + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

