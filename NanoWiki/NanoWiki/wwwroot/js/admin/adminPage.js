﻿$(document).ready(function () {
    $("#idHrHtmlEditor").hide();
    $("#idLoadEditor").hide();

    $("#LoadId").hide();
    $("#AddId").hide();
    $("#idDropTableShow").hide();

    // Sending a request to change the number of records displayed on the page
    $('#idPageSize').mouseup(function () {
        var open = $(this).data("isopen");
        if (open) {
            $.ajax({
                url: "/Admin/SetPageSize",
                type: "GET",
                data: { PageSize: $('select[id=idPageSize] option:selected').val() },
                dataType: "html",
                success: function (data) {
                    $("#idCountRecords").text(data);
                    $("#idCountRecords").css("color", "black");

                    var someObject = {
                        method: "Load",
                        table: $('select[id=idTables] option:selected').val(),
                        url: "/Admin/ListView"
                    };
                    AjaxLoad(someObject);
                },
                error: function () {
                    $("#idCountRecords").text(data);
                    $("#idCountRecords").css("color", "red");
                }
            });
        }
        $(this).data("isopen", !open);
    });

    // Sending a request to download records from a table(partial view)
    $('#LoadId').click(function () {
        if ($('select[id=idTables] option:selected').index() === 0) { return; }

        var someObject = {
            method: "Load",
            table: $('select[id=idTables] option:selected').val(),
            url: "/Admin/ListView"
        };
        AjaxLoad(someObject);
    });

    // Sending a request to open a form(partial view) of adding a new record
    $('#AddId').click(function () {
        if ($('select[id=idTables] option:selected').index() === 0) { return; }

        var someObject = {
            method: "Add",
            table: $('select[id=idTables] option:selected').val(),
            url: "/Admin/AddEditShow"
        };
        AjaxLoad(someObject);
    });

    // Sending a request to open a form(partial view) of editing the selected record
    $('#EditId').click(function () {
        if ($('select[id=idTables] option:selected').index() === 0) { return; }

        var someObject = {
            method: "Update",
            table: $('select[id=idTables] option:selected').val(),
            url: "/Admin/AddEditShow"
        };
        AjaxLoad(someObject);
    });

    // Sending a request to set the desired value in cookies
    $('#SearchField').on('input', function () {

        if ($('select[id=idTables] option:selected').index() === 0) { return; }

        $.ajax({
            url: "/Admin/SetSearchValue",
            type: "GET",
            data: { SearchValue: $('#SearchField').val() },
            dataType: "html",
            success: function () {
                var someObject = {
                    method: "Load",
                    table: $('select[id=idTables] option:selected').val(),
                    url: "/Admin/ListView"
                };
                AjaxLoad(someObject);
            },
            error: function () {
                AfterSendError();
            }
        });
    });

    // Loading and adjusting the html editor on the page
    $("#idLoadEditor").click(function () {
        var currentLang = $("#currentLang").val();

        if (currentLang === "en") {
            currentLang = "en_gb";
        }
        else if (currentLang === "zh") {
            currentLang = "zh_cn";
        }
        else if (currentLang === "ja") {
            currentLang = "ja";
        }

        var currentTable = $('select[id=idTables] option:selected').val();

        if (currentTable === "Posts" || currentTable === "PostsTranslations" ||
            currentTable === "Books" || currentTable === "BooksTranslations" ||
            currentTable === "Scientists" || currentTable === "ScientistsTranslations" ||
            currentTable === "Events" || currentTable === "EventsTranslations" ||
            currentTable === "Journals" || currentTable === "JournalsTranslations" || 
            currentTable === "AboutProject") {
            $(function () {
                $(".edit").froalaEditor({
                    // Set max size of files (50 MB)
                    fileMaxSize: 1024 * 1024 * 50,

                    // Set max size of images (10 MB)
                    imageMaxSize: 1024 * 1024 * 10,

                    // Set max size of videos (100 MB)
                    videoMaxSize: 1024 * 1024 * 100,

                    // Set max value of textarea
                    charCounterMax: 250000,

                    // Set current language
                    language: currentLang,

                    // Set the image upload URL.
                    imageUploadURL: '/UploadImage',
                    imageUploadParams: { typeFile: "image" },

                    // Set the file upload URL.
                    fileUploadURL: '/UploadFile',
                    fileUploadParams: { typeFile: "file" },

                    // Set the video upload URL.
                    videoUploadURL: '/UploadVideo',
                    videoUploadParams: { typeFile: "video" }
                })
                    .on('froalaEditor.image.removed', function (e, editor, $file) {
                        $.ajax({
                            // Request method.
                            method: "POST",
                            // Request URL.
                            url: "/DeleteImage",
                            // Request params.
                            data: {
                                src: $file.attr('src')
                            }
                        }).done(function (data) {
                            console.log(JSON.stringify(data));
                        }).fail(function (err) {
                            console.log(JSON.stringify(err));
                        });
                    })
                    .on('froalaEditor.video.removed', function (e, editor, $file) {
                        $.ajax({
                            // Request method.
                            method: "POST",
                            // Request URL.
                            url: "/DeleteVideo",
                            // Request params.
                            data: {
                                src: $file.attr('src')
                            }
                        }).done(function (data) {
                            console.log(JSON.stringify(data));
                        }).fail(function (err) {
                            console.log(JSON.stringify(err));
                        });
                    })
                    .on('froalaEditor.file.removed', function (e, editor, $file) {
                        $.ajax({
                            // Request method.
                            method: "POST",
                            // Request URL.
                            url: "/DeleteFile",
                            // Request params.
                            data: {
                                src: $file.attr('src')
                            }
                        }).done(function (data) {
                            console.log(JSON.stringify(data));
                        }).fail(function (err) {
                            console.log(JSON.stringify(err));
                        });
                    });

                $("#p-max-length-element").remove();
                $('.edit').froalaEditor('html.set', $(".edit").val());
            });
        }
    });

    // Sending a request to delete all records from the table
    $("#idDropTable").click(function () {
        if ($('select[id=idTables] option:selected').index() === 0) { return; }

        $.ajax({
            url: "/Admin/DropTable",
            type: "POST",
            data: {
                Table: $('select[id=idTables] option:selected').val()
            },
            dataType: "html",
            beforeSend: function () {
                ModalHide();
            },
            success: function (data) {
                if (JSON.parse(data).droptable === true) {
                    var someObject = {
                        method: "Load",
                        table: $('select[id=idTables] option:selected').val(),
                        url: "/Admin/ListView"
                    };
                    AjaxLoad(someObject);
                }
                else {
                    $("#tdPartialViewId").css("text-align", "center");
                    $("#PartialViewId").html("<h1>" + JSON.parse(data).message + "</h1><br>");
                }
            },
            error: function () {
                AfterSendError();
            }
        });
    });

    // Hides and shows additional functionality buttons depending on the selected table
    $("#idTables").change(function () {

        if ($('select[id=idTables] option:selected').index() === 0) {
            $("#idDropTableShow").hide();
            $("#LoadId").hide();
            $("#AddId").hide();
        }
        else {
            $("#idDropTableShow").show();
            $("#LoadId").show();
            $("#AddId").show();
        }

        if (this.value.includes("Posts") || this.value.includes("Books") ||
            this.value.includes("Scientists") || this.value.includes("AboutProject") ||
            this.value.includes("Events") || this.value.includes("Journals")) {
            if (this.value.includes("Rating") || this.value.includes("Comments")) {
                $("#idHrHtmlEditor").hide();
                $("#idLoadEditor").hide();
            } else {
                $("#idHrHtmlEditor").show();
                $("#idLoadEditor").show();
            }
        }
        else {
            $("#idHrHtmlEditor").hide();
            $("#idLoadEditor").hide();
        }
    });
});

// Setting the partial view output area after a successful query
function SuccessSend(data) {
    $("#tdPartialViewId").css("text-align", "left");
    $("#PartialViewId").html(data);
}

// Setting the partial view output area before querying
function BeforeSend() {
    $("#tdPartialViewId").css("text-align", "center");
    $('#PartialViewId').html("<img src=\"../gif/admin/Loading.gif\"/>");
}

// Setting the partial view output area after a failed request
function AfterSendError() {
    $("#tdPartialViewId").css("text-align", "center");
    $("#PartialViewId").html("<img src=\"../images/admin/adminError.png\" style=\"width: 200px; height: 200px; margin-top: 10%;\"/><br>");
}

// Add - on above standard modal window.This method hides the screen dimming and unlocks it after hiding the modal window.
function ModalHide() {
    $(".modal").modal("hide");
    $("body").removeClass("modal-open");
    $('.modal-backdrop').remove();
}

//Ensuring sending ajax pictures by request to the server
function UploadImg() {
    var currentTable = $('select[id=idTables] option:selected').val();

    if (currentTable === "Posts" || currentTable === "Books" || currentTable === "Scientists") {
        var status = $("select[id=fileInputStatus] option:selected").val();
        var table = $("#currentTable").val();
        var editId = $('input[name=Id]').val();

        var formData = new FormData();
        formData.append("fileImg", $("#IdFileImg")[0].files[0]);
        formData.append("status", status);
        formData.append("table", table);
        formData.append("id", editId);

        var other_data = $("#ApplyForm").serializeArray();
        $.each(other_data, function (_key, input) {
            formData.append(input.name, input.value);
        });

        $.ajax({
            url: "/Admin/ApplyImgFile",
            type: "POST",
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function () {
                console.log("success upload img");
            },
            error: function () {
                console.log("UploadImg() method error");
            }
        });
    }
}

// Provides ajax request for incoming json object
function AjaxLoad(data) {
    if ($('select[id=idTables] option:selected').index() === 0) { return; }

    $.ajax({
        url: data.url,
        type: "POST",
        data: {
            Table: $('select[id=idTables] option:selected').val(),
            Page: data.page,
            Method: data.method,
            Message: data.message
        },
        dataType: "html",
        beforeSend: function () {
            BeforeSend();
        },
        success: function (data) {
            SuccessSend(data);
        },
        error: function () {
            AfterSendError();
        }
    });

    $("#TableId").html("<p style=\"margin-left: 5px;\">" + $('select[id=idTables] option:selected').text() + "</p>");
}

// Reload page
function ReloadPage() { document.location.reload(true); }

// Redirect to the page whose address is specified in the incoming object
function OpenReturnUrl(data) { location.href = data.url; }