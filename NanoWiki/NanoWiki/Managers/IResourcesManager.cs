﻿using System.Resources;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Responsible for getting data from their file translations resource
    /// </summary>
    public interface IResourcesManager
    {
        /// <summary>
        /// Getting value from file resource by key
        /// </summary>
        /// <typeparam name="T">Resource type</typeparam>
        /// <param name="key">Resource key</param>
        /// <returns>Key translation value</returns>
        string GetResourceTitle<T>(string key) => new ResourceManager(typeof(T)).GetString(key);
    }
}