﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using NanoWiki.Classes;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Helper class for pages
    /// </summary>
    public class PageManager
    {
        /// <summary>
        /// Getting project language
        /// </summary>
        /// <param name="httpContextaccessor">Object from the controller</param>
        /// <returns>Language abbreviations</returns>
        public static string GetLanguage(IHttpContextAccessor httpContextaccessor)
        {
            string lang = httpContextaccessor.HttpContext.Request.Cookies[CookieRequestCultureProvider.DefaultCookieName];

            if (string.IsNullOrEmpty(lang))
                return GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation");
            else
                return lang.Substring(2, 2);
        }

        /// <summary>
        /// Getting count of records on the page
        /// </summary>
        /// <param name="httpContextaccessor">Object from the controller</param>
        /// <returns>Number of lines per page</returns>
        public static int GetPageSize(IHttpContextAccessor httpContextaccessor)
        {
            int pageSize = (int)(httpContextaccessor.HttpContext.Session
                .GetInt32("PageSize").HasValue ? httpContextaccessor.HttpContext.Session.GetInt32("PageSize") : 5);

            return pageSize;
        }
    }
}