﻿using System.Collections.Generic;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Interface that provides data from a model
    /// </summary>
    public interface IModelAttributesManager
    {
        /// <summary>
        /// Getting the value of the length attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the length attribute</returns>
        int GetLengthAttributeValue(string propertyName, object model);

        /// <summary>
        /// Getting the value of the display attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the display attribute</returns>
        string GetDisplayAttributeValue(string propertyName, object model);

        /// <summary>
        /// Getting the value of the requried attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the requried attribute</returns>
        bool GetRequiredAttributeValue(string propertyName, object model);

        /// <summary>
        /// Getting the value of the foreign key attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the foreign key attribute</returns>
        bool GetForeignKeyAttributeValue(string propertyName, object model);

        /// <summary>
        /// Getting the value of the data type attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the data type attribute</returns>
        object GetDataTypeAttributeValue(string propertyName, object model);

        /// <summary>
        /// Getting a list of field names from the MVC model
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>List with the names of the MVC model fields in the language of the site</returns>
        List<string> GetFieldsName(object model);
    }
}