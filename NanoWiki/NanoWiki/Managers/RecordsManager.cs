﻿using NanoWiki.Classes;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Helper class for working with strings when displaying them on the screen
    /// </summary>
    public class RecordsManager
    {
        /// <summary>
        /// Post icon, depending on the data type found
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>Post icon, depending on the data type found</returns>
        public static string GetPostIcon(object model)
        {
            var tableName = model.GetType().Name;

            switch (tableName)
            {
                case "Books":
                case "BooksTranslations":
                    return "post-cata cata-sm cata-success";

                case "Journals":
                case "JournalsTranslations":
                    return "post-cata cata-sm cata-success";

                case "Scientists":
                case "ScientistsTranslations":
                    return "post-cata cata-sm cata-primary";

                case "Posts":
                case "PostsTranslations":
                    return "post-cata cata-sm cata-danger";

                case "Events":
                case "EventsTranslations":
                    return "post-cata cata-sm cata-events";

                default:
                    return "post-cata cata-sm cata-success";
            }
        }

        /// <summary>
        /// Checking if the table with scientists is open
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>The result of checking whether the model is a model with a scientist</returns>
        public static bool GetInitials(object model)
        {
            var tableName = model.GetType().Name;

            if (tableName.Equals("Scientists") || tableName.Equals("ScientistsTranslations"))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Getting the title of a post depending on the language in which the post is open
        /// </summary>
        /// <param name="lang">Current language</param>
        /// <param name="initialsStatus">Indicator for opening a table with initials</param>
        /// <param name="model">Model with data</param>
        /// <returns>Post title according to language and table</returns>
        public static string GetPostName(string lang, bool? initialsStatus, object model)
        {
            string postName, temp;

            if (initialsStatus.HasValue && initialsStatus == true)
                temp = model.GetType().GetProperty("Initials").GetValue(model).ToString();
            else
                temp = model.GetType().GetProperty("Name").GetValue(model).ToString();

            if (string.IsNullOrEmpty(lang))
                lang = GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation");

            if (lang.Equals("zh") || lang.Equals("ja"))
                postName = temp.Substring(0, temp.Length > 100 ? 100 : temp.Length);
            else
                postName = temp.Substring(0, temp.Length > 100 ? 100 : temp.Length);

            return postName;
        }

        /// <summary>
        /// Getting the description of a post depending on the language in which the post is open
        /// </summary>
        /// <param name="lang">Current language</param>
        /// <param name="initialsStatus">Indicator for opening a table with initials</param>
        /// <param name="model">Model with data</param>
        /// <returns>Post description according to language and table</returns>
        public static string GetPostDescription(string lang, bool? initialsStatus, object model)
        {
            string postDescription, temp;

            if (initialsStatus.HasValue && initialsStatus == true)
                temp = model.GetType().GetProperty("Autobiography").GetValue(model).ToString();
            else
                temp = model.GetType().GetProperty("Description").GetValue(model).ToString();

            if (string.IsNullOrEmpty(lang))
                lang = GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation");

            if (lang.Equals("zh") || lang.Equals("ja"))
                postDescription = temp.Substring(0, temp.Length > 600 ? 100 : temp.Length);
            else
                postDescription = temp.Substring(0, temp.Length > 600 ? 100 : temp.Length);

            return postDescription;
        }
    }
}