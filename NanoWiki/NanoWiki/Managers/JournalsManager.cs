﻿using NanoWiki.Classes;
using NanoWiki.Models.Database;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Helper class for working with the "Journals", "JournalsTranslations" tables
    /// </summary>
    public class JournalsManager
    {
        /// <summary>
        /// Getting author initials via table foreign key
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>Author initials</returns>
        public static string GetAuthorInitials(object model)
        {
            var name = model.GetType().Name;

            string author;

            if (name.IndexOf("Translations") == -1)
            {
                var fkId = (int)model.GetType().GetProperty("FkAuthors").GetValue(model);

                author = NanoWikiConnector.GetById<Authors>(fkId).Initials;
            }
            else
            {
                var fkId = (int)model.GetType().GetProperty("FkAuthorsTranslations").GetValue(model);

                author = NanoWikiConnector.GetById<AuthorsTranslations>(fkId).Initials;
            }

            return author;
        }

        /// <summary>
        /// Getting organization name via table foreign key
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>Organization name</returns>
        public static string GetOrganizationName(object model)
        {
            var name = model.GetType().Name;

            string organization;

            if (name.IndexOf("Translations") == -1)
            {
                var fkId = (int)model.GetType().GetProperty("FkOrganizations").GetValue(model);

                organization = NanoWikiConnector.GetById<Organizations>(fkId).Name;
            }
            else
            {
                var fkId = (int)model.GetType().GetProperty("FkOrganizationsTranslations").GetValue(model);

                organization = NanoWikiConnector.GetById<OrganizationsTranslations>(fkId).Name;
            }

            return organization;
        }

        /// <summary>
        /// Getting the number of citations by a foreign key
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>The number of citations by a foreign key</returns>
        public static int GetCitationCount(object model)
        {
            var name = model.GetType().Name;

            var id = (int)model.GetType().GetProperty("Id").GetValue(model);

            int countCitation;

            if (name.IndexOf("Translations") == -1)
            {
                countCitation = NanoWikiConnector.Count("JournalsPublications", "FkJournalsArticles", id);
            }

            else
            {
                countCitation = NanoWikiConnector.Count("JournalsTranslationsPublications", "FkJournalsTranslationsArticles", id);
            }

            return countCitation;
        }

        /// <summary>
        /// Getting article name via table foreign key
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>Article name</returns>
        public static string GetArticleName(object model)
        {
            var name = model.GetType().Name;

            var articleName = "";

            if (name.Equals("Publications"))
            {
                var id = (int)model.GetType().GetProperty("FkJournalsArticles").GetValue(model);

                articleName = NanoWikiConnector.GetById<JournalsArticles>(id).Name;
            }
            else if (name.Equals("PublicationsTranslation"))
            {
                var id = (int)model.GetType().GetProperty("FkJournalsTranslationsArticles").GetValue(model);

                articleName = NanoWikiConnector.GetById<JournalsTranslationsArticles>(id).Name;
            }

            return articleName;
        }
    }
}