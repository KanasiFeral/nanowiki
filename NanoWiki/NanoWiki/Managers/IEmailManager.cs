﻿using NanoWiki.Models.Common;
using NanoWiki.Models.Database;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Interface responsible for sending Email
    /// </summary>
    public interface IEmailManager
    {
        /// <summary>
        /// Check email Address for correctness
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>Result of checking</returns>
        bool IsEmailValid(string emailAddress);

        /// <summary>
        /// The final generation of the letter with a temporary link. This method accepts the user model, the previously generated link and the status of whether the user has confirmed the email Address
        /// </summary>
        /// <param name="adminModel">Admin model with data</param>
        /// <param name="userModel">User model with data</param>
        /// <param name="TimeLinkUrl">Link to go</param>
        /// <param name="ConfirmEmail">Verified email status</param>
        /// <returns>Generated navigation link</returns>
        string SendTimeLink(Admins adminModel, Users userModel, string TimeLinkUrl, bool ConfirmEmail);

        /// <summary>
        /// Sending a letter through Google account associated with this project
        /// </summary>
        /// <param name="messageModel">Model for generating a letter</param>
        /// <param name="ToMe">Who are we sending?</param>
        /// <returns>Submission result</returns>
        string SendEmail(MessageModel messageModel, bool ToMe);
    }
}