﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NanoWiki.Resources;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Class that provides data from a model
    /// </summary>
    public class ModelAttributesManager : IModelAttributesManager
    {
        private readonly ResourcesManager _resourcesManager;

        public ModelAttributesManager() => _resourcesManager = new ResourcesManager();

        /// <summary>
        /// Getting the value of the length attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the length attribute</returns>
        public int GetLengthAttributeValue(string propertyName, object model)
        {
            var memberInfo = model.GetType().GetMember(propertyName);

            var attributes = memberInfo.FirstOrDefault()
                .GetCustomAttributes(typeof(StringLengthAttribute), false);

            if (attributes.FirstOrDefault() != null)
            {
                // Attempt to get attribute length
                var result = ((StringLengthAttribute)attributes
                    .FirstOrDefault()).MaximumLength;

                return result == 0 ? 2000 : result;
            }

            return 0;
        }

        /// <summary>
        /// Getting the value of the display attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the display attribute</returns>
        public string GetDisplayAttributeValue(string propertyName, object model)
        {
            var memberInfo = model.GetType().GetMember(propertyName);

            var attributes = memberInfo.FirstOrDefault().GetCustomAttributes(typeof(DisplayAttribute), false);

            if (attributes.FirstOrDefault() != null)
            {
                // Attempt to get attribute length
                var result = ((DisplayAttribute)attributes.FirstOrDefault()).Name;

                // If the name is empty, then return the void. Otherwise, we get a multilingual name 
                // from the file resource.
                if (string.IsNullOrEmpty(result))
                    return "";
                else
                    return _resourcesManager.GetResourceTitle<Translations>(result);
            }

            return null;
        }

        /// <summary>
        /// Getting the value of the requried attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the requried attribute</returns>
        public bool GetRequiredAttributeValue(string propertyName, object model)
        {
            var memberInfo = model.GetType().GetMember(propertyName);

            var attributes = memberInfo.FirstOrDefault()
                .GetCustomAttributes(typeof(RequiredAttribute), false);

            // Check whether attribute has binding property
            if (attributes.Length == 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Getting the value of the foreign key attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the foreign key attribute</returns>
        public bool GetForeignKeyAttributeValue(string propertyName, object model)
        {
            var memberInfo = model.GetType().GetMember(propertyName);

            var attributes = memberInfo.FirstOrDefault()
                .GetCustomAttributes(typeof(ForeignKeyAttribute), false);

            // Check whether attribute has binding property
            if (attributes.Length == 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Getting the value of the data type attribute
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <param name="model">Model with data</param>
        /// <returns>Value of the data type attribute</returns>
        public object GetDataTypeAttributeValue(string propertyName, object model)
        {
            var memberInfo = model.GetType().GetMember(propertyName);

            var attributes = memberInfo.FirstOrDefault()
                .GetCustomAttributes(typeof(DataTypeAttribute), false);

            if (attributes.FirstOrDefault() != null)
                return ((DataTypeAttribute)attributes.FirstOrDefault()).DataType;

            return null;
        }

        /// <summary>
        /// Getting a list of field names from the MVC model
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>List with the names of the MVC model fields in the language of the site</returns>
        public List<string> GetFieldsName(object model)
        {
            // Sheet for storing the list of MVC model fields
            List<string> list = new List<string>();
            // Getting the list of MVC model properties
            PropertyInfo[] properties = model.GetType().GetProperties();

            // The cycle on the properties of the MVC model
            foreach (var item in properties)
            {
                try
                {
                    // Skip foreign key field
                    if (!GetForeignKeyAttributeValue(item.Name, model))
                    {
                        object value;
                        string text = "";

                        try
                        {
                            value = GetDisplayAttributeValue(item.Name, model);

                            if (value == null)
                            {
                                text = Translations.NotFound;
                            }
                            else
                            {
                                text = Convert.ToString(value);
                            }
                        }
                        catch (Exception exc)
                        {
                            text = exc.Message;
                        }

                        // Getting a multilingual name
                        list.Add(text.ToString());
                    }
                }
                catch
                {
                    // Else add missing title
                    list.Add(Translations.NoContent);
                }
            }

            return list;
        }
    }
}