﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Class for converting files to base64 and back
    /// </summary>
    public class FileConvertingManager
    {
        /// <summary>
        /// Converting to base64 format
        /// </summary>
        /// <param name="file">Image to convert</param>
        /// <returns>Image converted to base64 string</returns>
        public static string ConvertImageToBase64(IFormFile file)
        {
            string base64String;

            using var memStream = new MemoryStream();
            file.CopyTo(memStream);
            byte[] imageBytes = memStream.ToArray();
            base64String = Convert.ToBase64String(imageBytes);

            return base64String;
        }

        /// <summary>
        /// Generating a string to display the base64 image correctly
        /// </summary>
        /// <param name="img">Base64 file</param>
        /// <returns>String for correct display of base64 image on HTML page</returns>
        public static string LoadBase64OrDefault(string img)
        {
            if (string.IsNullOrEmpty(img))
                return null;

            if (img.IndexOf("default.png") != -1)
                return img.Replace("~", "../..");
            else
                return $"data:image/png;base64, {img}";
        }
    }
}