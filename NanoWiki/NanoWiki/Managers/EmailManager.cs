﻿using System;
using System.Net;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using NanoWiki.Classes;
using NanoWiki.Resources;
using NanoWiki.Models.Common;
using NanoWiki.Models.Database;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Class responsible for sending Email
    /// </summary>
    public class EmailManager: IEmailManager
    {
        /// <summary>
        /// Check email Address for correctness
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>Result of checking</returns>
        public bool IsEmailValid(string emailAddress)
        {
            // Flag correctness
            bool isValid;
            try
            {
                // First validation check with built-in class
                MailAddress m = new MailAddress(emailAddress);
                isValid = true;
            }
            catch
            {
                isValid = false;
            }

            // Regular Expression for email verification
            Regex regexEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            if (!regexEmail.IsMatch(emailAddress))
                isValid = false;

            return isValid;
        }

        /// <summary>
        /// The final generation of the letter with a temporary link. This method accepts the user model, the previously generated link and the status of whether the user has confirmed the email Address
        /// </summary>
        /// <param name="adminModel">Admin model with data</param>
        /// <param name="userModel">User model with data</param>
        /// <param name="TimeLinkUrl">Link to go</param>
        /// <param name="ConfirmEmail">Verified email status</param>
        /// <returns>Generated navigation link</returns>
        [HttpPost]
        public string SendTimeLink(Admins adminModel, Users userModel, string TimeLinkUrl, bool ConfirmEmail)
        {
            // Dispatch model
            MessageModel messageModel = new MessageModel();

            if (adminModel != null)
            {
                messageModel.EmailTo = adminModel.Email;
            }
            else
            {
                messageModel.EmailTo = userModel.Email;
            }

            if (ConfirmEmail == false)
            {
                messageModel.Title = Translations.PasswordRecovery;
                messageModel.Message = TimeLinkUrl;
            }
            else
            {
                messageModel.Title = Translations.ConfirmEmail;
                messageModel.Message = $"{Translations.LinkForConfirmationEmail}.<br />{TimeLinkUrl}<br />" +
                    $"{Translations.DayToConfirmEmail}<br />{Translations.UserNameToSignIn}:<b>{ userModel.Email}<b/>";
            }

            // Sending a letter and receiving the result of sending
            var responseToString = SendEmail(messageModel, false);

            responseToString += ConfirmEmail == false ? $". {Translations.ToContinueChangingPassword}" : $". {Translations.ToContinueRegistration}";

            return responseToString;
        }

        /// <summary>
        /// Sending a letter through Google account associated with this project
        /// </summary>
        /// <param name="messageModel">Model for generating a letter</param>
        /// <param name="ToMe">Who are we sending?</param>
        /// <returns>Submission result</returns>
        [HttpPost]
        public string SendEmail(MessageModel messageModel, bool ToMe)
        {
            try
            {
                // Allows applications to send email by using the Simple Mail Transfer Protocol (SMTP). 
                // The SmtpClient type is now obsolete.
                SmtpClient client = new SmtpClient
                {
                    Port = 587,
                    Host = "smtp.gmail.com",
                    EnableSsl = true,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false
                };

                // Data for sending mail from the Address of the administration
                string Email = Encryption.Decrypt(GlobalValues.GetAppSettingsString("NanoWikiHelp:Login"));
                string Password = Encryption.Decrypt(GlobalValues.GetAppSettingsString("NanoWikiHelp:Password"));

                // Provides credentials for password-based authentication schemes such as basic, 
                // digest, NTLM, and Kerberos authentication.
                client.Credentials = new NetworkCredential(Email, Password);

                // Represents an email message that can be sent using the SmtpClient class.
                MailMessage mailMessage;

                // If you send a letter to the administration email
                if (ToMe == false)
                {
                    mailMessage = new MailMessage(Email, messageModel.EmailTo, messageModel.Title, messageModel.Message);
                }
                // If you send a letter to the user email
                else
                {
                    mailMessage = new MailMessage(messageModel.EmailTo, Email, messageModel.Title, messageModel.Message);
                }

                mailMessage.BodyEncoding = Encoding.UTF8;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.IsBodyHtml = true;

                client.Send(mailMessage);

                GlobalValues.SendMessage = true;
                return Translations.MessageSentSuccessfully + "\n";
            }
            catch (Exception exc)
            {
                GlobalValues.SendMessage = false;
                return exc.Message + "\n";
            }
        }
    }
}