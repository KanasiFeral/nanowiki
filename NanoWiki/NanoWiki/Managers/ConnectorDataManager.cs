﻿using System.Reflection;
using System.Collections.Generic;
using NanoWiki.Classes;
using NanoWiki.Resources;
using NanoWiki.Models.Database;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Helper class for working with data from the database
    /// </summary>
    public class ConnectorDataManager
    {
        /// <summary>
        /// Getting an abbreviation of a language based on its id
        /// </summary>
        /// <param name="id">Language id</param>
        /// <returns>Abbreviation of a language based on its id</returns>
        public static string GetLangById(int id) => NanoWikiConnector.GetById<Languages>(id).Abbreviation;

        /// <summary>
        /// Getting the id of the language on the selected abbreviation
        /// </summary>
        /// <param name="lang">Language abbreviation</param>
        /// <returns>Id of the language on the selected abbreviation</returns>
        public static int GetLangIdByName(string lang) => NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).Id;

        /// <summary>
        /// Get a list of all languages from the database
        /// </summary>
        /// <returns>List of all languages from the database</returns>
        public static List<Languages> GetLanguagesList() => NanoWikiConnector.Get<Languages>();

        /// <summary>
        /// Get contacts from database based on selected language
        /// </summary>
        /// <param name="lang">Language abbreviation</param>
        /// <returns>Contacts from database based on selected language</returns>
        public static Contacts GetContacts(string lang) => NanoWikiConnector.GetSingle<Contacts>("FkLanguages", GetLangIdByName(lang)) ?? new Contacts() { Address = "", Phone = "", Email = "" };

        /// <summary>
        /// Generation of important (latest) news in html code. Data is taken from a database based on the selected language.
        /// </summary>
        /// <param name="lang">Language abbreviation</param>
        /// <returns>Ismportant (latest) news in HTML code</returns>
        public static string GetBreakingNews(string lang)
        {
            var postName = "";
            var postId = "";
            var breakingNews = "";
            var currentTable = "";
            var fkFieldName = "";
            var typePost = "";
            var fkId = "";
            var langId = GetLangIdByName(lang);
            var conditions = new List<Condition>();

            for (int i = 0; i < 4; i++)
            {
                switch (i)
                {
                    case 0:
                        currentTable = "Posts";
                        fkFieldName = "FkPosts";
                        typePost = "post-cata cata-sm cata-success";
                        break;
                    case 1:
                        currentTable = "Books";
                        fkFieldName = "FkBooks";
                        typePost = "post-cata cata-sm cata-primary";
                        break;
                    case 2:
                        currentTable = "Scientists";
                        fkFieldName = "FkScientists";
                        typePost = "post-cata cata-sm cata-danger";
                        break;
                    case 3:
                        currentTable = "Events";
                        fkFieldName = "FkEvents";
                        typePost = "post-cata cata-sm cata-events";
                        break;
                    default:
                        currentTable = "Posts";
                        fkFieldName = "FkPosts";
                        typePost = "post-cata cata-sm cata-success";
                        break;
                }

                if (lang.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) != -1)
                {
                    currentTable = currentTable.Replace("Translations", "");
                }
                else
                {
                    currentTable += "Translations";
                    conditions.Add(new Condition() { Field = "FkLanguages", Value = langId });
                }

                if (conditions.Count == 0)
                {
                    postId = NanoWikiConnector.MaxId(currentTable);

                    if (string.IsNullOrEmpty(postId))
                    {
                        return null;
                    }

                    conditions.Add(new Condition() { Field = "Id", Value = postId });
                }
                else
                {
                    postId = NanoWikiConnector.GetValue(currentTable, "Id", conditions);
                }

                if (!string.IsNullOrEmpty(postId))
                {
                    if (lang.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) == -1)
                        fkId = NanoWikiConnector.GetValue(currentTable, fkFieldName, conditions);

                    fkId = string.IsNullOrEmpty(fkId) ? "0" : fkId;

                    if (currentTable.IndexOf("Scientists") != -1)
                        postName = NanoWikiConnector.GetValue(currentTable, "Initials", conditions);
                    else
                        postName = NanoWikiConnector.GetValue(currentTable, "Name", conditions);

                    if (string.IsNullOrEmpty(postName))
                        continue;

                    if (i == 0)
                    {
                        breakingNews += $"<li class='tickerHook' style='top: 0px; left: 0px; position: absolute; display: block; opaCity: 1; z - index: 99; '><a href='/Home/SinglePost?id={postId}&table={currentTable}&fkId={fkId}&typePost={typePost}'>{postName.Substring(0, postName.Length > 40 ? 40 : postName.Length)}</a></li>";
                    }                        
                    else
                    {
                        breakingNews += $"<li class=\"\" style=\"top: -3em; left: 0px; position: absolute; display: block; opaCity: 0; z - index: 98; \"><a href='/Home/SinglePost?id={postId}&table={currentTable}&fkId={fkId}&typePost={typePost}'>{postName.Substring(0, postName.Length > 40 ? 40 : postName.Length)}</a></li>";
                    }
                }

            }

            return breakingNews;
        }

        /// <summary>
        /// Generates a drop-down list based on post status
        /// </summary>
        /// <param name="field">Table field</param>
        /// <param name="currentStatus">Publication status</param>
        /// <returns>Drop-down HTML list based on post status</returns>
        public static string GetPostStatusComboBox(string field, bool currentStatus)
        {
            string HtmlComboBox = $"<select class=\"form-control adminFormControl\" name=\"{field}\" data-validation=\"required\">";

            if (currentStatus == true)
            {
                HtmlComboBox += $"<option value=\"{true}\" selected>{Translations.Published}</option>";
                HtmlComboBox += $"<option value=\"{false}\">{Translations.NotPublished}</option>";
            }
            else
            {
                HtmlComboBox += $"<option value=\"{true}\">{Translations.Published}</option>";
                HtmlComboBox += $"<option value=\"{false}\" selected>{Translations.NotPublished}</option>";
            }

            return HtmlComboBox += "</select>";
        }

        /// <summary>
        /// Generates a drop-down list based on journal values
        /// </summary>
        /// <param name="field">Table field</param>
        /// <param name="value">Fild value</param>
        /// <returns>Drop-down HTML list based on journal values</returns>
        public static string GetJournalValuesComboBox(string field, string value)
        {
            string HtmlComboBox = $"<select class=\"form-control adminFormControl\" name=\"{field}\" data-validation=\"required\"><option value=''></option>";

            switch (field)
            {
                case "JournalLanguage":
                    List<Languages> languages = NanoWikiConnector.Get<Languages>();

                    if (languages.Count != 0)
                    {
                        foreach (var item in languages)
                        {
                            HtmlComboBox += $"<option {(item.Abbreviation.Equals(value) ? "selected" : "")} value=\"{item.Abbreviation}\">{item.Abbreviation}</option>";
                        }
                    }
                    break;
                case "SCOPUS":
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.IndexedToScopus, Translations.IndexedToScopus.Equals(value) ? "selected" : "");
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.IndexedTranslationVersion, Translations.IndexedTranslationVersion.Equals(value) ? "selected" : "");
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.IndexedPartially, Translations.IndexedPartially.Equals(value) ? "selected" : "");
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.NotIndexedInScopus, Translations.NotIndexedInScopus.Equals(value) ? "selected" : "");
                    break;
                case "ImpactFactorRSCI":
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.IndexedInRSCI, Translations.IndexedInRSCI.Equals(value) ? "selected" : "");
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.NotIndexedInRSCI, Translations.NotIndexedInRSCI.Equals(value) ? "selected" : "");
                    break;
                case "CurrentlyStatus":
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.ComingOutNow, Translations.ComingOutNow.Equals(value) ? "selected" : "");
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.NotComingOutNow, Translations.NotComingOutNow.Equals(value) ? "selected" : "");
                    break;
                case "RenameTransferInformation":
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.FullyTranslated, Translations.FullyTranslated.Equals(value) ? "selected" : "");
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.IndividualTranslated, Translations.IndividualTranslated.Equals(value) ? "selected" : "");
                    HtmlComboBox += string.Format("<option {1} value='{0}'>{0}</option>", Translations.DoesNotTranslate, Translations.DoesNotTranslate.Equals(value) ? "selected" : "");
                    break;
                default:
                    break;
            }

            return HtmlComboBox += "</select>";
        }

        /// <summary>
        /// Generate drop-down lists for setting a rating for a specific user to a specific post.
        /// </summary>
        /// <param name="field">Table field</param>
        /// <param name="likeRating"></param>
        /// <returns>Drop-down HTML lists for setting a rating for a specific user to a specific post</returns>
        public static string GetRatingComboBox(string field, bool likeRating)
        {
            string HtmlComboBox = $"<select class=\"form-control adminFormControl\" name=\"{field}\" data-validation=\"required\">";

            if (likeRating == true)
            {
                HtmlComboBox += $"<option value=\"{true}\" selected>{Translations.Yes}</option>";
                HtmlComboBox += $"<option value=\"{false}\">{Translations.No}</option>";
            }
            else
            {
                HtmlComboBox += $"<option value=\"{true}\">{Translations.Yes}</option>";
                HtmlComboBox += $"<option value=\"{false}\" selected>{Translations.No}</option>";
            }

            return HtmlComboBox += "</select>";
        }

        /// <summary>
        /// Getting the value of a foreign key from the database
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Record id</param>
        /// <returns>Value of a foreign key from the database</returns>
        public static int GetFKValueFromPages(string table, int id)
        {
            var value = NanoWikiConnector.GetValueById(table, $"Fk{table.Replace("Translations", "")}", id);

            return value == "" ? 0 : int.Parse(value.ToString());
        }

        /// <summary>
        /// Returns the value of a foreign key by id
        /// </summary>
        /// <param name="field">Table field</param>
        /// <param name="fkId">Foreign record id</param>
        /// <returns>Value of a foreign key by id</returns>
        public static string GetFkValue(string field, int fkId)
        {
            if (fkId <= 0 || string.IsNullOrEmpty(field))
                return null;

            string ReturnField = "Name";

            if (string.IsNullOrEmpty(field))
                return Translations.NotFound;

            string Table = field;
            Table = Table.Substring(2);

            if (field.Equals("FkLanguages"))
                ReturnField = "FullName";
            else if (field.Equals("FkAdmins") || field.Equals("FkUsers"))
                ReturnField = "Login";
            else if (field.IndexOf("FkScientists") != -1 || field.IndexOf("FkAuthors") != -1)
                ReturnField = "Initials";

            return NanoWikiConnector.GetValueById(Table, ReturnField, fkId);
        }

        /// <summary>
        /// Generates a drop-down list based on foreign keys. Instead of keys, their string values
        /// </summary>
        /// <param name="field">Table field</param>
        /// <param name="fkId">Foreign record id</param>
        /// <param name="table">Table name</param>
        /// <returns>Drop-down HTML list based on foreign keys</returns>
        public static string GetFkComboBox(string field, int? fkId, string table)
        {
            string HtmlComboBox;
            if (table.IndexOf("Comments") != -1)
                HtmlComboBox = $"<select class=\"form-control adminFormControl\" name = \"{field}\">";
            else
                HtmlComboBox = $"<select class=\"form-control adminFormControl\" name = \"{field}\" data-validation=\"required\">";

            string ReturnField = "Name";

            if (field.Equals("FkLanguages"))
                ReturnField = "FullName";
            else if (field.Equals("FkAdmins") || field.Equals("FkUsers"))
                ReturnField = "Login";
            else if (field.IndexOf("FkScientists") != -1 || field.IndexOf("FkAuthors") != -1)
                ReturnField = "Initials";
            else if (field.IndexOf("Comments") != -1)
                ReturnField = "Text";

            if ((table.IndexOf("Comments") != -1) && ((field.IndexOf("Comments") != -1) || 
                (field.IndexOf("Users") != -1) || (field.IndexOf("Admins") != -1)))
            {
                //add empty value for combo box for null foreign keys in table
                HtmlComboBox += $"<option value = \"\">{Translations.EmptyValue}</option>";
            }

            if (string.IsNullOrEmpty(field))
                return null;

            var listData = NanoWikiConnector.Get(field.Substring(2));

            if (listData == null)
                return null;

            foreach (var item in listData)
            {
                int id = 0;
                string value = "";

                foreach (PropertyInfo property in item.GetType().GetProperties())
                {
                    if (property.Name.Equals("Id"))
                    {
                        id = (int)property.GetValue(item, null);
                        continue;
                    }
                    else if (property.Name.Equals(ReturnField))
                    {
                        value = (string)property.GetValue(item, null);
                    }
                    else
                    {
                        continue;
                    }

                    if (id == fkId)
                    {
                        HtmlComboBox += $"<option value = \"{id}\" selected>{value.TrimEnd()} ({id})</option>";
                        break;
                    }
                    else
                    {
                        HtmlComboBox += $"<option value = \"{id}\">{value.TrimEnd()} ({id})</option>";
                        break;
                    }
                }
            }

            return HtmlComboBox += "</select>";
        }

        /// <summary>
        /// Getting the foreign key of a parent record in a table
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>Foreign key of a parent record in a table</returns>
        public static int GetFkId(object model)
        {
            var tableName = model.GetType().Name;

            return tableName switch
            {
                "BooksTranslations" => (int)model.GetType().GetProperty("FkBooks").GetValue(model, null),
                "JournalsTranslations" => (int)model.GetType().GetProperty("FkJournals").GetValue(model),
                "ScientistsTranslations" => (int)model.GetType().GetProperty("FkScientists").GetValue(model),
                "PostsTranslations" => (int)model.GetType().GetProperty("FkPosts").GetValue(model),
                "EventsTranslations" => (int)model.GetType().GetProperty("FkEvents").GetValue(model),
                "Publications" => (int)model.GetType().GetProperty("FkJournalsArticles").GetValue(model),
                "PublicationsTranslations" => (int)model.GetType().GetProperty("FkJournalsTranslationsArticles").GetValue(model),
                _ => 0,
            };
        }

        /// <summary>
        /// Get visit count in a table
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>Visit count in a table</returns>
        public static int GetVisitCount(object model)
        {
            var visitCount = (int)model.GetType().GetProperty("VisitCount").GetValue(model, null);

            return visitCount;
        }
    }
}