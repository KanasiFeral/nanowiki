﻿using System;
using NanoWiki.Classes;
using NanoWiki.Resources;

namespace NanoWiki.Managers
{
    /// <summary>
    /// The class responsible for working with comments
    /// </summary>
    public class CommentsManager
    {
        /// <summary>
        /// Property storing comments
        /// </summary>
        public static string ReplyBranches { get; set; }

        /// <summary>
        /// Clearing method of global variable storing comment thread
        /// </summary>
        /// <returns>Return null</returns>
        public static string CleanReplyBranches() => ReplyBranches = null;

        /// <summary>
        /// Getting the number of comments from a post based on the selected table and the id of the post
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="idPost">Post id</param>
        /// <returns>Comments count</returns>
        public static string GetCountComments(string table, int idPost) => NanoWikiConnector.Count($"{table}Comments", $"Fk{table}", idPost).ToString();

        /// <summary>
        /// Getting the second line of comments from the database, generating html code
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="commentId">Comment id</param>
        /// <param name="user">User id</param>
        /// <returns>Generated HTML line with internal comment thread</returns>
        public static string GetSecondLevelComments(string table, int commentId, bool user)
        {
            if (string.IsNullOrEmpty(table))
                return Translations.UpsErrorMessage;

            var replyComments = NanoWikiConnector.Get(table, $"Fk{table}", commentId);

            foreach (var comment in replyComments)
            {
                int idReplayComment;
                try { idReplayComment = (int)comment.GetType().GetProperty("Id").GetValue(comment, null); } catch (Exception) { idReplayComment = 0; }

                int fkReplyId;
                try { fkReplyId = (int)comment.GetType().GetProperties()[4].GetValue(comment, null); } catch (Exception) { fkReplyId = 0; }

                int userId;
                try { userId = (int)comment.GetType().GetProperty("FkUsers").GetValue(comment, null); } catch (Exception) { userId = 0; }

                int adminId;
                try { adminId = (int)comment.GetType().GetProperty("FkAdmins").GetValue(comment, null); } catch (Exception) { adminId = 0; }

                string commentDate = (string)comment.GetType().GetProperty("DateComment").GetValue(comment, null);
                string commentTime = (string)comment.GetType().GetProperty("TimeComment").GetValue(comment, null);
                string commentText = (string)comment.GetType().GetProperty("Text").GetValue(comment, null);

                string userLogin = UserManager.GetUserLoginByComments(comment);
                string userAvatar = UserManager.GetUserAvatar(comment);

                ReplyBranches += "<ol class='children'>" +
                "<li class='single_comment_area'>" +
                "<div class='comment-content d-flex'>" +
                "<div class='comment-author'>" +
                $"<img style='width: 100%; height: 100%;' src='{userAvatar}' alt='author'>" +
                "</div>" +
                "<div class='comment-meta'>" +
                $"<a href='#' class='comment-date'>{commentDate} | {commentTime}</a>" +
                $"<h6>{userLogin}</h6>" +
                $"<p style='word-break: break-all;'>{commentText}</p>" +
                "<div class='d-flex align-items-center'>" +
                //"<a href='#' class='like'>like</a>" +
                string.Format("{0}", user == false ?
                $"<a href='/Account/Authorization/' class='reply'>{Translations.Reply} {Translations.AuthorizationRequired}</a>" :
                $"<a href='javascript: void(0)' class='reply replySecondLvl' name='{userLogin}'>{Translations.Reply}</a>") +
                "</div></div></div>";

                // Generate reply branches using recursion
                if (idReplayComment != 0)
                {
                    if (NanoWikiConnector.Count(table, $"Fk{table}", idReplayComment) != 0)
                    {
                        GetSecondLevelComments(table, idReplayComment, user);
                    }
                }

                ReplyBranches += "</li></ol>";
            }

            return ReplyBranches;
        }

        /// <summary>
        /// Getting the status of whether an internal comment thread exists
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="commentId">Comment id</param>
        /// <returns>Status of whether an internal comment thread exists</returns>
        public static bool GetReplyBranchesStatus(string table, int commentId) => NanoWikiConnector.Get(table, $"Fk{table}", commentId) != null;
    }
}