﻿using System.Collections.Generic;
using NanoWiki.Classes;
using NanoWiki.Models.Database;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Returns data associated with a user
    /// </summary>
    public class UserManager
    {
        /// <summary>
        /// Getting a user's avatar based on its id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>Path to user avatar</returns>
        public static string GetUserAvatar(int? id)
        {
            if (id != 0)
            {
                var userProfile = NanoWikiConnector.GetSingle<UserProfile>("FkUsers", id);

                if (userProfile != null)
                {
                    return userProfile.Avatar;
                }
            }

            return "~/images/user/UserProfile/default.png";
        }

        /// <summary>
        /// Getting user login using comments model
        /// </summary>
        /// <param name="model">User model (table) with data</param>
        /// <returns>User login</returns>
        public static string GetUserLoginByComments(object model)
        {
            var fkUser = model.GetType().GetProperty("FkUsers").GetValue(model);
            var fkAdmin = model.GetType().GetProperty("FkAdmins").GetValue(model);

            string login = "";

            if ((int)fkUser > 0)
                login = NanoWikiConnector.GetById<Users>((int)fkUser).Login;
            else if ((int)fkAdmin > 0)
                login = NanoWikiConnector.GetById<Admins>((int)fkAdmin).Login;

            return login;
        }

        /// <summary>
        /// Getting the status of whether the user has changed the rating of the selected post
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="idPost">Post id</param>
        /// <param name="like">True - like, false - dislike pressed button</param>
        /// <param name="idUser">User id</param>
        /// <returns>Status of whether the user has changed the rating of the selected post</returns>
        public static bool GetUserClickRating(string table, int idPost, bool like, int? idUser)
        {
            if (!idUser.HasValue)
                return false;

            var status = NanoWikiConnector.GetValue($"{table}Rating", like == true ? "LikeRating" : "DislikeRating", new List<Condition>()
            {
                new Condition { Field = $"Fk{table}", Value = idPost },
                new Condition { Field = "FkUsers", Value = idUser }
            });

            return status != null && bool.Parse(status);
        }

        /// <summary>
        /// Getting a user's avatar based on its model data
        /// </summary>
        /// <param name="model">User model</param>
        /// <returns>Path to user avatar</returns>
        public static string GetUserAvatar(object model)
        {
            string avatar = "";

            var fkUser = model.GetType().GetProperty("FkUsers").GetValue(model);
            var fkAdmin = model.GetType().GetProperty("FkAdmins").GetValue(model);

            if ((int)fkUser > 0)
            {
                var userProfile = NanoWikiConnector.GetSingle<UserProfile>("FkUsers", (int)fkUser);

                if (userProfile != null)
                {
                    avatar = userProfile.Avatar ?? "~/images/user/UserProfile/default.png";
                }
                else
                {
                    avatar = "~/images/user/UserProfile/default.png";
                }
            }
            else if ((int)fkAdmin > 0)
            {
                var adminInfo = NanoWikiConnector.GetSingle<AdminInfo>("FkAdmins", (int)fkAdmin);

                if (adminInfo != null)
                {
                    avatar = adminInfo.Avatar ?? "~/images/user/AdminInfo/default.png";
                }
                else
                {
                    avatar = "~/images/user/AdminInfo/default.png";
                }
            }

            return FileConvertingManager.LoadBase64OrDefault(avatar);
        }
    }
}