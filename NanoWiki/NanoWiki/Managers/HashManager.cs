﻿using System.Security.Cryptography;
using System.Text;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Cslass responsible for generating hash codes
    /// </summary>
    public static class HashManager
    {
        /// <summary>
        /// MD5 hash generation based on incoming string
        /// </summary>
        /// <param name="text">Text for generating</param>
        /// <returns>MD5 text hash</returns>
        public static string CalculateMD5Hash(string text)
        {
            //Step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(text);
            byte[] hash = md5.ComputeHash(inputBytes);

            //Step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                //To make the hex string use lower-case letters instead of upper-case, 
                //replace the single line inside the for loop with this line
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}