﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using NanoWiki.Classes;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Returns various strings needed to work
    /// </summary>
    public static class StringsManager
    {
        /// <summary>
        /// Getting project namespace
        /// </summary>
        /// <returns>Project namespace</returns>
        private static string GetProjectNamespace() => MethodBase.GetCurrentMethod().DeclaringType.Namespace;

        /// <summary>
        /// Getting the name of the project as a string
        /// </summary>
        /// <returns>Project name</returns>
        public static string GetProjectName() => GetProjectNamespace().Substring(0, GetProjectNamespace().IndexOf("."));

        /// <summary>
        /// Getting a random string of a given length in uppercase
        /// </summary>
        /// <param name="length">Random length</param>
        /// <returns>Generated random string</returns>
        public static string GetRandomString(int length) => new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", length).Select(s => s[new Random().Next(s.Length)]).ToArray());

        /// <summary>
        /// Get post icons
        /// </summary>
        /// <param name="table">Table name</param>
        /// <returns>Key-value dictionary (icon-type post)</returns>
        public static Dictionary<string, string> GetTypePost(string table)
        {
            string icon = "fa fa-", typePost = "post-cata cata-sm cata-";

            switch (table)
            {
                case "Books":
                case "Bookstranslations":
                case "Journals":
                case "JournalsTranslations":
                    icon += "book";
                    typePost += "success";
                    break;
                case "Scientists":
                case "ScientistsTranslations":
                    icon += "user";
                    typePost += "primary";
                    break;
                case "Posts":
                case "PostsTranslations":
                    icon += "newspaper-o";
                    typePost += "danger";
                    break;
                case "Events":
                case "EventsTranslations":
                    icon += "building";
                    typePost += "events";
                    break;
                default:
                    icon += "trophy";
                    typePost += "success";
                    break;
            }

            return new Dictionary<string, string>
            {
                { "icon", icon },
                { "typePost", typePost }
            };
        }

        /// <summary>
        /// Change the path to files depending on the current OS
        /// </summary>
        /// <param name="path">The path to something in the format of the current operating system</param>
        /// <returns>Edited path to something in the system specified in the settings</returns>
        public static string GetPathByCurrentOS(string path)
        {
            var os = GlobalValues.GetAppSettingsString("ConnectionStrings:OS");

            if (os.Equals("Windows"))
            {
                path = path.Replace("/","\\");
            }

            return path;
        }
    }
}