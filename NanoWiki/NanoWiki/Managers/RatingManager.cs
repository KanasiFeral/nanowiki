﻿using System.Linq;
using System.Security.Claims;
using NanoWiki.Classes;
using NanoWiki.Models.Database;

namespace NanoWiki.Managers
{
    /// <summary>
    /// Class providing work with the rating of posts
    /// </summary>
    public class RatingManager
    {
        /// <summary>
        /// Generating a unique html id for a rated block
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>The generated HTML name of the rating block</returns>
        public static string GetBlockId(object model)
        {
            var name = model.GetType().Name;

            var id = model.GetType().GetProperty("Id").GetValue(model);

            return $"ratingBlock_{name}-{id}";
        }

        /// <summary>
        /// Generating a unique html id for a like block
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>The generated HTML name of the like block</returns>
        public static string GetLikeId(object model)
        {
            var name = model.GetType().Name;

            var id = model.GetType().GetProperty("Id").GetValue(model);

            return $"postLikeId_{name}-{id}";
        }

        /// <summary>
        /// Generating a unique html id for a dislike block
        /// </summary>
        /// <param name="model">Model with data</param>
        /// <returns>The generated HTML name of the dislike block</returns>
        public static string GetDislikeId(object model)
        {
            var name = model.GetType().Name;

            var id = model.GetType().GetProperty("Id").GetValue(model);

            return $"postDislikeId_{name}-{id}";
        }

        /// <summary>
        /// Getting user role
        /// </summary>
        /// <param name="userIdentity">User identity from the page</param>
        /// <returns>User role</returns>
        public static string GetUserRole(ClaimsIdentity userIdentity)
        {
            string userRole = userIdentity.Claims.Where(c => c.Type == ClaimTypes.Role).FirstOrDefault().Value.ToString();

            return userRole;
        }

        /// <summary>
        /// Generating a style for a like / dislike button
        /// </summary>
        /// <param name="userIdentity">User identity from the page</param>
        /// <param name="model">Model with data</param>
        /// <param name="like">Like - true, dislike - false</param>
        /// <returns>Rating button style</returns>
        public static string GetRatingStyle(ClaimsIdentity userIdentity, object model, bool like)
        {
            if (!string.IsNullOrEmpty(userIdentity.Name) && GetUserRole(userIdentity).Equals("User"))
            {
                var user = NanoWikiConnector.GetSingle<Users>("Login", userIdentity.Name);

                if (user != null)
                {
                    var userId = user.Id;

                    var tableName = model.GetType().Name;

                    var postId = (int)model.GetType().GetProperty("Id").GetValue(model);

                    return UserManager.GetUserClickRating(tableName, postId, like, userId) == true ? "color: rgb(62,166,255);" : "";
                }
            }

            return null;
        }
    }
}