﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using NanoWiki.Classes;
using NanoWiki.Managers;
using NanoWiki.Resources;
using NanoWiki.Models.Common;
using NanoWiki.Models.Database;

namespace NanoWiki.Controllers
{
    /// <summary>
    /// Controller handling work with users
    /// </summary>
    public class AccountController : Controller
    {
        #region Init data

        private readonly IWebHostEnvironment _hostingEnvironment;

        public AccountController(IWebHostEnvironment hostingEnvironment) => _hostingEnvironment = hostingEnvironment;

        public object obj;

        #endregion

        #region public IActionResults

        /// <summary>
        /// Opens the login page
        /// </summary>
        /// <returns></returns>
        public IActionResult Authorization() => View();

        /// <summary>
        /// Validation of the entered data. If everything is correct and users exist in the database, then writing its data in a cookie and authorizing it in the system
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">User password</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SignIn(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                return Json(new
                {
                    divId = "signInBlock",
                    message = Translations.AllFieldsAreRequired,
                    btnId = "signInBtn",
                    time = 3000
                });
            }

            var hash = $"{HashManager.CalculateMD5Hash(email)}‡{HashManager.CalculateMD5Hash(password)}";

            Users user = NanoWikiConnector.GetSingle<Users>("Hash", hash);
            Admins admin = NanoWikiConnector.GetSingle<Admins>("Hash", hash);

            if (user == null && admin == null)
            {
                return Json(new
                {
                    divId = "signInBlock",
                    message = Translations.IncorrectLoginOrPassword,
                    btnId = "signInBtn",
                    time = 3000
                });
            }

            if ((user != null && user.ConfirmEmail == 1) || (admin != null))
            {
                List<Claim> claims;

                if (user != null)
                {
                    claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, user.Login),
                        new Claim(ClaimTypes.Role, "User")
                    };

                    var userSettings = NanoWikiConnector.GetSingle<UserSettings>("FkUsers", user.Id);

                    if (userSettings != null && userSettings.FkLanguages > 0)
                    {
                        Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
                            CookieRequestCultureProvider.MakeCookieValue(
                                new RequestCulture(ConnectorDataManager.GetLangById(userSettings.FkLanguages))),
                                new CookieOptions { Expires = DateTime.Now.AddDays(1) }
                        );
                    }
                }
                else
                {
                    claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, admin.Login),
                        new Claim(ClaimTypes.Role, "Admin")
                    };
                }

                var userIdentity = new ClaimsIdentity(claims, "Login");

                ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                await HttpContext.SignInAsync(principal);

                return Json(new
                {
                    divId = "signInBlock",
                    message = $"{Translations.Success}. {Translations.ForwardingWillOccurIn3Seconds}",
                    btnId = "signInBtn",
                    home = true,
                    time = 3000
                });
            }
            else
            {
                return Json(new
                {
                    divId = "signInBlock",
                    message = Translations.SignInIsBlocked,
                    btnId = "signInBtn",
                    time = 3000
                });
            }
        }

        /// <summary>
        /// Validation of the entered data. If everything is correct, then adding a user to the database with the email approval status as false. Sending a message with an email verification link
        /// </summary>
        /// <param name="login">Register login</param>
        /// <param name="email">Register email</param>
        /// <param name="password">Register password</param>
        /// <param name="confirmPassword">Confirm register password</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SignUp(string login, string email, string password, string confirmPassword)
        {
            // Input Validation for Void
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(confirmPassword))
            {
                return Json(new
                {
                    divId = "signUpBlock",
                    message = Translations.AllFieldsAreRequired,
                    btnId = "signUpBtn",
                    time = 3000
                });
            }

            if (password.Length < 6)
            {
                return Json(new
                {
                    divId = "signUpBlock",
                    message = Translations.PasswordMustHaveMinimum6Characters,
                    btnId = "signUpBtn",
                    time = 3000
                });
            }

            if (password != confirmPassword)
            {
                return Json(new
                {
                    divId = "signUpBlock",
                    message = Translations.PasswordAndConfirmPasswordDontMatch,
                    btnId = "signUpBtn",
                    time = 3000
                });
            }

            EmailManager emailLogic = new EmailManager();

            if (emailLogic.IsEmailValid(email) == false)
            {
                return Json(new
                {
                    divId = "signUpBlock",
                    message = Translations.InvalidEmail,
                    btnId = "signUpBtn",
                    time = 3000
                });
            }

            // Search users in the database
            bool isNewUser = NanoWikiConnector.Any<Users>(new List<Condition>()
            {
                new Condition() { Field = "Login", Value = login },
                new Condition() { Field = "Email", Value = email }
            });
            bool isAdminExist = NanoWikiConnector.Any<Admins>(new List<Condition>()
            {
                new Condition() { Field = "Login", Value = login },
                new Condition() { Field = "Email", Value = email }
            });

            if (isAdminExist)
            {
                return Json(new
                {
                    divId = "signUpBlock",
                    message = Translations.UsernameOrEmailAlreadyExists,
                    btnId = "signUpBtn",
                    time = 3000
                });
            }

            if (isNewUser)
            {
                return Json(new
                {
                    divId = "signUpBlock",
                    message = Translations.UsernameOrEmailAlreadyExists,
                    btnId = "signUpBtn",
                    time = 3000
                });
            }

            // Creating a model with a new user
            Users newUser = new Users()
            {
                Login = login,
                Password = Encryption.Encrypt(password),
                Email = email,
                TimeLink = "",
                DateLink = "",
                ConfirmEmail = 0,
                //alt + 0135
                Hash = $"{HashManager.CalculateMD5Hash(email)}‡{HashManager.CalculateMD5Hash(password)}"
            };

            // Sending a message to the mail and subsequent registration
            string message = SendEmail(newUser, null, true);

            if (GlobalValues.SendMessage == true)
                obj = new { divId = "signUpBlock", message, btnId = "signUpBtn", time = 5000 };
            else
                obj = new { divId = "signUpBlock", message, btnId = "signUpBtn", time = 5000 };

            return Json(obj);
        }

        /// <summary>
        /// User logout
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Comfirm user email after clicking by confirm link
        /// </summary>
        /// <param name="TimeLink">Email confirmation link</param>
        /// <returns></returns>
        public IActionResult EmailConfirm(string TimeLink)
        {
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];

            long diff = 0;

            // Search our user in the general list
            Users user = NanoWikiConnector.GetSingle<Users>("TimeLink", TimeLink);

            if (user == null)
            {
                ViewBag.RedirectMessage = Translations.TheLinkIsNotActive;
                return View("~/Views/Home/RedirectPage.cshtml");
            }

            // Getting the total number of seconds from the beginning of time
            long epochTicks = new DateTime(1970, 1, 1).Ticks;
            // Get current time in seconds
            long dateNow = (DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond;

            // Getting a time value from a new user
            int userTime = int.Parse(user.DateLink);

            // Check on whether the day has passed
            diff = userTime - dateNow;
            diff += 86400;

            // If the value is less than 0, then the day allocated for the confirmation email expired
            if (diff < 0)
            {
                ViewBag.RedirectMessage = Translations.TheLinkIsNotActive;
                return View("~/Views/Home/RedirectPage.cshtml");
            }
            else
            {
                // Otherwise, the user managed to follow the link. Set the mail confirmation value to 1 
                // and remove links.
                user.DateLink = "";
                user.TimeLink = "";
                user.ConfirmEmail = 1;

                try
                {
                    // Along with the user's admission, we create his profile with basic settings.
                    NanoWikiConnector.Insert(new UserProfile()
                    {
                        FkUsers = user.Id,
                        CreateDate = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).DateFormat),
                        CreateTime = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).TimeFormat),
                        Avatar = "~/images/user/UserProfile/default.png"
                    });

                    // User update
                    NanoWikiConnector.Update(user);
                }
                catch
                {
                    ViewBag.RedirectMessage = Translations.UpsErrorMessage;
                    return View("~/Views/Home/RedirectPage.cshtml");
                }
            }

            ViewBag.RedirectMessage = Translations.VerifiedEmail;

            return View("~/Views/Home/RedirectPage.cshtml");
        }

        /// <summary>
        /// Only for accounts with type "user". Opening a personal user account. Display all settings and data on the page
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public IActionResult PersonalPage()
        {
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];
            string langId = result["langId"];

            List<Condition> conditions;

            // Getting current user
            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);

            if (user == null)
                return View("~/Views/Account/Authorization.cshtml");

            // Get current user profile
            var userProfile = NanoWikiConnector.GetSingle<UserProfile>("FkUsers", user.Id);

            // Get current user settings
            var userSettings = NanoWikiConnector.GetSingle<UserSettings>("FkUsers", user.Id);

            // If during the registration suddenly did not create an account profile
            if (userProfile == null)
            {
                var newUserprofile = new UserProfile()
                {
                    FkUsers = user.Id,
                    Avatar = "~/images/user/UserProfile/default.png",
                    Address = "",
                    City = "",
                    Country = "",
                    CreateDate = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).DateFormat),
                    CreateTime = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).TimeFormat),
                    FirstName = "",
                    LastName = "",
                    MiddleName = "",
                    Phone = "",
                    StateProvince = "",
                    ZipPostCode = 0
                };

                userProfile = newUserprofile;

                NanoWikiConnector.Insert(newUserprofile);
            }

            // If the user first entered the personal account
            if (userSettings == null)
            {
                var newUsersettings = new UserSettings()
                {
                    FkUsers = user.Id,
                    FkLanguages = int.Parse(langId),
                    FkAccountTypes = 2,
                    EventsNotifications = 0,
                    BooksNotifications = 0,
                    JournalsNotifications = 0,
                    PostsNotifications = 0,
                    ScientistsNotifications = 0
                };

                userSettings = newUsersettings;

                try
                {
                    NanoWikiConnector.Insert(newUsersettings);
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(exc.Message);
                }
            }

            try
            {
                var accountTypeId = userSettings == null ? 2 : userSettings.FkAccountTypes;

                if (lang.IndexOf("Translations") != -1)
                {
                    conditions = new List<Condition>()
                    {
                        new Condition() { Field = "FkLanguages", Value = langId },
                        new Condition() { Field = "FkAccountTypes", Value = accountTypeId }
                    };

                    var foreignUser = NanoWikiConnector.GetSingle<AccountTypesTranslations>(conditions).Name;

                    ViewBag.AccountType = foreignUser;
                }
                else
                {
                    var accountType = NanoWikiConnector.GetById<AccountTypes>(accountTypeId).Name;

                    ViewBag.AccountType = accountType;
                }
            }
            catch (Exception)
            {
                ViewBag.AccountType = Translations.User;
            }

            // Getting the avatar of the current user
            var avatarUrl = userProfile == null ? "/images/user/UserProfile/default.png" : userProfile.Avatar;

            // If the avatar file does not exist physically on the server
            if (string.IsNullOrEmpty(avatarUrl))
            {
                // Replace with standard
                userProfile.Avatar = "~/images/user/UserProfile/default.png";
                NanoWikiConnector.Update(userProfile);
            }

            // Combined model for personal account
            PersonalPageModel model = new PersonalPageModel()
            {
                Users = user,
                UserProfile = userProfile,
                UserSettings = userSettings
            };

            ViewBag.SelectLanguage = GetSelectLanguage(ConnectorDataManager.GetLangById(model.UserSettings.FkLanguages));

            ViewBag.EventsNotifications = GetOptionNotifications(model.UserSettings.EventsNotifications);
            ViewBag.BooksNotifications = GetOptionNotifications(model.UserSettings.BooksNotifications);
            ViewBag.JournalsNotifications = GetOptionNotifications(model.UserSettings.JournalsNotifications);
            ViewBag.PostsNotifications = GetOptionNotifications(model.UserSettings.PostsNotifications);
            ViewBag.ScientistsNotifications = GetOptionNotifications(model.UserSettings.ScientistsNotifications);

            return View(model);
        }

        /// <summary>
        /// Opening password recovery page
        /// </summary>
        /// <returns></returns>
        public IActionResult RecoverAccess() => View();

        /// <summary>
        /// Sending a new generated password to the user's mail
        /// </summary>
        /// <param name="emailLogin">Email or login for recover password</param>
        /// <returns></returns>
        public IActionResult SendNewPassword(string emailLogin)
        {
            if (string.IsNullOrEmpty(emailLogin))
            {
                return Json(new
                {
                    divId = "recoverPasswordBlock",
                    message = Translations.AllFieldsAreRequired,
                    btnId = "recoverPasswordBtn",
                    time = 3000
                });
            }

            var user = NanoWikiConnector.Get<Users>().Where(u => u.Email.Equals(emailLogin) || u.Login.Equals(emailLogin)).FirstOrDefault();
            var admin = NanoWikiConnector.Get<Admins>().Where(a => a.Email.Equals(emailLogin) || a.Login.Equals(emailLogin)).FirstOrDefault();

            string newPassword = StringsManager.GetRandomString(10);
            string message = "";

            if (user != null)
            {
                user.Password = Encryption.Encrypt(newPassword);
                user.Hash = $"{HashManager.CalculateMD5Hash(user.Email)}‡{HashManager.CalculateMD5Hash(newPassword)}";

                message = SendEmail(user, null, false);
            }
            else if (admin != null)
            {
                admin.Password = Encryption.Encrypt(newPassword);
                admin.Hash = $"{HashManager.CalculateMD5Hash(admin.Email)}‡{HashManager.CalculateMD5Hash(newPassword)}";

                message = SendEmail(null, admin, false);
            }
            else
            {
                return Json(new
                {
                    divId = "recoverPasswordBlock",
                    message = Translations.UserNameOrEmailNotFound,
                    btnId = "recoverPasswordBtn",
                    time = 3000
                });
            }

            if (GlobalValues.SendMessage == true)
            {
                if (user != null)
                {
                    user.ConfirmEmail = 1;
                    NanoWikiConnector.Update(user);
                }
                else
                {
                    NanoWikiConnector.Update(admin);
                }

                obj = new { divId = "recoverPasswordBlock", message, btnId = "recoverPasswordBtn", time = 3000 };
            }
            else
            {
                obj = new { divId = "recoverPasswordBlock", message, btnId = "recoverPasswordBtn", time = 3000 };
            }

            return Json(obj);
        }

        /// <summary>
        /// Only for accounts with type "user". Reset old password and install new
        /// </summary>
        /// <param name="oldPassword">Old password</param>
        /// <param name="newPassword">New password</param>
        /// <param name="confirmNewPassword">Confirm new password</param>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public IActionResult ChangePassword(string oldPassword, string newPassword, string confirmNewPassword)
        {
            string message = "";

            if (string.IsNullOrEmpty(oldPassword) || string.IsNullOrEmpty(newPassword) || string.IsNullOrEmpty(confirmNewPassword))
            {
                return Json(new
                {
                    divId = "changePasswordBlock",
                    message = Translations.AllFieldsAreRequired,
                    btnId = "changePasswordBtn",
                    time = 3000
                });
            }

            if (newPassword.Length < 6)
            {
                return Json(new
                {
                    divId = "changePasswordBlock",
                    message = Translations.PasswordMustHaveMinimum6Characters,
                    btnId = "changePasswordBtn",
                    time = 3000
                });
            }

            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);

            if (!Encryption.Decrypt(user.Password).Equals(oldPassword))
            {
                return Json(new
                {
                    divId = "changePasswordBlock",
                    message = Translations.InvalidOldPassword,
                    btnId = "changePasswordBtn",
                    time = 3000
                });
            }

            if (!newPassword.Equals(confirmNewPassword))
            {
                return Json(new
                {
                    divId = "changePasswordBlock",
                    message = Translations.PasswordAndConfirmPasswordDontMatch,
                    btnId = "changePasswordBtn",
                    time = 3000
                });
            }

            if (Encryption.Decrypt(user.Password).Equals(newPassword))
            {
                return Json(new
                {
                    divId = "changePasswordBlock",
                    message = Translations.TheNewPasswordShouldNotBeTheSameAsTheOldOne,
                    btnId = "changePasswordBtn",
                    time = 3000
                });
            }

            // Encrypt new password
            user.Password = Encryption.Encrypt(newPassword);
            // Hash generation based on new password and email
            user.Hash = $"{HashManager.CalculateMD5Hash(user.Email)}‡{HashManager.CalculateMD5Hash(newPassword)}";

            // Sending a letter to the mail with a new password
            message = SendEmail(user, null, false);

            // If the letter is sent correctly
            if (GlobalValues.SendMessage == true)
            {
                NanoWikiConnector.Update(user);
                obj = new { divId = "changePasswordBlock", message, btnId = "changePasswordBtn", time = 3000 };
            }
            else
            {
                obj = new { divId = "changePasswordBlock", message, btnId = "changePasswordBtn", time = 3000 };
            }

            return Json(obj);
        }

        /// <summary>
        /// string City, int zipPostCode, string stateProvince, string country, string phone). Only for accounts with type "user". Setting user personal data
        /// </summary>
        /// <param name="FirstName">First user name</param>
        /// <param name="LastName">Last user name</param>
        /// <param name="MiddleName">Middle user name</param>
        /// <param name="Address">User address</param>
        /// <param name="City">User city</param>
        /// <param name="zipPostCode">User zip post code</param>
        /// <param name="stateProvince">User state province</param>
        /// <param name="country">User country</param>
        /// <param name="phone">User phone</param>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        //Need add more protection
        public IActionResult SetPersonalData(string FirstName, string LastName, string MiddleName, string Address, string City, int zipPostCode, string stateProvince, string country, string phone)
        {
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];

            // Getting authorized user
            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);

            // Getting an authorized user profile
            var userProfile = NanoWikiConnector.GetSingle<UserProfile>("FkUsers", user.Id);

            // If during the registration suddenly did not create an account profile
            if (userProfile == null)
            {
                userProfile = new UserProfile()
                {
                    FkUsers = user.Id,
                    CreateDate = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).DateFormat),
                    CreateTime = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).TimeFormat)
                };

                NanoWikiConnector.Insert(userProfile);
            }

            // Setting user personal data
            userProfile.FirstName = FirstName;
            userProfile.LastName = LastName;
            userProfile.MiddleName = MiddleName;
            userProfile.Address = Address;
            userProfile.City = City;
            userProfile.ZipPostCode = zipPostCode;
            userProfile.StateProvince = stateProvince;
            userProfile.Country = country;
            userProfile.Phone = phone;

            if (NanoWikiConnector.Update(userProfile) == true)
            {
                obj = new
                {
                    divId = "personalDataBlock",
                    message = Translations.Success,
                    btnId = "personalDataBtn",
                    time = 3000
                };
            }

            else
            {
                obj = new
                {
                    divId = "personalDataBlock",
                    message = Translations.Error,
                    btnId = "personalDataBtn",
                    time = 3000
                };
            }

            return Json(obj);
        }

        /// <summary>
        /// Only for accounts with type "user". Setting the priority language of the site and notifications about the creation of new posts
        /// </summary>
        /// <param name="siteLanguage">The language in which the user wants to see the site</param>
        /// <param name="eventsNotifications">Notice of events</param>
        /// <param name="booksNotifications">Notice of books</param>
        /// <param name="journalsNotifications">Notice of journals</param>
        /// <param name="postsNotifications">Notice of posts</param>
        /// <param name="scientistsNotifications">Notice of scientists</param>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public IActionResult UserSettingsChanging(string siteLanguage, int eventsNotifications, int booksNotifications, int journalsNotifications, int postsNotifications, int scientistsNotifications)
        {
            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);

            var userSettings = NanoWikiConnector.GetSingle<UserSettings>("FkUsers", user.Id);

            userSettings ??= new UserSettings
            {
                FkUsers = user.Id,
                FkAccountTypes = 1
            };

            userSettings.FkLanguages = NanoWikiConnector.GetSingle<Languages>("Abbreviation", siteLanguage).Id;

            userSettings.EventsNotifications = eventsNotifications;
            userSettings.BooksNotifications = booksNotifications;
            userSettings.JournalsNotifications = journalsNotifications;
            userSettings.PostsNotifications = postsNotifications;
            userSettings.ScientistsNotifications = scientistsNotifications;

            ViewBag.SelectLanguage = GetSelectLanguage(siteLanguage);

            ViewBag.EventsNotifications = GetOptionNotifications(eventsNotifications);
            ViewBag.BooksNotifications = GetOptionNotifications(booksNotifications);
            ViewBag.JournalsNotifications = GetOptionNotifications(journalsNotifications);
            ViewBag.PostsNotifications = GetOptionNotifications(postsNotifications);
            ViewBag.ScientistsNotifications = GetOptionNotifications(scientistsNotifications);

            if (userSettings.Id <= 0)
            {
                if (NanoWikiConnector.Insert(userSettings) == true)
                {
                    return Json(new
                    {
                        divId = "userSettingsChangingBlock",
                        message = Translations.Success,
                        btnId = "userSettingsChangingBtn",
                        time = 3000
                    });
                }
                else
                {
                    return Json(new
                    {
                        divId = "userSettingsChangingBlock",
                        message = Translations.Error,
                        btnId = "userSettingsChangingBtn",
                        time = 3000
                    });
                }
            }
            else
            {
                if (NanoWikiConnector.Update(userSettings) == true)
                {
                    return Json(new
                    {
                        divId = "userSettingsChangingBlock",
                        message = Translations.Success,
                        btnId = "userSettingsChangingBtn",
                        time = 3000
                    });
                }
                else
                {
                    return Json(new
                    {
                        divId = "userSettingsChangingBlock",
                        message = Translations.Error,
                        btnId = "userSettingsChangingBtn",
                        time = 3000
                    });
                }
            }
        }

        // Need more settings and protections
        /// <summary>
        /// Only for accounts with type "user". Completely remove a user account from the system
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "User")]
        public async Task<IActionResult> DeleteAccount()
        {
            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);

            NanoWikiConnector.Delete(user);

            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Only for accounts with type "user". Change user avatar: install new, delete old
        /// </summary>
        /// <param name="fileImg">Object with file</param>
        /// <param name="status">Delete or add file</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "User")]
        public IActionResult ChangeAvatar(IFormFile fileImg, string status)
        {
            if (string.IsNullOrEmpty(status))
                status = "Update";

            // Getting current user
            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);

            // Get current user profile
            var userProfile = NanoWikiConnector.GetSingle<UserProfile>("FkUsers", user.Id);

            if (user == null || userProfile == null)
                return RedirectToAction("PersonalPage", "Account");

            // If the user deletes his avatar
            if (status.Equals("Delete"))
            {
                userProfile.Avatar = "~/images/user/UserProfile/default.png";
            }
            // If the user changes his avatar
            else
            {
                // Check that the file was sent to the server
                if (fileImg != null)
                {
                    try
                    {
                        userProfile.Avatar = FileConvertingManager.ConvertImageToBase64(fileImg);
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("PersonalPage", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("PersonalPage", "Account");
                }
            }

            NanoWikiConnector.Update(userProfile);

            if (status.Equals("Delete"))
                return Json(true);
            else
                return RedirectToAction("PersonalPage", "Account");
        }

        /// <summary>
        /// Generating a letter to send it to the mail in the following cases: registration, password recovery, password change
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="admin">Admin object</param>
        /// <param name="ConfirmEmail">Password confirmed status</param>
        /// <returns></returns>
        public string SendEmail(Users user, Admins admin, bool ConfirmEmail)
        {
            string result = "";
            EmailManager sendMessage = new EmailManager();

            // Getting total time in seconds
            long epochTicks = new DateTime(1970, 1, 1).Ticks;
            // Getting current time
            long unixTime = ((DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond);

            Guid guid;
            // Link generation
            guid = Guid.NewGuid();

            if (user != null)
            {
                user.DateLink = unixTime.ToString();
                user.TimeLink = guid.ToString();
            }
            else if (admin != null)
            {
                admin.DateLink = unixTime.ToString();
                admin.TimeLink = guid.ToString();
            }

            string confirmUrl;
            if (ConfirmEmail == true)
            {
                confirmUrl = $"{Translations.NanoWikiDescription}. {Translations.SignUp}." +
                    $"<br/><a href=\"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.Value}" +
                    $"/Account/EmailConfirm?TimeLink={user.TimeLink}\">" +
                    $"{Translations.GoTo}</a><br/>©{DateTime.Now.Year}";

                if (NanoWikiConnector.Insert(user) == false)
                    return Translations.UpsErrorMessage;
                else
                    result = sendMessage.SendTimeLink(null, user, confirmUrl, true);
            }
            else
            {
                string newPassword;
                if (user != null)
                    newPassword = Encryption.Decrypt(user.Password);
                else
                    newPassword = Encryption.Decrypt(admin.Password);

                confirmUrl = $"{Translations.NanoWikiDescription}. {Translations.RecoverAccess}." +
                    $"<br/><br/>{Translations.WarningMessageNewPassword}<hr/><br/><br/>" +
                    $"{Translations.NewPassword}: " +
                    $"{newPassword}<br/><br/><br/>©{DateTime.Now.Year}";

                if (user != null)
                {
                    if (NanoWikiConnector.Update(user) == false)
                        return Translations.UpsErrorMessage;
                    else
                        result = sendMessage.SendTimeLink(null, user, confirmUrl, false);
                }
                else if (admin != null)
                {
                    if (NanoWikiConnector.Update(admin) == false)
                        return Translations.UpsErrorMessage;
                    else
                        result = sendMessage.SendTimeLink(admin, null, confirmUrl, false);
                }
            }

            return result;
        }

        #endregion

        #region private methods

        /// <summary>
        /// Get html code by language
        /// </summary>
        /// <param name="siteLanguage">Language name</param>
        /// <returns>HTML code with languages</returns>
        private string GetSelectLanguage(string siteLanguage)
        {
            return $"<option { (siteLanguage.Equals("en") ? "selected" : null) } value = \"en\">EN</option>" +
                $"<option { (siteLanguage.Equals("ru") ? "selected" : null) } value = \"ru\">RU</option>" +
                $"<option { (siteLanguage.Equals("zh") ? "selected" : null) } value = \"zh\">ZH</option>" +
                $"<option { (siteLanguage.Equals("ja") ? "selected" : null) } value = \"ja\">JA</option>";
        }

        /// <summary>
        /// Get html code by notification value
        /// </summary>
        /// <param name="notification">Notification status</param>
        /// <returns>HTML code with notifications</returns>
        private string GetOptionNotifications(int notification)
        {
            return $"<option { (notification == 0 ? "selected" : null) } value=\"0\">{Translations.DontNotify}</option><option { (notification == 1 ? "selected" : null) } value = \"1\" >{Translations.Notify}</option>";
        }

        #endregion

        #region Cookie

        /// <summary>
        /// Getting language from cookies
        /// </summary>
        /// <returns>Key-value format dictionary</returns>
        private Dictionary<string, string> GetLangFromCookie()
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            if (Request.Cookies.ContainsKey(CookieRequestCultureProvider.DefaultCookieName))
            {
                var langName = Request.Cookies[CookieRequestCultureProvider.DefaultCookieName];
                langName = langName.Substring(2, 2);
                keyValuePairs.Add("langName", langName);

                var id = Request.Cookies["currentLanguageId"];

                if (string.IsNullOrEmpty(id))
                    id = "1";

                keyValuePairs.Add("langId", id);
            }
            else
            {
                keyValuePairs.Add("langName", GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"));
                keyValuePairs.Add("langId", GlobalValues.GetAppSettingsString("ProjectLanguage:Id"));
            }

            return keyValuePairs;
        }

        #endregion
    }
}