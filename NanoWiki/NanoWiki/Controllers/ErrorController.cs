﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using NanoWiki.Models.Common;

namespace NanoWiki.Controllers
{
    /// <summary>
    /// Error handling controller
    /// </summary>
    public class ErrorController : Controller
    {
        ErrorModel errorModel;

        /// <summary>
        /// Error code processing and display of this error to the user on the screen
        /// </summary>
        /// <param name="statusCode">Error status code</param>
        /// <returns></returns>
        public IActionResult ErrorStatus(int? statusCode = null)
        {
            if (statusCode.HasValue)
            {
                errorModel = statusCode switch
                {
                    204 => new ErrorModel
                    {
                        Code = "204",
                        MessageToUser = Resources.Translations.NoContent
                    },
                    400 => new ErrorModel
                    {
                        Code = "400",
                        MessageToUser = Resources.Translations.BadRequest
                    },
                    404 => new ErrorModel
                    {
                        Code = "404",
                        MessageToUser = Resources.Translations.NotFound
                    },
                    500 => new ErrorModel
                    {
                        Code = "500",
                        MessageToUser = Resources.Translations.InternalServerError
                    },
                    501 => new ErrorModel
                    {
                        Code = "501",
                        MessageToUser = Resources.Translations.NotImplemented
                    },
                    _ => new ErrorModel
                    {
                        Code = "500",
                        MessageToUser = Resources.Translations.InternalServerError
                    },
                };
            }
            else
            {
                errorModel = new ErrorModel
                {
                    Code = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    MessageToUser = Resources.Translations.UpsErrorMessage
                };
            }

            return View("~/Views/Error/Error.cshtml", errorModel);
        }
    }
}