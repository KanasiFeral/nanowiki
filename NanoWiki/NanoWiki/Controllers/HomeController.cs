﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using X.PagedList;
using Newtonsoft.Json;
using NanoWiki.Classes;
using NanoWiki.Managers;
using NanoWiki.Resources;
using NanoWiki.Models.Database;
using NanoWiki.Models.Common;

namespace NanoWiki.Controllers
{
    /// <summary>
    /// Controller is responsible for working with the user part of the site
    /// </summary>
    [AllowAnonymous]
    public class HomeController : Controller
    {
        #region Init data

        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IModelAttributesManager _modelAttributesManager;
        private readonly IResourcesManager _resourcesManager;

        private object _obj;
        private int _pageIndex = 1;
        private readonly int _pageSize = 7;

        public struct ModelData
        {
            public string Field { get; set; }

            public string Value { get; set; }
        }

        public HomeController(IWebHostEnvironment hostingEnvironment, IModelAttributesManager modelAttributesManager, IResourcesManager resourcesManager)
        {
            _hostingEnvironment = hostingEnvironment;
            _modelAttributesManager = modelAttributesManager;
            _resourcesManager = resourcesManager;
        }

        #endregion

        #region Public IActionResult

        /// <summary>
        /// Opening the home page
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var currentTable = "";

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];
            string langId = result["langId"];

            var condition = new List<Condition>
            {
                new Condition()
                {
                    Field = "PostStatus",
                    Value = true
                }
            };

            if (langName.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) != -1)
            {
                currentTable = currentTable.Replace("Translations", "");
            }
            else
            {
                currentTable += "Translations";
                condition.Add(new Condition() { Field = "FkLanguages", Value = langId });
            }

            var news = NanoWikiConnector.Get($"Posts{currentTable}", condition).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x));

            if (news.Count() > 0)
            {
                // News
                ViewBag.News = news.ToPagedList(_pageIndex, _pageSize);
                ViewBag.NewsName = "Posts" + currentTable;
            }

            var books = NanoWikiConnector.Get($"Books{currentTable}", condition).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x));

            if (books.Count() > 0)
            {
                //Books
                ViewBag.Books = books.ToPagedList(_pageIndex, _pageSize);
                ViewBag.BooksName = "Books" + currentTable;
            }

            var journals = NanoWikiConnector.Get($"Journals{currentTable}", condition).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x));

            if (journals.Count() > 0)
            {
                //Journals
                ViewBag.Journals = journals.ToPagedList(_pageIndex, _pageSize);
                ViewBag.JournalsName = "Journals" + currentTable;
            }

            var scientists = NanoWikiConnector.Get($"Scientists{currentTable}", condition).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x));

            if (scientists.Count() > 0)
            {
                //Scientists
                ViewBag.Scientists = scientists.ToPagedList(_pageIndex, _pageSize);
                ViewBag.ScientistsName = "Scientists" + currentTable;
            }

            //var events = Connector.Get("Events" + CurrentTable, Condition);
            var events = NanoWikiConnector.Get($"Events{currentTable}", condition).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x));

            if (events.Count() > 0)
            {
                //Events
                ViewBag.Events = events.ToPagedList(_pageIndex, _pageSize);
                ViewBag.EventsName = "Events" + currentTable;
            }

            //Most popular
            try
            {
                ViewBag.LastBook = NanoWikiConnector.GetSingle($"Books{currentTable}", GetMostPopular($"Books{currentTable}"));
            }
            catch (Exception)
            {
                ViewBag.LastBook = null;
            }

            try
            {
                ViewBag.LastScientist = NanoWikiConnector.GetSingle($"Scientists{currentTable}", GetMostPopular($"Scientists{currentTable}"));
            }
            catch (Exception)
            {
                ViewBag.LastScientist = null;
            }

            try
            {
                ViewBag.LastEvent = NanoWikiConnector.GetSingle($"Events{currentTable}", GetMostPopular($"Events{currentTable}"));
            }
            catch (Exception)
            {
                ViewBag.LastEvent = null;
            }

            return View();
        }

        /// <summary>
        /// Opening a page with project information and feedback
        /// </summary>
        /// <returns></returns>
        public IActionResult About()
        {
            Dictionary<string, string> result = GetLangFromCookie();
            string langId = result["langId"];

            return View("~/Views/Home/About.cshtml", NanoWikiConnector.GetSingle<AboutProject>("FkLanguages", langId));
        }

        /// <summary>
        /// The redirection method on the error page
        /// </summary>
        /// <param name="message">Error message</param>
        /// <returns></returns>
        public IActionResult Error(string message) => View("~/Views/Error/ErrorUser.cshtml", message);

        /// <summary>
        /// Sending a message to the administration post, feedback to the administration
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="subject">Email title</param>
        /// <param name="email">Email sender</param>
        /// <param name="message">Email message</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SendFeedback(string name, string subject, string email, string message)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(subject) ||
                string.IsNullOrEmpty(email) || string.IsNullOrEmpty(message))
            {
                return Json(new
                {
                    divId = "feedbackBlock",
                    message = Translations.AllFieldsAreRequired,
                    btnId = "sendFeedbackBtn",
                    time = 5000
                });
            }

            EmailManager emailLogic = new EmailManager();

            if (emailLogic.IsEmailValid(email) == false)
            {
                return Json(new
                {
                    divId = "feedbackBlock",
                    message = Translations.InvalidEmail,
                    btnId = "sendFeedbackBtn",
                    time = 5000
                });
            }

            MessageModel messageModel = new MessageModel
            {
                EmailTo = email,
                Message = $"{Translations.NanoWiki}.<br/>{email}.<br/>{name}.<br/>{subject}." +
                    $"<br/>{message}<br/><br/>{DateTime.Now.ToShortDateString()}",
                Title = Translations.Feedback
            };

            // true - to My email(internationalnanoplatfrom@gmail.com) | false - to anothers emails
            message = emailLogic.SendEmail(messageModel, true);

            if (GlobalValues.SendMessage == true)
                return Json(new { divId = "feedbackBlock", message, btnId = "sendFeedbackBtn", time = 5000 });
            else
                return Json(new { divId = "feedbackBlock", message, btnId = "sendFeedbackBtn", time = 5000 });
        }

        /// <summary>
        /// Opening a page with all Posts
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="searchValue">Value for searhing</param>
        /// <returns></returns>
        public IActionResult Posts(string table, string searchValue)
        {
            // CSS names of the icons that shows what type of post is currently displayed
            var icon = "fa fa-";
            var typePost = "post-cata cata-sm cata-";

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];

            // Get current table based on current language
            table = CurrentTable(table, searchValue, langName);

            // Check table value
            if (string.IsNullOrEmpty(table) && string.IsNullOrEmpty(searchValue))
                return NoPosts(false);

            if (table.IndexOf("Scientists") != -1)
                ViewBag.Initials = true;

            if (table.IndexOf("Scientists") != -1)
                ViewBag.Initials = true;

            var icons = StringsManager.GetTypePost(table);
            icon = icons["icon"];
            typePost = icons["typePost"];

            if (table.IndexOf("Journals") != -1)
            {
                var filterTable = "";

                if (langName.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) == -1)
                {
                    filterTable += "Translations";
                }

                // Generate languages select
                //List<Languages> languages = Connector.Get<Languages>();
                var languages = NanoWikiConnector.Get<Languages>();

                if (languages != null)
                {
                    string selectLanguages = "<select name='JournalLanguage' class='form-control' style='min-height: 48px;'><option value=''></option>";
                    foreach (var item in languages)
                    {
                        selectLanguages += $"<option value='{item.Abbreviation}'>{item.Abbreviation}</option>";
                    }
                    selectLanguages += "</select>";
                    ViewBag.SelectLanguages = selectLanguages;
                }

                // Generate countries select
                var countries = NanoWikiConnector.Get($"Countries{filterTable}");

                if (countries != null)
                {
                    string selectCountries = $"<select name='FkCountries{filterTable}' class='form-control' " +
                        $"style='min-height: 48px;'><option value=''></option>";
                    foreach (var item in countries)
                    {
                        selectCountries += $"<option value='" +
                            $"{item.GetType().GetProperty("Id").GetValue(item, null)}'>" +
                            $"{item.GetType().GetProperty("Name").GetValue(item, null)}</option>";
                    }
                    selectCountries += "</select>";
                    ViewBag.SelectCountries = selectCountries;
                }

                // Generate topics select
                var topics = NanoWikiConnector.Get($"Topics{filterTable}");

                if (topics != null)
                {
                    string selectTopics = $"<select name='FkTopics{filterTable}' class='form-control' " +
                        $"style='min-height: 48px;'><option value=''></option>";
                    foreach (var item in topics)
                    {
                        selectTopics += $"<option value='" +
                            $"{item.GetType().GetProperty("Id").GetValue(item, null)}'>" +
                            $"{item.GetType().GetProperty("Name").GetValue(item, null)}</option>";
                    }
                    selectTopics += "</select>";
                    ViewBag.SelectTopics = selectTopics;
                }

                //Generate scopus select
                var selectScopus = "<select name='SCOPUS' class='form-control' style='min-height: 48px;'><option value=''></option>";
                selectScopus += string.Format("<option value='{0}'>{0}</option>", Translations.IndexedToScopus);
                selectScopus += string.Format("<option value='{0}'>{0}</option>", Translations.IndexedTranslationVersion);
                selectScopus += string.Format("<option value='{0}'>{0}</option>", Translations.IndexedPartially);
                selectScopus += string.Format("<option value='{0}'>{0}</option>", Translations.NotIndexedInScopus);
                selectScopus += "</select>";
                ViewBag.SelectScopus = selectScopus;

                //Generate rsci select
                string selectRSCI = "<select name='ImpactFactorRSCI' class='form-control' style='min-height: 48px;'><option value=''></option>";
                selectRSCI += string.Format("<option value='{0}'>{0}</option>", Translations.IndexedInRSCI);
                selectRSCI += string.Format("<option value='{0}'>{0}</option>", Translations.NotIndexedInRSCI);
                selectRSCI += "</select>";
                ViewBag.SelectRSCI = selectRSCI;

                //Generate status select
                string selectComingOut = "<select name='CurrentlyStatus' class='form-control' style='min-height: 48px;'><option value=''></option>";
                selectComingOut += string.Format("<option value='{0}'>{0}</option>", Translations.ComingOutNow);
                selectComingOut += string.Format("<option value='{0}'>{0}</option>", Translations.NotComingOutNow);
                selectComingOut += "</select>";
                ViewBag.SelectComingOut = selectComingOut;

                //Generate status select
                string selectRenameTransferInformation = "<select name='RenameTransferInformation' class='form-control' style='min-height: 48px;'><option value=''></option>";
                selectRenameTransferInformation += string.Format("<option value='{0}'>{0}</option>", Translations.FullyTranslated);
                selectRenameTransferInformation += string.Format("<option value='{0}'>{0}</option>", Translations.IndividualTranslated);
                selectRenameTransferInformation += string.Format("<option value='{0}'>{0}</option>", Translations.DoesNotTranslate);
                selectRenameTransferInformation += "</select>";
                ViewBag.SelectRenameTransferInformation = selectRenameTransferInformation;
            }

            ViewBag.Icon = icon;
            ViewBag.PostsName = _resourcesManager.GetResourceTitle<Translations>(table);
            ViewBag.TypePost = typePost;
            ViewBag.Lang = langName;
            ViewBag.Table = table;
            ViewBag.SearchValue = searchValue;
            ViewBag.HeaderName = string.IsNullOrEmpty(searchValue) ?
                _resourcesManager.GetResourceTitle<Translations>(table) :
                $"{_resourcesManager.GetResourceTitle<Translations>(table)} " +
                $"({Translations.SearchResult}: {searchValue})";

            // Conclusion 4 recommended Posts on the right side of the page
            if (!string.IsNullOrEmpty(table) && (string.IsNullOrEmpty(searchValue)))
            {
                try
                {
                    ViewBag.Recomended = null;
                    ViewBag.Recomended = NanoWikiConnector.Get(table, GetPostConditionByLang(searchValue)).Take(4).ToList();
                }
                catch (Exception exc)
                {
                    var error = exc.Message;
                    ViewBag.Recomended = null;
                }
            }

            return View("~/Views/Home/Posts.cshtml");
        }

        /// <summary>
        /// Uploading a partial view. Loading records of a specific table. Works in conjunction with the "Posts" method
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="searchValue">Value for searhing</param>
        /// <param name="page">Page number</param>
        /// <param name="typePost">Type post based on table</param>
        /// <param name="filter">Filter call on page</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Records(string table, string searchValue, int? page, string typePost, bool filter)
        {
            // Index of currently open page
            _pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            _obj = new List<object>();

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];

            // Get current table based on current language
            table = CurrentTable(table, searchValue, langName);

            // Check table value
            if (string.IsNullOrEmpty(table) && string.IsNullOrEmpty(searchValue))
                return NoPosts(false);

            if (table.IndexOf("Scientists") != -1)
                ViewBag.Initials = true;

            int totalCount;
            try
            {
                // If the user opens the page through the menu, without searching and user doesn't use filter panel
                if (string.IsNullOrEmpty(searchValue))
                {
                    _obj = NanoWikiConnector.Get(table, GetPostConditionByLang(null)).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToPagedList(_pageIndex, _pageSize);
                    totalCount = NanoWikiConnector.Count(table, GetPostConditionByLang(null));
                }
                else if (!string.IsNullOrEmpty(searchValue) & filter == true)
                {
                    _obj = NanoWikiConnector.Get(table, searchValue).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToPagedList(_pageIndex, _pageSize);
                    totalCount = NanoWikiConnector.Count(table, searchValue);
                }
                // Displays all Posts matching the search criteria.
                else
                {
                    List<object> list = new List<object>();

                    if (langName == GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
                    {
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("Books", GenerateLikeCondition("Books", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("Journals", GenerateLikeCondition("Journals", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("Scientists", GenerateLikeCondition("Scientists", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("Posts", GenerateLikeCondition("Posts", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("Events", GenerateLikeCondition("Events", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                    }
                    else
                    {
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("BooksTranslations", GenerateLikeCondition("BooksTranslations", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("JournalsTranslations", GenerateLikeCondition("JournalsTranslations", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("ScientistsTranslations", GenerateLikeCondition("ScientistsTranslations", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("PostsTranslations", GenerateLikeCondition("PostsTranslations", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                        list.AddRange(SetSearchDataToList(NanoWikiConnector.Get("EventsTranslations", GenerateLikeCondition("EventsTranslations", searchValue))
                            .OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToList()));
                    }

                    _obj = list.ToPagedList(_pageIndex, _pageSize);
                    totalCount = list.Count;
                }
            }
            catch (Exception)
            {
                return NoPosts(true);
            }

            if (_obj == null || totalCount <= 0)
                return NoPosts(true);

            if (!string.IsNullOrEmpty(searchValue))
                ViewBag.SearchList = true;
            else
                ViewBag.SearchList = false;

            // If user used filter panel need to just load filter records
            if (filter)
                ViewBag.SearchList = false;

            ViewBag.TotalCount = totalCount;
            ViewBag.Items = _obj;
            ViewBag.TypePost = typePost;
            ViewBag.Filter = filter;
            ViewBag.Table = table;
            ViewBag.SearchValue = filter ? searchValue : "";
            ViewBag.HeaderName = filter ? _resourcesManager.GetResourceTitle<Translations>(table) :
                string.IsNullOrEmpty(searchValue) ?
                    _resourcesManager.GetResourceTitle<Translations>(table) :
                $"{_resourcesManager.GetResourceTitle<Translations>(table)} " +
                    $"({Translations.SearchResult}: {searchValue})";

            return PartialView("~/Views/Home/PartialViews/Records.cshtml");
        }

        /// <summary>
        /// Opening the page of the selected post
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Table id</param>
        /// <param name="fkId">Id from translation table</param>
        /// <param name="typePost">Type post based on table</param>
        /// <returns></returns>
        public IActionResult SinglePost(string table, int? id, int? fkId, string typePost)
        {
            int visitCount = 0;

            // Getting own record id value
            int ownRecordId = (int)(fkId.HasValue && fkId != 0 ? fkId : id);

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];
            string langId = result["langId"];

            // Get current table based on current language
            table = CurrentTable(table, null, langName);

            if (string.IsNullOrEmpty(table))
                return NoPosts(false);

            if (table.IndexOf("Scientists") != -1)
                ViewBag.Initials = true;

            // Getting the current post
            try
            {
                if (!fkId.HasValue && table.IndexOf("Translations") != -1)
                {
                    var baseTable = table.Replace("Translations", "");
                    //_obj = NanoWikiConnector.GetSingle(table, $"Fk{baseTable}", (int)(id.HasValue ? id : 0));
                    _obj = NanoWikiConnector.GetSingle(table, new List<Condition>()
                    {
                        new Condition() { Field = $"Fk{baseTable}", Value = (int)(id.HasValue ? id : 0) },
                        new Condition() { Field = "FkLanguages", Value = langId }
                    });
                }
                else
                {
                    _obj = NanoWikiConnector.GetById(table, (int)(id.HasValue ? id : 0));
                }
            }
            catch (Exception)
            {
                return NoPosts(false);
            }

            if (_obj == null)
                return NoPosts(false);

            List<ModelData> modelDataList = new List<ModelData>();

            foreach (var item in _obj.GetType().GetProperties())
            {
                if (_modelAttributesManager.GetForeignKeyAttributeValue(item.Name, _obj))
                {
                    continue;
                }

                if (item.Name.Equals("Text") || item.Name.Equals("Initials") ||
                    item.Name.Equals("PostDate") || item.Name.Equals("Name") ||
                    item.Name.Equals("Photo") || item.Name.Equals("Id") ||
                    item.Name.Equals("Autobiography") || item.Name.Equals("PostStatus") ||
                    item.Name.Equals("FkAdmins") || item.Name.Equals("FkLanguages") ||
                    item.Name.Equals("LikeCount") || item.Name.Equals("DislikeCount") ||
                    item.Name.Equals("VisitCount"))
                {
                    continue;
                }
                else if (item.Name.IndexOf("Fk") != -1)
                {
                    if (item.Name.IndexOf(table.Replace("Translations", "")) != -1)
                        continue;

                    string title = "";
                    if (item.Name.IndexOf("Topic") != -1)
                        title = Translations.Topic;
                    else if (item.Name.IndexOf("Countr") != -1)
                        title = Translations.Country;
                    else if (item.Name.IndexOf("Cit") != -1)
                        title = Translations.City;
                    else if (item.Name.IndexOf("Author") != -1)
                        title = Translations.Authors;

                    modelDataList.Add(new ModelData
                    {
                        Field = title,
                        Value = ConnectorDataManager.GetFkValue(item.Name,
                        (int)_obj.GetType().GetProperty(item.Name).GetValue(_obj, null))
                    });
                }
                else if (item.Name.Equals("Shops") || item.Name.Equals("SourceReference") || item.Name.Equals("SocialNetwork"))
                {
                    modelDataList.Add(GetLinks(table, (int)_obj.GetType().GetProperty("Id").GetValue(_obj), item.Name));
                }
                else
                {
                    var key = _resourcesManager.GetResourceTitle<Translations>(item.Name);
                    var value = item.GetValue(_obj).ToString();

                    modelDataList.Add(new ModelData
                    {
                        Field = key,
                        Value = value
                    });
                }
            }

            // Getting data from the main table. Data that is not in the translation tables
            if (langName != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
            {
                switch (table)
                {
                    #region PostsTranslations

                    case "PostsTranslations":

                        Posts post = NanoWikiConnector.GetById<Posts>(ownRecordId);

                        // No data, table is empty
                        if (post == null)
                        {
                            return NoPosts(false);
                        }

                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.Year,
                            Value = post.Tags.ToString()
                        });

                        break;

                    #endregion

                    #region BooksTranslations

                    case "BooksTranslations":

                        Books book = NanoWikiConnector.GetById<Books>(ownRecordId);

                        // No data, table is empty
                        if (book == null)
                        {
                            return NoPosts(false);
                        }

                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.Year,
                            Value = book.Year.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.PageCount,
                            Value = book.PageCount.ToString()
                        });

                        break;

                    #endregion

                    #region JournalsTranslations

                    case "JournalsTranslations":

                        Journals journal = NanoWikiConnector.GetById<Journals>(ownRecordId);

                        // No data, table is empty
                        if (journal == null)
                        {
                            return NoPosts(false);
                        }

                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.Year,
                            Value = journal.Year.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.IssuesPerYear,
                            Value = journal.IssuesPerYear.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.IssueArticles,
                            Value = journal.IssueArticles.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.SubscriptionIndex,
                            Value = journal.SubscriptionIndex.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.TotalArticles,
                            Value = journal.TotalArticles.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.TotalIssues,
                            Value = journal.TotalIssues.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.FullTexts,
                            Value = journal.FullTexts.ToString()
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.Quotes,
                            Value = journal.Quotes.ToString()
                        });

                        break;

                    #endregion

                    #region ScientistsTranslations

                    case "ScientistsTranslations":

                        Scientists scientist = NanoWikiConnector.GetById<Scientists>(ownRecordId);

                        // No data, table is empty
                        if (scientist == null)
                        {
                            return NoPosts(false);
                        }

                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.Phone,
                            Value = scientist.Phone
                        });
                        modelDataList.Add(new ModelData
                        {
                            Field = Translations.Email,
                            Value = scientist.Email
                        });

                        modelDataList.Add(GetLinks("Scientists", scientist.Id, "SocialNetwork"));

                        break;

                    #endregion

                    default:
                        break;
                }
            }

            int fkTranslationsId = 0;

            if (table.IndexOf("Books") != -1 && table.IndexOf("Translations") != -1)
            {
                fkTranslationsId = (int)_obj.GetType().GetProperty("FkBooks").GetValue(_obj, null);
            }
            else if (table.IndexOf("Journals") != -1 && table.IndexOf("Translations") != -1)
            {
                fkTranslationsId = (int)_obj.GetType().GetProperty("FkJournals").GetValue(_obj, null);
            }
            else if (table.IndexOf("Scientists") != -1 && table.IndexOf("Translations") != -1)
            {
                fkTranslationsId = (int)_obj.GetType().GetProperty("FkScientists").GetValue(_obj, null);
            }
            else if (table.IndexOf("Posts") != -1 && table.IndexOf("Translations") != -1)
            {
                fkTranslationsId = (int)_obj.GetType().GetProperty("FkPosts").GetValue(_obj, null);
            }
            else if (table.IndexOf("Events") != -1 && table.IndexOf("Translations") != -1)
            {
                fkTranslationsId = (int)_obj.GetType().GetProperty("FkEvents").GetValue(_obj, null);
            }

            string postText = "";
            string postName = "";

            if (ViewBag.Initials == true)
                postText = _obj.GetType().GetProperty("Autobiography").GetValue(_obj, null).ToString();
            else
                postText = _obj.GetType().GetProperty("Text").GetValue(_obj, null).ToString();

            if (ViewBag.Initials == true)
                postName = _obj.GetType().GetProperty("Initials").GetValue(_obj, null).ToString();
            else
                _obj.GetType().GetProperty("Name").GetValue(_obj, null).ToString();

            // Getting a list of all table Posts for selecting two: the previous and next Posts
            List<object> list = new List<object>();

            try
            {
                if (langName != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
                {
                    list = NanoWikiConnector.Get(table, new List<Condition>()
                    {
                        new Condition() { Field = "PostStatus", Value = true },
                        new Condition() { Field = "FkLanguages", Value = langId }
                    });
                }
                else
                {
                    list = NanoWikiConnector.Get(table, new List<Condition>()
                    {
                        new Condition() { Field = "PostStatus", Value = true }
                    });
                }
            }
            catch (Exception)
            {
                list = new List<object>();
            }

            if (list.Count != 0)
            {
                int currIndex = -1;

                try
                {
                    if (langName != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
                    {
                        var baseTable = table.Replace("Translations", "");
                        currIndex = list.ToList().Select((a, i) => new { element = a, index = i }).First(a => int.Parse(a.element.GetType().GetProperty($"Fk{baseTable}").GetValue(a.element).ToString()) == ownRecordId).index;
                    }
                    else
                    {
                        currIndex = list.ToList().Select((a, i) => new { element = a, index = i }).First(a => int.Parse(a.element.GetType().GetProperty("Id").GetValue(a.element).ToString()) == id.Value).index;
                    }

                    // Indexes of the previous and next Posts
                    var prevIndex = (currIndex > 0 ? currIndex : list.Count) - 1;
                    var nextIndex = (currIndex < list.Count - 1 ? currIndex : -1) + 1;

                    // Posts themselves
                    ViewBag.PrevPost = list[prevIndex];
                    ViewBag.NextPost = list[nextIndex];
                }
                catch (Exception)
                {
                    ViewBag.PrevPost = null;
                    ViewBag.NextPost = null;
                }
            }

            try
            {
                ViewBag.User =
                    NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name) != null |
                    NanoWikiConnector.GetSingle<Admins>("Login", HttpContext.User.Identity.Name) != null;
            }
            catch (Exception)
            {
                ViewBag.User = null;
            }

            // Get visitor Id
            var visitorId = Request.Cookies["VisitorId"];

            // Try to get current visitor
            var currentVisitor = NanoWikiConnector.GetSingle<VisitorsInfo>("VisitorId", visitorId);

            // If current visitor don't opened this page
            if (currentVisitor == null)
            {
                int.TryParse(NanoWikiConnector.MaxId("VisitorsInfo"), out int maxId);

                maxId++;

                // Add current user to list of all visitors
                NanoWikiConnector.Insert(new VisitorsInfo()
                {
                    Id = maxId,
                    VisitorId = visitorId,
                    TableId = id.Value,
                    TableName = table,
                    VisitDate = DateTime.Now.Date.ToShortDateString()
                });

                // Get all visit counts for current table
                visitCount = (int)_obj.GetType().GetProperty("VisitCount").GetValue(_obj);

                // Increase by one
                visitCount++;

                // Set new visit count value
                _obj.GetType().GetProperty("VisitCount").SetValue(_obj, visitCount);

                // Update database
                NanoWikiConnector.Update(_obj);
            }

            ViewBag.Model = _obj;
            ViewBag.FkId = fkId;
            ViewBag.TypePost = typePost;
            ViewBag.PostText = postText;
            ViewBag.PostName = postName;
            ViewBag.ModelDataList = modelDataList;
            ViewBag.Lang = langName;
            ViewBag.Header = _resourcesManager.GetResourceTitle<Translations>(table);
            ViewBag.Table = table;
            ViewBag.TableComments = table + "Comments";
            ViewBag.VisitCount = visitCount;

            return View("~/Views/Home/SinglePost.cshtml");
        }

        /// <summary>
        /// The method of work with the rating of Posts. Increase and decrease the rating of the selected post
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Table id</param>
        /// <param name="ratingType">Like or dislike</param>
        /// <param name="ratingTagId">Like HTML div id or dislike HTML div id</param>
        /// <param name="blockId">HTML id with rating data</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult RatingUpdate(string table, int? id, string ratingType, string ratingTagId, string blockId)
        {
            if (string.IsNullOrEmpty(HttpContext.User.Identity.Name))
                return Json(new { auth = false, message = Translations.AuthorizationRequired, blockId });

            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);
            var admin = NanoWikiConnector.GetSingle<Admins>("Login", HttpContext.User.Identity.Name);

            if (admin != null)
            {
                return Json(new
                {
                    auth = false,
                    message = Translations.AdministratorsAreNotAllowedToInfluenceRating,
                    blockId
                });
            }

            if (user == null)
                return Json(new { auth = false, message = Translations.UnknownUser, blockId });

            int ratingCount = 0;
            bool likePressed = false;
            bool dislikePressed = false;

            if (string.IsNullOrEmpty(table) || id.HasValue || id != 0)
            {
                bool RatingPut = false;

                var parentTable = NanoWikiConnector.GetById(table, (int)(id.HasValue ? id : 0));

                // Check for the existence of a record of rating changes
                var childRatingTable = NanoWikiConnector.GetSingle($"{table}Rating", new List<Condition>()
                {
                    new Condition() { Field = $"Fk{table}", Value = id },
                    new Condition() { Field = "FkUsers", Value = user.Id }
                });

                if (childRatingTable != null)
                {
                    if (ratingType.Equals("LikeCount"))
                    {
                        try
                        {
                            // If a user likes, try to get information about it.
                            RatingPut = (bool)childRatingTable.GetType().GetProperty("LikeRating").GetValue(childRatingTable);
                        }
                        catch (Exception)
                        {
                            RatingPut = false;
                        }
                    }
                    else
                    {
                        try
                        {
                            // If a user dislikes, try to get information about it.
                            RatingPut = (bool)childRatingTable.GetType().GetProperty("DislikeRating").GetValue(childRatingTable);
                        }
                        catch (Exception)
                        {
                            RatingPut = false;
                        }
                    }
                }

                // Getting the value of a changeable rating             
                ratingCount = int.Parse(parentTable.GetType().GetProperty(ratingType).GetValue(parentTable).ToString());

                // Reception of object (table) with a rating
                object ratingTable = childRatingTable ?? Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{table}Rating"));
                var Properties = ratingTable.GetType().GetProperties();

                // Current post property
                Properties[1].SetValue(ratingTable, id);
                // Current user property
                Properties[2].SetValue(ratingTable, user.Id);

                // When you click on the button to change the rating there can be three situations:
                // 1.Rating is changed for the first time;
                // 2.Rating is changed to the positive or negative;
                // 3.Cancellation of rating change

                // Rating changes for the first time and rating changes again
                if ((childRatingTable == null & RatingPut == false) | (childRatingTable != null & RatingPut == false))
                {
                    // Getting the number of positive and negative ratings
                    int likeCount = int.Parse(parentTable.GetType().GetProperty("LikeCount").GetValue(parentTable).ToString());
                    int dislikeCount = int.Parse(parentTable.GetType().GetProperty("DislikeCount").GetValue(parentTable).ToString());

                    ratingCount++;

                    // If the positive rating increases, then the negative rating decreases accordingly
                    if (ratingType.Equals("LikeCount"))
                    {
                        likeCount++;

                        likePressed = true;
                        dislikePressed = false;

                        // LikeRating property
                        if (childRatingTable != null & (bool)Properties[4].GetValue(ratingTable) == true)
                            dislikeCount--;

                        Properties[3].SetValue(ratingTable, true);
                        Properties[4].SetValue(ratingTable, false);
                    }
                    // If the negative rating increases, then the positive rating decreases accordingly
                    else
                    {
                        dislikeCount++;

                        likePressed = false;
                        dislikePressed = true;

                        // DislikeRating property
                        if (childRatingTable != null & (bool)Properties[3].GetValue(ratingTable) == true)
                            likeCount--;

                        Properties[3].SetValue(ratingTable, false);
                        Properties[4].SetValue(ratingTable, true);
                    }

                    // If the rating is changed for the first time by this user
                    if (childRatingTable == null)
                    {
                        parentTable.GetType().GetProperty(ratingType).SetValue(parentTable, ratingCount);

                        if (NanoWikiConnector.Update(parentTable) == false)
                        {
                            return Json(Translations.Error);
                        }

                        NanoWikiConnector.Insert(ratingTable);
                    }
                    // If the rating has already changed at least once
                    else
                    {
                        string likeId = $"postLikeId_{table}-{ratingTagId[(ratingTagId.IndexOf("-") + 1)..]}";
                        string dislikeId = $"postDislikeId_{table}-{ratingTagId[(ratingTagId.IndexOf("-") + 1)..]}";

                        parentTable.GetType().GetProperty("LikeCount").SetValue(parentTable, likeCount);
                        parentTable.GetType().GetProperty("DislikeCount").SetValue(parentTable, dislikeCount);

                        if (NanoWikiConnector.Update(parentTable) == false)
                        {
                            return Json(Translations.Error);
                        }

                        NanoWikiConnector.Update(ratingTable);

                        return Json(new
                        {
                            likeId,
                            dislikeId,
                            likeCount,
                            dislikeCount,
                            ratingType = "both",
                            likePressed,
                            dislikePressed
                        });
                    }
                }
                // Cancel rating change
                else if (childRatingTable != null & RatingPut == true)
                {
                    ratingCount--;

                    if (ratingType.Equals("LikeCount"))
                        Properties[3].SetValue(ratingTable, false);
                    else
                        Properties[4].SetValue(ratingTable, false);

                    NanoWikiConnector.Update(ratingTable);

                    parentTable.GetType().GetProperty(ratingType).SetValue(parentTable, ratingCount);

                    if (NanoWikiConnector.Update(parentTable) == false)
                    {
                        return Json(Translations.Error);
                    }
                }
            }

            return Json(new { ratingTagId, ratingCount, ratingType, likePressed, dislikePressed });
        }

        /// <summary>
        /// Uploading a partial view. Uploading all the comments of a specific post. Works in conjunction with the "Posts" method
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Table id</param>
        /// <param name="page">Page number</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Comments(string table, int id, int? page)
        {
            if (string.IsNullOrEmpty(table))
                return NoPosts(true);

            try
            {
                var list = NanoWikiConnector.Get(table, new List<Condition>()
                {
                    new Condition() { Field = $"Fk{table.Replace("Comments", "")}", Value = id },
                    new Condition() { Field = $"Fk{table}", Value = 0 }
                }).OrderBy(x => x.GetType().GetProperty("Id"));

                ViewBag.CommentsList = list.ToPagedList(page.HasValue ? Convert.ToInt32(page) : 1, 5);
            }
            catch (Exception)
            {
                ViewBag.CommentsList = null;
            }

            ViewBag.Page = page.HasValue ? Convert.ToInt32(page) : 1;

            if (string.IsNullOrEmpty(table))
            {
                return NoPosts(true);
            }
            else
            {
                ViewBag.TableComments = table;
                ViewBag.Table = table.Replace("Comments", "");
                ViewBag.Id = id;
            }

            return PartialView("~/Views/Home/PartialViews/Comments.cshtml");
        }

        /// <summary>
        /// Adding a new comment to the database, regardless of the branch
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Table id</param>
        /// <param name="page">Page number</param>
        /// <param name="replyCommentId">HTML id block with reply form</param>
        /// <param name="commentText">Text with comment</param>
        /// <param name="divId">Div HTML id that keeping all data</param>
        /// <param name="btnId">Button HTML id</param>
        /// <param name="secondLvl">Internal comment thread indicator</param>
        /// <param name="divIdInsert">Div HTML id where to insert the internal comment thread</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult LeaveComment(string table, int id, int? page, int? replyCommentId, string commentText, string divId, string btnId, bool secondLvl, string divIdInsert)
        {
            if (string.IsNullOrEmpty(commentText))
            {
                return Json(new
                {
                    ignoreMessage = false,
                    message = Translations.AllFieldsAreRequired,
                    divId,
                    btnId
                });
            }

            object obj;
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];

            var user = NanoWikiConnector.GetSingle<Users>("Login", HttpContext.User.Identity.Name);
            var admin = NanoWikiConnector.GetSingle<Admins>("Login", HttpContext.User.Identity.Name);

            string date = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).DateFormat);
            string time = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).TimeFormat);

            if (user == null & admin == null)
            {
                return Json(new
                {
                    message = Translations.AllFieldsAreRequired,
                    ignoreMessage = false,
                    divId,
                    btnId
                });
            }
            else
            {
                obj = Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{table}"));

                obj.GetType().GetProperties()[1].SetValue(obj, id, null);
                obj.GetType().GetProperties()[4].SetValue(obj, replyCommentId, null);
                obj.GetType().GetProperty("FkUsers").SetValue(obj, user?.Id, null);
                obj.GetType().GetProperty("FkAdmins").SetValue(obj, admin?.Id, null);

                obj.GetType().GetProperty("DateComment").SetValue(obj, date, null);
                obj.GetType().GetProperty("TimeComment").SetValue(obj, time, null);
                obj.GetType().GetProperty("Text").SetValue(obj, commentText, null);

                if (NanoWikiConnector.Insert(obj) == false)
                    return Json(new { ignoreMessage = true, message = Translations.Error, divId, btnId });
            }

            if (secondLvl == true)
            {
                return Json(new
                {
                    secondLvl,
                    divIdInsert,
                    table,
                    postId = id,
                    replyCommentId,
                    userId = user?.Id,
                    adminId = admin?.Id,
                    date,
                    time,
                    commentText
                });
            }
            else
            {
                return Json(new
                {
                    secondLvl,
                    table,
                    id,
                    page,
                    ignoreMessage = true
                });
            }
        }

        /// <summary>
        /// Uploading a partial view. Output a child comment thread
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="postId">Id record</param>
        /// <param name="replyCommentId">Id comment record</param>
        /// <param name="userId">User who left a comment</param>
        /// <param name="adminId">Admin who left a comment</param>
        /// <param name="date">Comment date</param>
        /// <param name="time">Comment time</param>
        /// <param name="commentText">Comment text</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SingleCommentLvl2(string table, int postId, int replyCommentId, int? userId, int? adminId, string date, string time, string commentText)
        {
            _obj = Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{table}"));

            _obj.GetType().GetProperties()[1].SetValue(_obj, postId, null);
            _obj.GetType().GetProperties()[4].SetValue(_obj, replyCommentId, null);
            _obj.GetType().GetProperty("FkUsers").SetValue(_obj, userId, null);
            _obj.GetType().GetProperty("FkAdmins").SetValue(_obj, adminId, null);

            _obj.GetType().GetProperty("DateComment").SetValue(_obj, date, null);
            _obj.GetType().GetProperty("TimeComment").SetValue(_obj, time, null);
            _obj.GetType().GetProperty("Text").SetValue(_obj, commentText, null);

            ViewBag.Model = _obj;

            return PartialView("~/Views/Home/PartialViews/SingleCommentLvl2.cshtml");
        }

        /// <summary>
        /// The method of entering email addresses in the database for subsequent distribution
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Subscribe(string email, string divId, string btnId)
        {
            if (string.IsNullOrEmpty(email))
            {
                return Json(new
                {
                    divId,
                    message = Translations.AllFieldsAreRequired,
                    btnId,
                    time = 5000
                });
            }

            EmailManager emailLogic = new EmailManager();

            if (emailLogic.IsEmailValid(email) == false)
            {
                return Json(new
                {
                    divId,
                    message = Translations.InvalidEmail,
                    btnId,
                    time = 5000
                });
            }

            var CheckEmail = NanoWikiConnector.GetSingle<EmailList>("Email", email);
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];
            string langId = result["langId"];

            if (CheckEmail == null)
            {
                NanoWikiConnector.Insert(new EmailList()
                {
                    CreateDate = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).DateFormat),
                    CreateTime = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).TimeFormat),
                    Email = email,
                    FkLanguages = int.Parse(langId)
                });
                return Json(new
                {
                    divId,
                    message = Translations.Success,
                    btnId,
                    time = 5000
                });
            }
            else
            {
                return Json(new
                {
                    divId,
                    message = Translations.UsernameOrEmailAlreadyExists,
                    btnId,
                    time = 5000
                });
            }
        }

        /// <summary>
        /// Displays warning page that there are no Posts. Both a regular page and a partial view
        /// </summary>
        /// <param name="ajaxQuery">Loading indicator for partial view or full page</param>
        /// <returns></returns>
        public IActionResult NoPosts(bool ajaxQuery)
        {
            ViewBag.RedirectMessage = Translations.NotFound;

            if (ajaxQuery == true)
            {
                ViewBag.AjaxQuery = true;
                return PartialView("~/Views/Home/RedirectPage.cshtml");
            }
            else
                return View("~/Views/Home/RedirectPage.cshtml");
        }

        /// <summary>
        /// The method that loads the contents of the journal
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="journalId">Id journal table</param>
        /// <param name="journalFkId">Id journal translation table</param>
        /// <param name="id">Table id</param>
        /// <param name="fkId">Id translation table</param>
        /// <param name="typePost">Type post based on table</param>
        /// <param name="journalDataType">Journal content type: table of contents, articles, publications</param>
        /// <param name="citationCondition">Is there any citation</param>
        /// <returns></returns>
        public IActionResult JournalData(string table, int? journalId, int? journalFkId, int? id, int? fkId, string typePost, string journalDataType, int? citationCondition)
        {
            var filterTable = "";
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];
            var tableName = $"Journals{(lang.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) == -1 ? "Translations" : "")}";

            if (lang.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) == -1)
                filterTable += "Translations";

            // Generate languages select
            var languages = NanoWikiConnector.Get<Languages>();
            string selectLanguages = "<select name='ArticleLanguage' class='form-control' style='min-height: 48px;'><option value=''></option>";

            if (languages != null)
            {
                foreach (var item in languages)
                {
                    selectLanguages += $"<option value='{item.Abbreviation}'>{item.Abbreviation}</option>";
                }
            }

            selectLanguages += "</select>";
            ViewBag.SelectLanguages = selectLanguages;

            // Generate topics select
            var topics = NanoWikiConnector.Get($"Topics{filterTable}");

            string selectTopics = $"<select name='FkTopics{filterTable}' class='form-control' " +
                "style='min-height: 48px;'><option value=''></option>";

            if (topics != null)
            {
                foreach (var item in topics)
                {
                    selectTopics += $"<option value='{item.GetType().GetProperty("Id").GetValue(item, null)}'>" +
                        $"{item.GetType().GetProperty("Name").GetValue(item, null)}</option>";
                }
            }

            selectTopics += "</select>";
            ViewBag.SelectTopics = selectTopics;

            // Generate organizations select
            var organizations = NanoWikiConnector.Get($"Organizations{filterTable}");

            string selectOrganizations = $"<select name='FkOrganizations{filterTable}' class='form-control' " +
                "style='min-height: 48px;'><option value=''></option>";

            if (organizations != null)
            {
                foreach (var item in organizations)
                {
                    selectOrganizations += $"<option value='{item.GetType().GetProperty("Id").GetValue(item, null)}'>" +
                        $"{item.GetType().GetProperty("Name").GetValue(item, null)}</option>";
                }
            }

            selectOrganizations += "</select>";
            ViewBag.SelectOrganizations = selectOrganizations;

            // Generate authors select
            var authors = NanoWikiConnector.Get($"Authors{filterTable}");

            string selectAuthors = $"<select name='FkAuthors{filterTable}' class='form-control' " +
                "style='min-height: 48px;'><option value=''></option>";

            if (authors != null)
            {
                foreach (var item in authors)
                {
                    selectAuthors += string.Format("<option value='{0}'>{1}</option>",
                        item.GetType().GetProperty("Id").GetValue(item, null),
                        item.GetType().GetProperty("Initials").GetValue(item, null));
                }
            }

            selectAuthors += "</select>";
            ViewBag.SelectAuthors = selectAuthors;

            // Set session values to publications data
            HttpContext.Session.SetInt32("CitationId", (int)id);
            HttpContext.Session.SetInt32("CitationFkId", (int)fkId);
            HttpContext.Session.SetString("CitationTable", lang != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation") ? "JournalsTranslationsPublications" : "JournalsPublications");

            HttpContext.Session.SetInt32("CitationCondition", citationCondition.HasValue ? (int)citationCondition : 0);

            if (journalId.HasValue || journalFkId.HasValue)
            {
                HttpContext.Session.SetInt32("JournalId", (int)journalId);

                HttpContext.Session.SetInt32("JournalFkId", (int)journalFkId);

                var name = NanoWikiConnector.GetValueById(table, "Name", (int)(journalId.HasValue ? journalId : 0));
                var publisher = NanoWikiConnector.GetValueById(table, "Publisher", (int)(journalId.HasValue ? journalId : 0));

                HttpContext.Session.SetString("JournalName", name);

                HttpContext.Session.SetString("Publisher", publisher);

                HttpContext.Session.SetString("TypePost", typePost);
            }

            // Remember journal data id and table name
            ViewBag.JournalId = journalId;
            ViewBag.JournalFkId = journalFkId;
            if (lang.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) == -1)
                ViewBag.ReturnTable = "JournalsTranslations";
            else
                ViewBag.ReturnTable = "Journals";

            if (table.IndexOf(journalDataType) == -1)
                ViewBag.Table = table + journalDataType;
            else
                ViewBag.Table = table;

            var translation = lang.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) == -1 ? "translations" : "";

            ViewBag.JournalHeader = NanoWikiConnector.GetValueById($"Journals{translation}", "Name", (int)(journalId.HasValue ? journalId : 0));

            ViewBag.Publisher = NanoWikiConnector.GetValueById($"Journals{translation}", "Publisher", (int)(journalId.HasValue ? journalId : 0));

            ViewBag.Year = (fkId.HasValue && fkId > 0) ?
                NanoWikiConnector.GetSingle<Journals>("FkJournals", fkId).Year :
                NanoWikiConnector.GetById<Journals>((int)(id.HasValue ? id : 0)).Year;

            ViewBag.Id = id;
            ViewBag.TypePost = typePost;
            ViewBag.FkId = fkId;
            ViewBag.Lang = lang;

            if (journalDataType.IndexOf("Contents") != -1)
            {
                ViewBag.PageHeader = Translations.JournalsContents;
                ViewBag.Contents = NanoWikiConnector.Get($"{table}Contents", $"Fk{table}", id).OrderBy(x => x.GetType().GetProperty("Id"));
            }
            else if (journalDataType.IndexOf("Articles") != -1)
                ViewBag.PageHeader = Translations.JournalsArticles;
            else if (journalDataType.IndexOf("Publications") != -1)
            {
                var translations = lang.IndexOf(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")) == -1 ? "Translations" : "";

                ViewBag.ReturnTable = $"Journals{translations}Articles";
                ViewBag.JournalId = HttpContext.Session.GetInt32("JournalId");
                ViewBag.JournalFkId = HttpContext.Session.GetInt32("JournalFkId");
                ViewBag.Publisher = HttpContext.Session.GetString("Publisher");
                ViewBag.TypePost = HttpContext.Session.GetString("TypePost");
                ViewBag.JournalHeader = ViewBag.JournalName = HttpContext.Session.GetString("JournalName");

                ViewBag.JournalArticleName = NanoWikiConnector.GetValueById($"Journals{translations}Articles", "Name", (int)(citationCondition.HasValue ? citationCondition : 0));
                ViewBag.PageHeader = Translations.JournalsPublications;
            }

            return View();
        }

        /// <summary>
        /// Uploading a partial view. Loads articles and publications related with journal
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="searchValue"></param>
        /// <param name="id">Table id</param>
        /// <param name="fkId">Id translation table</param>
        /// <param name="page">Page number</param>
        /// <param name="filter">Is it caused by a filter</param>
        /// <param name="sortingValue">Sorting value</param>
        /// <param name="orderValue">Order value</param>
        /// <returns></returns>
        public IActionResult JournalArticlesAndPublucations(string table, string searchValue, int? id, int? fkId, int? page, bool filter, string sortingValue, string orderValue)
        {
            var conditions = new List<Condition>();
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];
            string translations = lang != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation") ? "Translations" : "";

            int countRecords = 0;
            _pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            _obj = new List<object>();

            if (lang != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
                table += "Translations";

            conditions.Add(new Condition() { Field = $"FkJournals{translations}", Value = (fkId != 0 ? fkId : id) });

            if (table.IndexOf("Contents") != -1)
            {
                ViewBag.PageHeader = Translations.JournalsContents;
            }
            else if (table.IndexOf("Articles") != -1)
            {
                ViewBag.PageHeader = Translations.JournalsArticles;
            }
            else if (table.IndexOf("Publications") != -1)
            {
                ViewBag.PageHeader = Translations.JournalsPublications;

                conditions.RemoveAt(0);
                conditions.Add(new Condition() { Field = $"FkJournalsArticles{translations}", Value = HttpContext.Session.GetInt32("CitationCondition") });
            }

            try
            {
                // If the user opens the page through the menu, without searching
                // and user doesn't use filter panel
                if (string.IsNullOrEmpty(searchValue))
                {
                    if (!string.IsNullOrEmpty(orderValue) && orderValue.Equals("ASC"))
                    {
                        _obj = NanoWikiConnector.Get(table, conditions).OrderBy(a => a.GetType().GetProperty(sortingValue).GetValue(a)).ToPagedList(_pageIndex, _pageSize);
                    }
                    else if (!string.IsNullOrEmpty(orderValue) && orderValue.Equals("DESC"))
                    {
                        _obj = NanoWikiConnector.Get(table, conditions).OrderByDescending(a => a.GetType().GetProperty(sortingValue).GetValue(a)).ToPagedList(_pageIndex, _pageSize);
                    }
                    else
                    {
                        _obj = NanoWikiConnector.Get(table, conditions).OrderBy(a => a.GetType().GetProperty("Id").GetValue(a)).ToPagedList(_pageIndex, _pageSize);
                    }

                    countRecords = NanoWikiConnector.Count(table, conditions);
                }
                else if ((!string.IsNullOrEmpty(searchValue) & filter == true) || (!string.IsNullOrEmpty(searchValue) & HttpContext.Session.GetInt32("CitationCondition") > 0))
                {
                    if (orderValue.Equals("ASC"))
                    {
                        _obj = NanoWikiConnector.Get(table, searchValue).OrderBy(a => a.GetType().GetProperty(sortingValue).GetValue(a)).ToPagedList(_pageIndex, _pageSize);
                    }
                    else if (orderValue.Equals("DESC"))
                    {
                        _obj = NanoWikiConnector.Get(table, searchValue).OrderByDescending(a => a.GetType().GetProperty(sortingValue).GetValue(a)).ToPagedList(_pageIndex, _pageSize);
                    }

                    countRecords = NanoWikiConnector.Count(table, searchValue);
                }
            }
            catch (Exception exc)
            {
                return NoPosts(true);
            }

            if (_obj == null || countRecords <= 0)
                return NoPosts(true);

            // This data needs to load publications which are related to articles
            ViewBag.CitationId = HttpContext.Session.GetInt32("CitationId");
            ViewBag.CitationFkId = HttpContext.Session.GetInt32("CitationFkId");
            ViewBag.CitationTable = HttpContext.Session.GetString("CitationTable");

            ViewBag.CurrentCount = countRecords;
            ViewBag.Table = table;
            ViewBag.Items = _obj;
            ViewBag.Id = id;
            ViewBag.FkId = fkId;
            ViewBag.Filter = filter;
            ViewBag.SearchValue = filter ? searchValue : "";
            ViewBag.SortingValue = sortingValue;
            ViewBag.OrderValue = orderValue;

            return PartialView("~/Views/Home/PartialViews/JournalArticlesAndPublucations.cshtml");
        }

        /// <summary>
        /// This method converts the data sent from the form into a part of the request for subsequent output
        /// </summary>
        /// <param name="postData">Generated JSON data from HTML page</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GenerateFilterCondition([FromBody] dynamic postData)
        {
            string filterCondition = "PostStatus = True && ";
            string sortingValue = null;
            string orderValue = null;
            List<ModelData> list = new List<ModelData>();

            foreach (var item in JsonConvert.DeserializeObject(postData))
            {
                list.Add(new ModelData
                {
                    Field = item.Key,
                    Value = item.Value
                });
            }

            // Get current filter table and remove from fields list
            string table = list[0].Value;
            string id = list[1].Value;
            string fkId = list[2].Value;
            list.RemoveRange(0, 3);

            if (table.IndexOf("Articles") != -1 || table.IndexOf("Publications") != -1)
                filterCondition = "";

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];
            string langId = result["langId"];

            if (langName != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
                filterCondition += $"FkLanguages = {langId} && ";

            // Generate filter condition
            foreach (var item in list)
            {
                if (item.Field.Equals("Sorting"))
                {
                    sortingValue = item.Value;
                    continue;
                }

                if (item.Field.Equals("Order"))
                {
                    orderValue = item.Value;
                    continue;
                }

                if (item.Value.Equals(""))
                {
                    continue;
                }
                else
                {
                    if (item.Field.IndexOf("Fk") != -1)
                    {
                        filterCondition += $"{item.Field} = {item.Value} && ";
                        continue;
                    }

                    int buffer = 0;

                    try
                    {
                        buffer = int.Parse(item.Value);
                        filterCondition += $"{item.Field} = {item.Value} && ";
                    }
                    catch (Exception)
                    {
                        filterCondition += $"{item.Field} = \"{item.Value}\" && ";
                    }
                }
            }

            if (filterCondition.IndexOf(" &&") != -1)
                filterCondition = filterCondition.Substring(0, filterCondition.LastIndexOf(" &&"));

            //filterCondition += $" ORDER BY {sortingValue} {orderValue}";

            string urlRecords;
            if (table.IndexOf("Articles") != -1 || table.IndexOf("Publications") != -1)
                urlRecords = "/Home/JournalArticlesAndPublucations";
            else
                urlRecords = "/Home/Records";

            return Json(new { filterCondition, id, fkId, filter = true, sortingValue, orderValue, table, urlRecords });
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Method for getting links from a model object (table)
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="id">Record id</param>
        /// <param name="field">Fild name</param>
        /// <returns>Name-link format object</returns>
        private ModelData GetLinks(string table, int id, string field)
        {
            string buffer = "";
            string nameSource = "";
            string translationName = "";

            object model = NanoWikiConnector.GetById(table, id);
            PropertyInfo propertyInfo = model.GetType().GetProperty(field);

            if (propertyInfo.Name.Equals("Shops"))
            {
                buffer = model.GetType().GetProperty("Shops").GetValue(model, null).ToString();
                translationName = Translations.Shops.Substring(0, Translations.Shops.IndexOf("("));
            }
            else if (propertyInfo.Name.Equals("SourceReference"))
            {
                buffer = model.GetType().GetProperty("SourceReference").GetValue(model, null).ToString();
                translationName = Translations.SourceReference.Substring(0, Translations.SourceReference.IndexOf("("));
            }
            else if (propertyInfo.Name.Equals("SocialNetwork"))
            {
                buffer = model.GetType().GetProperty("SocialNetwork").GetValue(model, null).ToString();
                translationName = Translations.SocialNetwork.Substring(0, Translations.SocialNetwork.IndexOf("("));
            }

            if (buffer.IndexOf(",") != -1)
            {
                string[] links = buffer.Split(',');
                links = links.Select(x => x.TrimStart()).ToArray();

                foreach (var itemLinks in links)
                {
                    nameSource += $"<a href='{itemLinks.Substring(itemLinks.IndexOf("[") + 1, itemLinks.IndexOf("]") - 1)}'>{itemLinks[(itemLinks.IndexOf("]") + 1)..]}</a>, ";
                }

                nameSource = nameSource.Remove(nameSource.LastIndexOf(","));
            }
            else
            {
                if (buffer.IndexOf("[") != -1)
                {
                    nameSource = $"<a href='{buffer.Substring(buffer.IndexOf("[") + 1, buffer.IndexOf("]") - 1)}'>{buffer[(buffer.IndexOf("]") + 1)..]}</a>";
                }
                else
                {
                    nameSource = buffer;
                }
            }

            return new ModelData
            {
                Field = translationName,
                Value = nameSource
            };
        }

        /// <summary>
        /// Get current table
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="searchValue">Searching value</param>
        /// <param name="langName">Short language name (en/ru/zh/ja)</param>
        /// <returns>Return the name of the table based on the current language</returns>
        private string CurrentTable(string table, string searchValue, string langName)
        {
            string currentTable;

            // If e.g. user change site language
            if (string.IsNullOrEmpty(table))
            {
                string cookieValue;

                try
                {
                    cookieValue = Request.Cookies["CurrentTable"];
                }
                catch (Exception)
                {
                    return null;
                }

                currentTable = GetTableBasedOnLang(cookieValue, langName, searchValue);
            }
            else
            {
                // Get table name based on the current language
                currentTable = GetTableBasedOnLang(table, langName, searchValue);
                // Set cookie value
                SetCookie("CurrentTable", currentTable, 1);
            }

            return currentTable;
        }

        /// <summary>
        /// Returning the name of the table based on the current language
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="lang"></param>
        /// <returns>Name of the table based on the current language</returns>
        private string GetTableBasedOnLang(string table, string lang, string searchValue)
        {
            if (string.IsNullOrEmpty(table))
            {
                return "";
            }

            if (!string.IsNullOrEmpty(searchValue))
            {
                return "search";
            }

            // If current language is EN
            if (lang.Equals(GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation")))
            {
                return table.Replace("Translations", "");
            }
            else
            {
                return $"{table.Replace("Translations", "")}Translations";
            }
        }

        /// <summary>
        /// Method that generate condition for the most popular record from the table
        /// </summary>
        /// <param name="table">Table name</param>
        /// <returns>Generated search terms</returns>
        private string GetMostPopular(string table)
        {
            string condition = "PostStatus = True && ";

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];
            string langId = result["langId"];

            if (langName != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
            {
                condition += $"FkLanguages = {langId} && ";
            }               

            var likeCount = NanoWikiConnector.Max(table, "LikeCount");

            if (!string.IsNullOrEmpty(likeCount))
            {
                if (likeCount.Equals("0"))
                {
                    string maxId;

                    if (langName != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
                    {
                        maxId = NanoWikiConnector.Max(table, "FkLanguages", langId);
                    }
                    else
                    {
                        maxId = NanoWikiConnector.MaxId(table);
                    }

                    condition += $"Id = {maxId}";
                }
                else
                {
                    condition += $"LikeCount = {likeCount}";
                }
            }

            return condition;
        }

        /// <summary>
        /// Generating a condition for outputting data based on the current language, table, and desired value
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns>Generated search terms</returns>
        private string GetPostConditionByLang(string searchValue)
        {
            string condition = "PostStatus = True && ";

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];
            string langId = result["langId"];

            if (langName != GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"))
                condition += $"FkLanguages = {langId} && ";

            if (!string.IsNullOrEmpty(searchValue))
                condition += searchValue.Trim() + " ";

            int pos = condition.LastIndexOf("&& ");

            condition = condition.Substring(0, pos - 1);

            return condition;
        }

        /// <summary>
        /// The method of generating a special condition for partial search in all fields of the model. A special condition is the type of post "Published"
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="searchValue"></param>
        /// <returns>Generated partial match condition</returns>
        private string GenerateLikeCondition(string table, string searchValue)
        {
            string likeCondition = "PostStatus = True && (";

            if (table.IndexOf("Translations") != -1)
            {
                likeCondition = likeCondition.Replace("(", "");

                Dictionary<string, string> result = GetLangFromCookie();
                string langId = result["langId"];

                likeCondition += $" FkLanguages = {langId} && (";
            }

            object Model = Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{table}"));

            foreach (var item in Model.GetType().GetProperties())
            {
                if (item.PropertyType == typeof(int) || item.PropertyType == typeof(double))
                {
                    try
                    {
                        double temp = double.Parse(searchValue);

                        likeCondition += $"{item.Name} = {searchValue} || ";
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
                else if (item.PropertyType == typeof(string))
                {
                    likeCondition += $"{item.Name}.IndexOf(\"{searchValue}\") != -1 || ";
                }
            }

            likeCondition = likeCondition.Substring(0, likeCondition.LastIndexOf(" ||")) + ")";

            return likeCondition;
        }

        /// <summary>
        /// The method of collecting in one list Posts of different types. Needed to display a list of all Posts found after searching by the user.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns>List of different posts</returns>
        private List<object> SetSearchDataToList(List<object> buffer)
        {
            List<object> list = new List<object>();

            foreach (var item in buffer)
            {
                list.Add(item);
            }

            return list;
        }

        #endregion

        #region Cookie

        /// <summary>
        /// Opening the CookiePolicy page
        /// </summary>
        /// <returns></returns>
        public IActionResult CookiePolicy()
        {
            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];

            string path = StringsManager.GetPathByCurrentOS($"{_hostingEnvironment.WebRootPath}/docs/cookies/{langName}.txt");

            if (!System.IO.File.Exists(path))
                return NoPosts(false);

            var text = System.IO.File.ReadAllLines(path);

            ViewBag.CookiePolicy = text;

            return View();
        }

        /// <summary>
        /// Getting the name of the language from cookies
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetLangFromCookie()
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            if (Request.Cookies.ContainsKey(CookieRequestCultureProvider.DefaultCookieName))
            {
                var langName = Request.Cookies[CookieRequestCultureProvider.DefaultCookieName];
                langName = langName.Substring(2, 2);
                keyValuePairs.Add("langName", langName);

                var id = Request.Cookies["currentLanguageId"];

                if (string.IsNullOrEmpty(id))
                    id = "1";

                keyValuePairs.Add("langId", id);
            }
            else
            {
                keyValuePairs.Add("langName", GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"));
                keyValuePairs.Add("langId", GlobalValues.GetAppSettingsString("ProjectLanguage:Id"));
            }

            return keyValuePairs;
        }

        /// <summary>
        /// Setting the language in cookies
        /// </summary>
        /// <param name="key">Cookie key name</param>
        /// <param name="value">Cookine value</param>
        /// <param name="days">Cookie lifetime</param>
        private void SetCookie(string key, string value, int? days)
        {
            // The default number of days is 1.
            if (days.HasValue)
                Response.Cookies.Append(key, value, new CookieOptions { Expires = DateTime.Now.AddDays(days.Value) });
            else
                Response.Cookies.Append(key, value, new CookieOptions { Expires = DateTime.Now.AddDays(1) });
        }

        #endregion
    }
}