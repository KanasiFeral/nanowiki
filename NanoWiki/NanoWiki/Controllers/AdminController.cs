﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Authorization;
using X.PagedList;
using Newtonsoft.Json;
using NanoWiki.Classes;
using NanoWiki.Managers;
using NanoWiki.Resources;
using NanoWiki.Models.Common;
using NanoWiki.Models.Database;

namespace NanoWiki.Controllers
{
    /// <summary>
    /// Class of work with admin panel
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        #region Init data

        public struct ModelData
        {
            public string Field { get; set; }

            public string Value { get; set; }
        }
                
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IModelAttributesManager _modelAttributesManager;
        private readonly IResourcesManager _resourcesManager;

        private int _pageIndex = 1;
        private int _oldId = 1;

        private List<TablesInfo> _tableInfos;

        public AdminController(IWebHostEnvironment hostingEnvironment, IModelAttributesManager modelAttributesManager, IResourcesManager resourcesManager)
        {
            _hostingEnvironment = hostingEnvironment;
            _modelAttributesManager = modelAttributesManager;
            _resourcesManager = resourcesManager;
        }

        #endregion

        #region Public IActionResult

        /// <summary>
        /// Uploading method of the partial submission of the html editor
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult HtmlEditor() => PartialView("~/Views/HtmlEditor/HtmlEditor.cshtml");

        /// <summary>
        /// Opening the admin panel
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            try
            {
                _tableInfos = NanoWikiConnector.ExecSQL<TablesInfo>(GlobalValues.SqlQueryTables()).ToList().OrderBy(x => x.Tablename).ToList();
            }
            catch (Exception exc)
            {
                return ErrorAdmin(exc.Message, "Index loading");
            }

            if (_tableInfos == null && _tableInfos.Count == 0)
            {
                ViewBag.ErrorMessage = Translations.ServerProblemMaybeOff;
                return View("~/Views/Error/Error.cshtml");
            }

            string tables = "<option value=\"Tables\" selected>" + @Translations.Tables + "</option>";

            ViewBag.TablesCount = _tableInfos.Count;

            foreach (TablesInfo item in _tableInfos)
            {
                tables += $"<option value=\"{item.Tablename}\">{_resourcesManager.GetResourceTitle<Translations>(item.Tablename)}</option>";
            }

            ViewBag.Tables = tables;
            Dictionary<string, string> result = GetLangFromCookie();
            ViewBag.Language = result["langName"];            

            if (!string.IsNullOrEmpty(ViewBag.Language))
            {
                Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
                    CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(ViewBag.Language)),
                    new CookieOptions { Expires = DateTime.Now.AddDays(1) });
            }

            ViewBag.TotalVisitCount = NanoWikiConnector.Get<TotalVisitCount>().Sum(x => x.VisitCount);

            return View();
        }

        /// <summary>
        /// Unloading a partial view due to an error in the admin panel
        /// </summary>
        /// <returns></returns>
        public IActionResult ErrorLoading()
        {
            ViewBag.Method = Translations.Loading;
            ViewBag.ErrorMessage = Translations.Error;
            return PartialView("~/Views/Error/ErrorAdmin.cshtml");
        }

        /// <summary>
        /// Opening a form (partial view) of working with records (adding / editing)
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="method">Add or update</param>
        /// <param name="id">Record id</param>
        /// <param name="page">Page number</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddEditShow(string table, string method, int? id, int? page)
        {
            _pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            object Model;
            try
            {
                if (method == "Update")
                {
                    Model = NanoWikiConnector.GetById(table, (int)(id.HasValue ? id : 0));
                }
                else
                {
                    Model = Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{table}"));
                }
            }
            catch (Exception exc)
            {
                return ErrorAdmin($"{Translations.Error} {exc.Message}", method == "Update" ? Translations.Edit : Translations.Add);
            }

            if (Model == null)
                return ErrorLoading();

            Dictionary<string, string> result = GetLangFromCookie();
            string langName = result["langName"];

            ViewBag.ValidationList = GetValidationList(Model);
            ViewBag.Id = id;
            ViewBag.Lang = langName;
            ViewBag.Model = Model;
            ViewBag.Table = table;
            ViewBag.Page = _pageIndex;
            ViewBag.Columns = _modelAttributesManager.GetFieldsName(Model);
            ViewBag.Properties = Model.GetType().GetProperties();

            if (method == "Update")
                ViewBag.MethodText = Translations.Edit;
            else
                ViewBag.MethodText = Translations.Add;

            ViewBag.MethodValue = method;

            return PartialView("~/Views/Admin/PartialViews/AddEdit.cshtml");
        }

        /// <summary>
        /// Display all records of the selected table (partial view)
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="sortingField"></param>
        /// <param name="page">Page number</param>
        /// <param name="orderBy">Order by</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ListView(string table, string sortingField, int? page, bool orderBy = true)
        {
            _pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            string CurrentTable = "";

            try
            {
                CurrentTable = Request.HttpContext.Session.GetString("CurrentTable");
            }
            catch { }

            if (string.IsNullOrEmpty(CurrentTable) || !CurrentTable.Equals(table))
            {
                CurrentTable = table;
                Response.HttpContext.Session.SetString("CurrentTable", table);
            }

            string SearchValue = string.IsNullOrEmpty(Request.HttpContext.Session.GetString("SearchValue")) ? null : Request.HttpContext.Session.GetString("SearchValue");

            int? PageSize = Request.HttpContext.Session.GetInt32("PageSize").HasValue ? Request.HttpContext.Session.GetInt32("PageSize") : 5;

            IEnumerable<object> ListModels = null;

            if (string.IsNullOrEmpty(sortingField) && string.IsNullOrEmpty(SearchValue))
            {
                try
                {
                    ListModels = NanoWikiConnector.Get(CurrentTable).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToPagedList(_pageIndex, (int)PageSize);
                }
                catch (Exception exc)
                {
                    return ErrorAdmin(exc.Message, "ListView");
                }
            }
            else
            {
                string condition = "";

                if (!string.IsNullOrEmpty(SearchValue))
                    condition = GenerateLikeCondition(CurrentTable);

                if (!string.IsNullOrEmpty(sortingField))
                {
                    if (orderBy == true)
                    {
                        ListModels = NanoWikiConnector.Get(CurrentTable, condition).OrderBy(x => x.GetType().GetProperty(sortingField).GetValue(x)).ToPagedList(_pageIndex, (int)PageSize);
                        orderBy = false;
                    }
                    else
                    {
                        ListModels = NanoWikiConnector.Get(CurrentTable, condition).OrderByDescending(x => x.GetType().GetProperty(sortingField).GetValue(x)).ToPagedList(_pageIndex, (int)PageSize);
                        orderBy = true;
                    }
                }
                else
                {
                    ListModels = NanoWikiConnector.Get(CurrentTable, condition).OrderByDescending(x => x.GetType().GetProperty("Id").GetValue(x)).ToPagedList(_pageIndex, (int)PageSize);
                }
            }

            if (ListModels == null)
                return ErrorLoading();

            object Model = null;

            // Additional page load protection
            try
            {
                Model = Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{CurrentTable}"));
            }
            catch (Exception exc)
            {
                return ErrorAdmin($"{Translations.Error} {exc.Message}", CurrentTable);
            }

            ViewBag.Page = _pageIndex;
            ViewBag.Table = CurrentTable;
            ViewBag.Model = ListModels;
            ViewBag.OrderBy = orderBy;
            ViewBag.Properties = Model.GetType().GetProperties();
            ViewBag.Columns = _modelAttributesManager.GetFieldsName(Model);

            //ViewBag.TableCount = Connector.GetValue($"SELECT COUNT(*) FROM public.\"{CurrentTable}\"");
            ViewBag.TableCount = ListModels.Count();

            return PartialView("~/Views/Admin/PartialViews/List.cshtml");
        }

        /// <summary>
        /// Removes the selected entry from the database and updates the list
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="page">Page number</param>
        /// <param name="id">Record id</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Delete(string table, int page, int? id)
        {
            if (!id.HasValue)
                return Json(new { message = Translations.UpsErrorMessage });

            var model = NanoWikiConnector.GetById(table, (int)(id.HasValue ? id : 0));

            if (NanoWikiConnector.Delete(model) == false)
                return Json(new { message = Translations.UpsErrorMessage });

            return Json(new { page, url = "/Admin/ListView" });
        }

        /// <summary>
        /// Clearing the entire selected table
        /// </summary>
        /// <param name="table">Table name</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DropTable(string table)
        {
            var result = NanoWikiConnector.Delete(table);

            if (result == false)
                return Json(new { droptable = false, message = Translations.UpsErrorMessage });
            else
                return Json(new { droptable = true, table });
        }

        /// <summary>
        /// A method that completes the work of adding or modifying data. This method works with any table of any structure within a reasonable
        /// </summary>
        /// <param name="postData"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Apply([FromBody] dynamic postData)
        {
            Dictionary<string, string> result = GetLangFromCookie();
            string lang = result["langName"];
            
            string serverPath = _hostingEnvironment.WebRootPath;
            string table = "", methodValue = "", imgPath = "";
            int? page = 0;
            int id = 0;

            List<ModelData> list = new List<ModelData>();

            foreach (var item in JsonConvert.DeserializeObject(postData))
            {
                list.Add(new ModelData
                {
                    Field = item.Key,
                    Value = item.Value
                });
            }

            table = list[0].Value;
            methodValue = list[1].Value;
            page = int.Parse(list[2].Value);
            imgPath = list[3].Value ?? $"~/images/user/{table}/default.png";
            imgPath = imgPath.Replace("undefined", $"~/images/user/{table}/default.png");

            var photo = list.Where(x => x.Field.Equals("Photo")).FirstOrDefault().Value;
            var avatar = list.Where(x => x.Field.Equals("Avatar")).FirstOrDefault().Value;

            if (!string.IsNullOrEmpty(photo) && string.IsNullOrEmpty(imgPath))
            {
                imgPath = photo;
            }

            if (!string.IsNullOrEmpty(avatar) && string.IsNullOrEmpty(imgPath))
            {
                imgPath = avatar;
            }

            list.RemoveRange(0, 4);

            id = int.Parse(list[0].Value);

            object obj = Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{table}"));
            var Properties = obj.GetType().GetProperties();

            for (int i = 0; i < list.Count; i++)
            {
                if (Properties[i].PropertyType == typeof(int))
                {
                    try
                    {
                        Properties[i].SetValue(obj, int.Parse(list[i].Value), null);
                    }
                    catch (Exception)
                    {
                        Properties[i].SetValue(obj, 0, null);
                    }
                }
                else if (Properties[i].PropertyType == typeof(bool))
                {
                    try
                    {
                        Properties[i].SetValue(obj, bool.Parse(list[i].Value), null);
                    }
                    catch (Exception)
                    {
                        Properties[i].SetValue(obj, false, null);
                    }
                }
                else if (Properties[i].PropertyType == typeof(object))
                {
                    try
                    {
                        Properties[i].SetValue(obj, list[i].Value, null);
                    }
                    catch (Exception)
                    {
                        Properties[i].SetValue(obj, null, null);
                    }
                }
                else
                {
                    try
                    {
                        if (Properties[i].Name.Equals("Photo") || Properties[i].Name.Equals("Avatar"))
                        {
                            // Adding new image
                            if (imgPath.IndexOf("default") == -1)
                            {
                                Properties[i].SetValue(obj, imgPath);
                            }
                            else
                            {
                                string dataBaseImg = null;

                                try
                                {
                                    dataBaseImg = NanoWikiConnector.GetValueById(table, Properties[i].Name, id);
                                }
                                catch (Exception)
                                {
                                    dataBaseImg = null;
                                }

                                if (string.IsNullOrEmpty(dataBaseImg) || dataBaseImg.IndexOf("default") != -1)
                                {
                                    Properties[i].SetValue(obj, imgPath);
                                }
                                else
                                {
                                    Properties[i].SetValue(obj, dataBaseImg);
                                }
                            }
                        }
                        else
                        {
                            Properties[i].SetValue(obj, list[i].Value);
                        }
                    }
                    catch (Exception)
                    {
                        Properties[i].SetValue(obj, "", null);
                    }
                }
            }

            switch (table)
            {
                case "AdminInfo":
                    if (methodValue.Equals("Add"))
                    {
                        AdminInfo admininfo = (AdminInfo)obj;
                        admininfo.CreateDate = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).DateFormat);
                        admininfo.CreateTime = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).TimeFormat);
                        obj = admininfo;
                    }
                    break;
                case "Admins":
                    Admins admins = (Admins)obj;
                    admins.Hash = $"{HashManager.CalculateMD5Hash(admins.Email)}‡{HashManager.CalculateMD5Hash(admins.Password)}";
                    admins.Password = Encryption.Encrypt(admins.Password);
                    obj = admins;
                    break;
                case "Users":
                    Users users = (Users)obj;
                    users.Hash = $"{HashManager.CalculateMD5Hash(users.Email)}‡{HashManager.CalculateMD5Hash(users.Password)}";
                    users.Password = Encryption.Encrypt(users.Password);
                    obj = users;
                    break;
                case "UserProfile":
                    if (methodValue.Equals("Add"))
                    {
                        UserProfile userprofile = (UserProfile)obj;
                        userprofile.Avatar = "~/images/user/UserProfile/default.png";
                        userprofile.CreateDate = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).DateFormat);
                        userprofile.CreateTime = DateTime.Now.ToString(NanoWikiConnector.GetSingle<Languages>("Abbreviation", lang).TimeFormat);
                        obj = userprofile;
                    }
                    break;
                default:
                    break;
            }

            _pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            if (methodValue.Equals("Add"))
            {
                int maxId;
                if (id == 0)
                {
                    int.TryParse(NanoWikiConnector.MaxId(table), out maxId);
                    maxId++;
                }
                else
                {
                    maxId = id;
                }

                // Set id for current table
                obj.GetType().GetProperty("Id").SetValue(obj, maxId, null);

                if (NanoWikiConnector.Insert(obj) == false)
                    return Json(new { message = Translations.Error, url = "/Admin/ErrorAdmin" });
            }
            else
            {
                if (NanoWikiConnector.Update(obj) == false)
                    return Json(new { message = Translations.Error, url = "/Admin/ErrorAdmin" });
            }

            var publishProp = obj.GetType().GetProperty("PostStatus");
            
            if (publishProp != null)
            {
                // If new record has published in the site
                if (bool.Parse(publishProp.GetValue(obj).ToString()) == true)
                {
                    // Send email for users about adding new post
                    SendNotificationAboutNewPostByEmail(table);
                }
            }

            return Json(new { page = _pageIndex, url = "/Admin/ListView" });
        }

        /// <summary>
        /// Error displayed when working with records (adding / editing)
        /// </summary>
        /// <param name="message">Error message</param>
        /// <param name="method">Method in which the error occurred</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ErrorAdmin(string message, string method)
        {
            ViewBag.ErrorMessage = message;
            ViewBag.Method = method;
            return PartialView("~/Views/Error/ErrorAdmin.cshtml");
        }

        /// <summary>
        /// Setting via cookie the number of records displayed on the page
        /// </summary>
        /// <param name="pageSize">Number of records per page</param>
        /// <returns></returns>
        public IActionResult SetPageSize(int pageSize)
        {
            try
            {
                Response.HttpContext.Session.SetInt32("PageSize", pageSize);
            }
            catch { }

            try
            {
                Response.HttpContext.Session.Set("OrderBy", false);
            }
            catch { }

            return Json(pageSize);
        }

        /// <summary>
        /// Setting through the cookie the desired value
        /// </summary>
        /// <param name="searchValue">Searching value</param>
        /// <returns></returns>
        public IActionResult SetSearchValue(string searchValue)
        {
            try
            {
                if (string.IsNullOrEmpty(searchValue))
                    Response.HttpContext.Session.Remove("SearchValue");
                else
                    Response.HttpContext.Session.SetString("SearchValue", searchValue);
            }
            catch { }

            return Json(searchValue);
        }

        /// <summary>
        /// Method from html editor. This method uploads the file to the server
        /// </summary>
        /// <param name="typeFile">Type of file</param>
        /// <returns></returns>
        [HttpPost("UploadImage")]
        [HttpPost("UploadVideo")]
        [HttpPost("UploadFile")]
        [Produces("application/json")]
        public async Task<IActionResult> UploadFile(string typeFile)
        {
            // Get the file from the POST request
            IFormFile theFile = HttpContext.Request.Form.Files.GetFile("file");

            // Get the server path, wwwroot
            string webRootPath = _hostingEnvironment.WebRootPath;

            // Building the path to the uploads directory
            string fileRoute;
            if (typeFile.Equals("image"))
                fileRoute = Path.Combine(webRootPath, "uploadedImages/admin");
            else if (typeFile.Equals("video"))
                fileRoute = Path.Combine(webRootPath, "uploadedVideos/admin");
            else
                fileRoute = Path.Combine(webRootPath, "uploadedFiles/admin");

            // Get the mime type
            var mimeType = HttpContext.Request.Form.Files.GetFile("file").ContentType;

            // Get File Extension
            string extension = Path.GetExtension(theFile.FileName);

            // Generate Random name.
            string name = Guid.NewGuid().ToString().Substring(0, 8) + extension;

            // Build the full path inclunding the file name
            string link = Path.Combine(fileRoute, name);

            // Basic validation on mime types and file extension
            string[] imageMimetypes = { "image/gif", "image/jpeg", "image/pjpeg",
                "image/x-png", "image/png", "image/svg+xml", "image/JPG", "image/JPEG" };
            string[] imageExt = { ".gif", ".jpeg", ".jpg", ".png", ".svg",
                ".blob", ".JPG", ".JPEG" };

            // Basic validation on mime types and file extension
            string[] videoMimetypes = { "video/mp4", "video/webm", "video/ogg" };
            string[] videoExt = { ".mp4", ".webm", ".ogg" };

            // Basic validation on mime types and file extension
            string[] fileMimetypes = { "text/plain", "application/msword", "application/x-pdf",
                "application/pdf", "application/json", "text/html",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document" };
            string[] fileExt = { ".txt", ".pdf", ".doc", ".json", ".html", ".docx" };

            if (typeFile.Equals("image"))
            {
                try
                {
                    if (Array.IndexOf(imageMimetypes, mimeType) >= 0 && 
                        (Array.IndexOf(imageExt, extension) >= 0))
                    {
                        await UploadFileFromServerAsync(link, theFile);

                        // Return the file path as json
                        Hashtable imageUrl = new Hashtable
                        {
                            { "link", $"/uploadedImages/admin/{name}" }
                        };

                        return Json(imageUrl);
                    }
                    throw new ArgumentException(Translations.TheImageDidNotPassTheValidation);
                }
                catch (ArgumentException ex)
                {
                    return Json(ex.Message);
                }
            }
            else if (typeFile.Equals("video"))
            {
                try
                {
                    if (Array.IndexOf(videoMimetypes, mimeType) >= 0 && 
                        (Array.IndexOf(videoExt, extension) >= 0))
                    {
                        await UploadFileFromServerAsync(link, theFile);

                        // Return the file path as json
                        Hashtable videoUrl = new Hashtable
                        {
                            { "link", $"/uploadedVideos/admin/{name}" }
                        };

                        return Json(videoUrl);
                    }
                    throw new ArgumentException(Translations.TheVideoDidNotPassTheValidation);
                }
                catch (ArgumentException ex)
                {
                    return Json(ex.Message);
                }
            }
            else
            {
                try
                {
                    if (Array.IndexOf(fileMimetypes, mimeType) >= 0 && 
                        (Array.IndexOf(fileExt, extension) >= 0))
                    {
                        await UploadFileFromServerAsync(link, theFile);

                        // Return the file path as json
                        Hashtable fileUrl = new Hashtable
                        {
                            { "link", $"/uploadedFiles/admin/{name}" }
                        };

                        return Json(fileUrl);
                    }
                    throw new ArgumentException(Translations.TheFileDidNotPassTheValidation);
                }
                catch (ArgumentException ex)
                {
                    return Json(ex.Message);
                }
            }
        }

        /// <summary>
        /// Method from html editor. This method deletes the file from the server
        /// </summary>
        /// <param name="src">Path to file</param>
        /// <returns></returns>
        [HttpPost("DeleteImage")]
        [HttpPost("DeleteVideo")]
        [HttpPost("DeleteFile")]
        [Produces("application/json")]
        public IActionResult DeleteFile(string src)
        {
            if (string.IsNullOrEmpty(src))
                return null;

            var pathFile = _hostingEnvironment.WebRootPath + src.Replace("/", "\\");

            FileInfo fileDelete;

            try
            {
                fileDelete = new FileInfo(pathFile);
            }
            catch (Exception)
            {
                return Json(Translations.Error);
            }

            if (fileDelete.Exists)
            {
                try
                {
                    fileDelete.Delete();
                }
                catch (Exception exc)
                {
                    return Json(exc.Message);
                }
            }

            return Json(Translations.Success);
        }

        /// <summary>
        /// Adding a file to the server. Needed to add avatars, preview images for Posts. Associated with the Apply method
        /// </summary>
        /// <param name="fileImg">Object with file</param>
        /// <param name="status">Delete or add file</param>
        /// <param name="table">Table name</param>
        /// <param name="id">Record id</param>
        /// <returns></returns>
        [HttpPost]
        public string ApplyImgFile(IFormFile fileImg, string status, string table, int? id)
        {
            string image = "";

            if (status.Equals("Delete"))
            {
                var field = "Photo";
                if (table.Equals("UserProfile") || table.Equals("AdminInfo"))
                    field = "Avatar";

                NanoWikiConnector.Update(table, (int)(id.HasValue ? id : 0), field, $"~/images/user/{table}/default.png");

                image = $"~/images/user/{table}/default.png";
            }
            // Add new file
            else
            {
                // Also show list of uploaded files!
                // var filesObj = Request.Form.Files;

                if (fileImg != null)
                {
                    image = FileConvertingManager.ConvertImageToBase64(fileImg);
                }
            }

            return image;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Method from html editor. Upload any type of file to server
        /// </summary>
        /// <param name="link">Source link</param>
        /// <param name="theFile">Object with file</param>
        /// <returns>Result of loading file</returns>
        private async Task UploadFileFromServerAsync(string link, IFormFile theFile)
        {
            // Copy contents to memory stream.
            Stream stream;
            stream = new MemoryStream();
            theFile.CopyTo(stream);
            stream.Position = 0;
            string serverPath = link;

            // Save the file
            using FileStream writerFileStream = System.IO.File.Create(serverPath);
            await stream.CopyToAsync(writerFileStream);
            writerFileStream.Dispose();
        }

        /// <summary>
        /// Generate validation sheet for selected MVC model
        /// </summary>
        /// <param name="model">Model with data (table)</param>
        /// <returns>Validation sheet for a specific table</returns>
        private List<string> GetValidationList(object model)
        {
            List<string> validationList = new List<string>();

            foreach (var item in model.GetType().GetProperties())
            {
                PropertyInfo info = model.GetType().GetProperty(item.Name);

                if (_modelAttributesManager.GetRequiredAttributeValue(item.Name, model) == true)
                {
                    if (info.PropertyType == typeof(int))
                    {
                        if (item.Name.IndexOf("Fk") != -1)
                        {
                            validationList.Add("data-validation=\"number\" " +
                                "data-validation-allowing=\"range[1;2147483647]\"");
                        }
                        else
                        {
                            validationList.Add("data-validation=\"number\"");
                        }
                    }
                    else if ((info.PropertyType == typeof(object) &&
                        model.GetType().Name.IndexOf("Comments") != -1) ||
                        info.PropertyType == typeof(bool))
                    {
                        validationList.Add("data-validation=\"number\" " +
                            "data-validation-allowing=\"range[1;2147483647]\"");
                    }
                    else if (info.PropertyType == typeof(string))
                    {
                        var buffer = _modelAttributesManager.GetDataTypeAttributeValue(item.Name, model);

                        if (buffer != null && (DataType)buffer == DataType.EmailAddress)
                        {
                            validationList.Add("data-validation=\"email\"");
                        }
                        else
                        {
                            validationList.Add($"data-validation=\"length\" " +
                                $"data-validation-length=\"1-" +
                                $"{_modelAttributesManager.GetLengthAttributeValue(item.Name, model)}\"");
                        }
                    }
                }
                else
                {
                    if (item.Name.Equals("Id"))
                        _oldId = (int)item.GetValue(model, null);

                    validationList.Add("");
                }
            }

            return validationList;
        }

        /// <summary>
        /// Generation of search in all fields of MVC model
        /// </summary>
        /// <param name="table">Table name</param>
        /// <returns>Partial match condition generated</returns>
        private string GenerateLikeCondition(string table)
        {
            string searchValue = Request.HttpContext.Session.GetString("SearchValue");

            string likeCondition = "";

            object Model = Activator.CreateInstance(Type.GetType($"{StringsManager.GetProjectName()}.Models.Database.{table}"));

            foreach (var item in Model.GetType().GetProperties())
            {
                if (item.PropertyType == typeof(int) || item.PropertyType == typeof(double))
                {
                    try
                    {
                        double temp = double.Parse(searchValue);

                        likeCondition += $"{item.Name} = {searchValue} || ";
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
                else if (item.PropertyType == typeof(string))
                {
                    likeCondition += $"{item.Name}.IndexOf(\"{searchValue}\") != -1 || ";
                }
            }

            likeCondition = likeCondition.Substring(0, likeCondition.LastIndexOf(" ||"));

            return likeCondition;
        }

        /// <summary>
        /// Send email with notification about new post
        /// </summary>
        /// <param name="table">Table name</param>
        private void SendNotificationAboutNewPostByEmail(string table)
        {
            List<UserSettings> list = new List<UserSettings>();
            object lastPost;

            switch (table)
            {
                case "Events":
                case "EventsTranslations":
                    list = NanoWikiConnector.Get<UserSettings>().Where(x => x.EventsNotifications == 1).ToList();
                    break;

                case "Books":
                case "BooksTranslations":
                    list = NanoWikiConnector.Get<UserSettings>().Where(x => x.BooksNotifications == 1).ToList();
                    break;

                case "Journals":
                case "JournalsTranslations":
                    list = NanoWikiConnector.Get<UserSettings>().Where(x => x.JournalsNotifications == 1).ToList();
                    break;

                case "Posts":
                case "PostsTranslations":
                    list = NanoWikiConnector.Get<UserSettings>().Where(x => x.PostsNotifications == 1).ToList();
                    break;

                case "Scientists":
                case "ScientistsTranslations":
                    list = NanoWikiConnector.Get<UserSettings>().Where(x => x.ScientistsNotifications == 1).ToList();
                    break;

                default:
                    break;
            }

            // Exit from method if no one user want to see notifications
            if (list.Count == 0)
                return;

            // Get id of last post by table name
            var maxId = NanoWikiConnector.MaxId(table);
            // Get last post
            lastPost = NanoWikiConnector.GetSingle(table, "Id", maxId);

            //http://localhost:2416/Home/SinglePost/10?table=Posts&fkId=0&typePost=post-cata%20cata-sm%20cata-danger

            // Base link data
            string id = "0", fkId = "0", typePost = "typePost=post-cata%20cata-sm%20cata-danger";

            // Add values from base link data
            id = lastPost.GetType().GetProperty("Id").GetValue(lastPost).ToString();

            // Check another language
            if (table.IndexOf("translations") != -1)
            {
                fkId = lastPost.GetType().GetProperty($"Fk{table.Replace("translations", "")}").GetValue(lastPost).ToString();
            }

            // Get icons for current table
            var icons = StringsManager.GetTypePost(table);
            typePost = icons["typePost"].Replace(" ", "%");

            // Generate mesage link
            var link = $"<a href=\"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.Value}/" +
                $"Home/SinglePost/{id}?table={table}&fkId={fkId}&{typePost}\">{Translations.GoTo}</a>";

            // Generate message text
            var message = $"{lastPost.GetType().GetProperty("Description").GetValue(lastPost)}.<br/>" +
                $"{Translations.TheSiteHasANewNews} -> {link}";

            EmailManager sendMessage = new EmailManager();           

            // Loop by email list and send email message about new post
            foreach (var item in list)
            {
                // Generate message for user
                MessageModel messageModel = new MessageModel()
                {
                    EmailTo = NanoWikiConnector.GetById<Users>(item.FkUsers).Email,
                    Message = message,
                    Title = lastPost.GetType().GetProperty("Name").GetValue(lastPost).ToString()
                };

                // Send email for user
                var result = sendMessage.SendEmail(messageModel, false);
            }
        }

        #endregion

        #region Cookie

        /// <summary>
        /// Getting language from cookies
        /// </summary>
        /// <returns>Key-value format dictionary</returns>
        private Dictionary<string, string> GetLangFromCookie()
        {

            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            if (Request.Cookies.ContainsKey(CookieRequestCultureProvider.DefaultCookieName))
            {
                var langName = Request.Cookies[CookieRequestCultureProvider.DefaultCookieName];
                langName = langName.Substring(2, 2);
                keyValuePairs.Add("langName", langName);

                var id = Request.Cookies["currentLanguageId"];

                if (string.IsNullOrEmpty(id))
                    id = "1";

                keyValuePairs.Add("langId", id);
            }
            else
            {
                keyValuePairs.Add("langName", GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation"));
                keyValuePairs.Add("langId", GlobalValues.GetAppSettingsString("ProjectLanguage:Id"));
            }

            return keyValuePairs;
        }

        #endregion
    }
}