﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using NanoWiki.Classes;
using NanoWiki.Models.Database;

namespace NanoWiki.Controllers
{
    /// <summary>
    /// Controller is responsible for working with the language of the site
    /// </summary>
    public class LanguageController : Controller
    {
        /// <summary>
        /// Changing the language of the site and setting its value in cookies with a storage period of one day
        /// </summary>
        /// <param name="culture">Language culture (en/ru/zh/ja)</param>
        /// <param name="returnUrl">The page on which the language was changed</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            // If the culture is empty, then set the default English
            if (string.IsNullOrEmpty(culture))
                culture = GlobalValues.GetAppSettingsString("ProjectLanguage:Abbreviation");

            // Return Address for JS function
            if (string.IsNullOrEmpty(returnUrl))
                returnUrl = "/";

            // Writing the current culture to cookies and setting the lifetime to 1 day

            string currentLanguage = CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture));
            string currentLanguageId = GetLanguageID(currentLanguage);
            
            Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName, currentLanguage, 
                new CookieOptions { Expires = DateTime.Now.AddDays(1)});

            Response.Cookies.Append("currentLanguageId", currentLanguageId,
                new CookieOptions { Expires = DateTime.Now.AddDays(1)});

            // Get visitor Id
            var visitorId = Request.Cookies["VisitorId"];

            if (!string.IsNullOrEmpty(visitorId))
            {
                Response.Cookies.Delete("VisitorId");
            }

            // Return json object
            return Json(new { url = returnUrl });
        }
        
        /// <summary>
        /// Getting language id by language name
        /// </summary>
        /// <param name="currentLanguage">Language name</param>
        /// <returns>Language id</returns>
        private string GetLanguageID(string currentLanguage)
        {
            currentLanguage = currentLanguage.Substring(2, 2);
            Languages result = (Languages)NanoWikiConnector.GetSingle("Languages", "Abbreviation", currentLanguage);
            if (result != null)
            {
                return result.Id.ToString();
            }
            return GlobalValues.GetAppSettingsString("ProjectLanguage:Id");
        }
      
    }
}