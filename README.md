#### NanoWiki ###
* https://nanowiki.net/

### About ####
* Scientific and technical news online portal for nanotechnology, with elements of a social network and online store.

### Technologies ###
* Asp net mvc core 3.1
* Postgresql 12.3

### Creater ###
* Asinouski Kanstantsin (https://vk.com/kanasi.feral)

### Emails ###
* zicise@mail.ru
* zicshepard@gmail.com